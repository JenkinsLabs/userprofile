$( document ).ready(function() {  
	
	var valid = true;
	var isEmailValid = true;
	var isEmirateValid = true;
	var isMobileValid = true;


$("#mobileNo").on("blur change",function() {
				this.value = this.value.trim();
				$(this).parent().find('.errormsg').remove();
				if ($("#mobileNo").val() == "") {
					if ($(this).parent().find('errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.mobile+ "</p>");

					}
					valid = valid && false;
				} else if (!/^(([+]?971)|(00971)|0|(0971))[5][0-9]{8,9}$|^$/
						.test(this.value)) {
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.invalidMobile+ "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('.errormsg').remove();
					checkMobileNumberAvailability();
					valid = valid && true;
				}
			});

$("#email").on("blur change",function() {
		
			this.value = this.value.trim();
			$(this).parent().find('.errormsg').remove();
			if ($("#email").val() == "") {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.email+ "</p>");

				}
				valid = valid & false;
			} else if (!/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
					.test(this.value)) {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.emailFormat+ "</p>");

				}
				valid = valid && false;
			} else {
				$(this).parent().find('errormsg').remove();
				 checkEmailAvailability(); // Ajax call to check the availability
				
				valid = valid && true;
			//	isEmailValid = true;
			}
		});
$("#confirmEmail").on("blur change",function() {
			this.value = this.value.trim();
			$(this).parent().find('.errormsg')
					.remove();
			if ($("#confirmEmail").val() == "") {
				if ($(this).parent().find(
						'errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.confirmEmail+ "</p>");

				}
				valid = valid & false;
			} else if ($.trim($("#confirmEmail").val()) != $.trim($("#email").val())) {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.invalidConfirmEmail+ "</p>");

				}
				valid = valid && false;
			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}
		});

$("#nationality").on("blur change",function() {
			this.value = this.value.trim();
			$(this).parent().find('.errormsg')
					.remove();
			if ($("#nationality").val() == -1) {
				if ($(this).parent().find(
						'errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.nationality+ "</p>");

				}
				valid = valid & false;
			} else {
				$(this).parent().find(
						'errormsg').remove();
				valid = valid && true;
			}
		});

$("#emirateidexpiry").on("blur change",function() {

			if (($('#nationality').val() == '217'
					|| $('#nationality').val() == '187'
					|| $('#nationality').val() == '119'
					|| $('#nationality').val() == '36'
					|| $('#nationality').val() == '165' || $(
					'#nationality').val() == '176')
					|| (document.userSignUp.radioId[0].checked == true)) {
				this.value = this.value.trim();
				$(this).parent().find('.errormsg').remove();

				if ($("#emirateidexpiry").val() == "") {
					if ($(this).parent().find(
							'errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.emirateExpiryDate+ "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('errormsg').remove();
					valid = valid && true;
				}

			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}
		});

$("#emirateAttachement").on("blur change",function() {

			if (($('#nationality').val() == '217'
					|| $('#nationality').val() == '187'
					|| $('#nationality').val() == '119'
					|| $('#nationality').val() == '36'
					|| $('#nationality').val() == '165' || $(
					'#nationality').val() == '176')
					|| (document.userSignUp.radioId[0].checked == true)) {

				$(this).parent().find('.errormsg').remove();

				if ($("#emirateAttachement").val() == "") {
					if ($(this).parent().find('errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.idCopyAttachement + "</p>");

					}
					valid = valid & false;
				} else if (!isFileSizeOK("emirateAttachement")) {
					$(this).parent().find('errormsg')
							.remove();
					if ($(this).parent().find(
							'errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.attachmentType+ "</p>");

					valid = valid && false;
				} else if (!isFileLength("emirateAttachement")) {
					$(this).parent().find('errormsg').remove();
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>" + errorMessage.fileLength + "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('errormsg').remove();
					valid = valid && true;
				}

			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}

		});

$("#residenceId").on("blur change",function() {

			if (($('#nationality').val() != '217'
					&& $('#nationality').val() != '187'
					&& $('#nationality').val() != '119'
					&& $('#nationality').val() != '36'
					&& $('#nationality').val() != '165'
					&& $('#nationality').val() != '176' && $(
					'#nationality').val() != '217')
					&& (document.userSignUp.radioId[1].checked == true)) {
				this.value = this.value.trim();
				$(this).parent().find(
						'.errormsg').remove();
				if ($("#residenceId").val() == "") {
					if ($(this).parent().find(
							'errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.unifiedID+ "</p>");
					}
					valid = valid & false;
				} else if (!/^[A-Za-z0-9\u0600-\u06FF.]+$/
						.test(this.value)) {
					if ($(this).parent().find('errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.invalidResidenceId+ "</p>");

					}
					valid = valid && false;
				} else {
					$(this).parent().find('errormsg').remove();
					checkResidenceIDAvailability();
					valid = valid && true;
				}

			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}

		});

$("#residenceExpireDate").on("blur change",function() {

			if (($('#nationality').val() != '217'
					&& $('#nationality').val() != '187'
					&& $('#nationality').val() != '119'
					&& $('#nationality').val() != '36'
					&& $('#nationality').val() != '165'
					&& $('#nationality').val() != '176' && $(
					'#nationality').val() != '217')
					&& (document.userSignUp.radioId[1].checked == true)) {

				this.value = this.value.trim();
				$(this).parent().find(
						'.errormsg').remove();
				if ($("#residenceExpireDate").val() == "") {
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.unifiedExpiryDate+ "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('errormsg').remove();
					valid = valid && true;
				}

			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}

		});
$("#residenceAttachement").on("blur change",function() {

			if (($('#nationality').val() != '217'
					&& $('#nationality').val() != '187'
					&& $('#nationality').val() != '119'
					&& $('#nationality').val() != '36'
					&& $('#nationality').val() != '165'
					&& $('#nationality').val() != '176' && $(
					'#nationality').val() != '217')
					&& (document.userSignUp.radioId[1].checked == true)) {

				$(this).parent().find('.errormsg').remove();
				if ($("#residenceAttachement").val() == "") {
					if ($(this).parent().find('errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.idCopyAttachement+ "</p>");

					}
					valid = valid & false;
				} else if (!isFileSizeOK("residenceAttachement")) {
					$(this).parent().find('errormsg').remove();
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.attachmentType+ "</p>");

					valid = valid && false;
				} else if (!isFileLength("residenceAttachement")) {
					$(this).parent().find('errormsg').remove();
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.fileLength+ "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('errormsg').remove();
					valid = valid && true;
				}

			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}
		});
	 


$("#emiratesId").on( "blur change", function() {
	
	
	if(($('#nationality').val()!='217'&& $('#nationality').val()!='187' && $('#nationality').val()!='119' && $('#nationality').val()!='36' && $('#nationality').val()!='165' && $('#nationality').val()!='176') && (document.userSignUp.radioId[0].checked == true )){
		
		this.value = this.value.trim();
		$(this).parent().find('.errormsg').remove();
			if($("#emiratesId").val()==""){
					if($(this).parent().find('errormsg').length == 0){
				    	$(this).parent().append("<p class='errormsg'>errorMessage.emirateId</p>");
				    }
					valid=valid&false;
				}else if(!isEmirateId(this.value)){
					if($(this).parent().find('errormsg').length == 0)
					$(this).parent().append("<p class='errormsg'>errorMessage.invalidEmirateId</p>");
					valid = valid && false;
				}else{
					$(this).parent().find('errormsg').remove();
					isEmirateValid=checkEmirateIDAvailability('emiratesId');//------------------------------ newly added ------------------
					valid = valid && true;
				}
				
	}else if($('#nationality').val()=='217'){
		this.value = this.value.trim();
		$(this).parent().find('.errormsg').remove();
		if($("#emiratesId").val()==""){
					if($(this).parent().find('errormsg').length == 0){
				    	$(this).parent().append("<p class='errormsg'>errorMessage.emirateId</p>");
				    }
					valid=valid&false;
				}else if(!isEmirateId(this.value)){
					if($(this).parent().find('errormsg').length == 0)
					$(this).parent().append("<p class='errormsg'>errorMessage.invalidEmirateId</p>");
					valid = valid && false;
				}else{
					$(this).parent().find('errormsg').remove();
					isEmirateValid=checkEmirateIDAvailability('emiratesId');//------------------------------ newly added ------------------
					valid = valid && true;
				}
	}
	
});




}); 



/*function isEmirateId(emiratsId) {
	var re = /^(784)-[0-9]{4}-[0-9]{7}-[0-9]{1}$|^$/;
	return re.test(emiratsId);
}

function isFileSizeOK(fileNodeID) {
	var fileNode = document.getElementById(fileNodeID);
	if (navigator.appName == "Microsoft Internet Explorer"
			&& navigator.appVersion.indexOf("MSIE 9.0")) {
	} else {
		for (var i = 0; i < fileNode.files.length; i++) {
			if (fileNode.files[i].size > 2097152) {
				return false;
			}
		}
	}
	return (/(\.(gif|jpg|jpeg|png|pdf|doc|docx|pdf)$)|^$/i)
			.test(fileNode.value);
}
function isFileLength(fileNodeID) {
	var fileNode = document.getElementById(fileNodeID);
	var fileName = fileNode.value;
	if ((navigator.appName == "Microsoft Internet Explorer" && (navigator.appVersion
			.indexOf("MSIE 9.0") || navigator.appVersion
			.indexOf("MSIE 10.0") > -1))
			|| (navigator.appName == "Netscape" && navigator.appVersion
					.indexOf("Trident/7.0") > -1)) {
		fileName = fileName.substring(fileName.lastIndexOf('\\') + 1,
				fileName.length);
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
	} else {
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
	}
	return fileName.length < 45;
}

function clearAttachment(nodeID) {
	console.log("--clear attachment--");
		    var control = $("#"+nodeID);
		    console.log('control--'+control);
		    if(isMSIE()){
		                    control.replaceWith( control = control.clone( true ) );
		            		emirateValidation();
		            		residentValidation();
		    }else{
		    	console.log('---else---');
		                    control.val('');
		    }
}*/
