if (typeof console == "undefined") {
    window.console = {
        log: function () {}
    };
}

$('#dateOfBirth').datepicker({ minDate: "-100Y",maxDate:"-15Y",
							dateFormat : "dd/mm/yy",
							changeMonth : true,
							changeYear : true,
							showOn : "button",
							buttonImage : "/html/themes/css/base/images/calendar.gif",
							yearRange : 'c-110:c+50',
							buttonImageOnly : true,
							yearRange : 'c-110:c+50',
							changeMonth : true,
							onClose : function(selectedDate) {
								this.focus();
								this.blur();
								$(this).change();
							}
						});

$('#dateOfBirth').attr('readonly', true);

		
$('#idExpireDate').datepicker({ minDate: "-100Y",maxDate: "-15Y",
			dateFormat : "dd/mm/yy",
			changeMonth : true,
			changeYear : true,
			showOn : "button",
			buttonImage : "/html/themes/css/base/images/calendar.gif",
			yearRange : 'c-110:c+50',
			buttonImageOnly : true,
			yearRange : 'c-110:c+50',
			changeMonth : true,
			onClose : function(selectedDate) {
			}
		});

		$('#idExpireDate').attr('readonly', true);

		
		$('#emirateExpireDatePopup').datepicker({
			dateFormat:  "dd/mm/yy",
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "/html/themes/css/base/images/calendar.gif",
			yearRange: 'c-110:c+50',
			buttonImageOnly: true,
			yearRange: 'c-110:c+50',
			changeMonth: true,
			minDate: "dateToday",
		    onSelect: function(date) {
			this.focus();
			this.blur();
			$(this).change();
		}
	}); 
	$('#emirateExpireDatePopup').attr('readonly', true);
		
		
$('#expatExpiredate').datepicker({ minDate: "-100Y",maxDate: "-15Y",
			dateFormat : "dd/mm/yy",
			changeMonth : true,
			changeYear : true,
			showOn : "button",
			buttonImage : "/html/themes/css/base/images/calendar.gif",
			yearRange : 'c-110:c+50',
			buttonImageOnly : true,
			yearRange : 'c-110:c+50',
			changeMonth : true,
			onClose : function(selectedDate) {
			}
		});

		$('#expatExpiredate').attr('readonly', true);
		
		
		
		$('#residenceExpireDate').datepicker({
			dateFormat:  "dd/mm/yy",
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "/html/themes/css/base/images/calendar.gif",
			yearRange: 'c-110:c+50',
			buttonImageOnly: true,
			yearRange: 'c-110:c+50',
			changeMonth: true,
			minDate: "dateToday",
		    onSelect: function(date) {
			this.focus();
			this.blur();
			$(this).change();
		}
	}); 
	$('#residenceExpireDate').attr('readonly', true);
	
	$('#emirateExpireDate').datepicker({
		dateFormat:  "dd/mm/yy",
		changeMonth: true,
		changeYear: true,
		showOn: "button",
		buttonImage: "/html/themes/css/base/images/calendar.gif",
		yearRange: 'c-110:c+50',
		buttonImageOnly: true,
		yearRange: 'c-110:c+50',
		changeMonth: true,
		minDate: "dateToday",
	    onSelect: function(date) {
		this.focus();
		this.blur();
		$(this).change();
	}
}); 
$('#emirateExpireDate').attr('readonly', true);

$('#passportExpiryDate').datepicker({
	dateFormat:  "dd/mm/yy",
	changeMonth: true,
	changeYear: true,
	showOn: "button",
	buttonImage: "/html/themes/css/base/images/calendar.gif",
	yearRange: 'c-110:c+50',
	buttonImageOnly: true,
	yearRange: 'c-110:c+50',
	changeMonth: true,
	minDate: "dateToday",
    onSelect: function(date) {
	this.focus();
	this.blur();
	$(this).change();
}
}); 
$('#passportExpiryDate').attr('readonly', true);


	
//initUI();


function setHeightOfLeftinfoBar(tbWhole, parentIndex, addedRows) {
	console.log("-----inside setHeightOfLeftinfoBar--------");
	AUI().use('event', 'node', function(A) {
		A.all('.left-info-bar > ul').each(function(node) {
			node.setStyle('height',(122)+'px');
		});
		A.all('.left-info-bar > ul').each(function(node) {
			node.setStyle('height',(A.one('form').height()-166)+'px');
		});
		A.all('.aui-label-required').each(function(node) {
			node.setStyle('display', 'inline');
		});
		/*if(A.one(tbWhole) && A.one(tbWhole).all('.delete').size()==1){
			console.log('count of delete button : '+A.one(tbWhole).all('.delete').size());
			A.one(tbWhole).all('.delete').setStyle('display', 'none');
		} else if(A.one(tbWhole)) {
			A.one(tbWhole).all('.delete').setStyle('display', 'block');
		}*/
		if(typeof(readOnly) != 'undefined' && readOnly != null && readOnly == true){
			makeReadOnly();
		}
	});
}
