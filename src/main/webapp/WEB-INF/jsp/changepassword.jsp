<!DOCTYPE HTML><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
		<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
     
     <portlet:actionURL var="changePassword" >
 		 <portlet:param name="action" value="updatepassword" />
 		</portlet:actionURL>
 		
 		<style>
	
	.removeAttachmentClass{
	position: absolute;right: 9px;top: 8px;color: red;
	}

.arrow_box {
	position: relative;
	background: #88b7d5;
	border: 4px solid #c2e1f5;
}
.arrow_box:after, .arrow_box:before {
	bottom: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(136, 183, 213, 0);
	border-bottom-color: #88b7d5;
	border-width: 30px;
	margin-left: -30px;
}
.arrow_box:before {
	border-color: rgba(194, 225, 245, 0);
	border-bottom-color: #c2e1f5;
	border-width: 36px;
	margin-left: -36px;
}
.text-center .errormsg{position:inherit;}

.succesmsg{position:absolute;color:green;}

#pswd_info {
    position:absolute;
   
    width:250px;
    padding:15px;
    background:#fefefe;
    font-size:.875em;
    border-radius:5px;
    box-shadow:0 1px 3px #ccc;
    border:1px solid #ddd;
    z-index:9999;
    top:65px;
}
#pswd_info h4 {
    margin:0 0 10px 0;
    padding:0;
    font-weight:normal;
}
#pswd_info::before {
    content: "\25B2";
    position:absolute;
    top:-12px;
    left:45%;
    font-size:14px;
    line-height:14px;
    color:#ddd;
    text-shadow:none;
    display:block;
}

.errmsg{
		color : red;
		}

.invalid {
    background:url(/wps/userregistration/images/invalid.png) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#ec3f41;
}
.valid {
    background:url(/wps/userregistration/images/valid.png) no-repeat 0 50%;
    padding-left:22px;
    line-height:24px;
    color:#3a7d34;
}
#pswd_info {
    display:none;
}

/* .ur_succesmsg {
    background: rgba(0,0,0,0) url("/wps/userregistration/images/checked.svg") no-repeat scroll 0 0;
    float: right;
    margin-right: 10%;
    width: 6%;
}
.ur_failureImg {
    background: rgba(0,0,0,0) url("/wps/userregistration/images/cancel-button.svg") no-repeat scroll 0 0;
    float: right;
    margin-right: 10%;
    width: 6%;
} */

</style>
 		
 
<script type="text/javascript">
  var errorMessage = {
			currentempty : '<spring:message code="userprofile.currentpwdempty"/>' ,
			newpwsdempty : '<spring:message code="userprofile.newpwdempty"/>' ,
			confirempwsdempty : '<spring:message code="userprofile.confirmpwd"/>' ,
			newpwdinvalid : '<spring:message code="userprofile.newpwdinvalid"/>' ,
			confirmpwdmismatch : '<spring:message code="userprofile.confirmpwdmismatch"/>' 
		  
		
		
	};
	</script>
 		
 		<script>
 		
 		$( document ).ready(function() {
 		
 		var valid = true;
 		var letters = /[A-Z]/;
	
	$('#newPassword').keyup(function() {

		var password = $(this).val();

		if (password.length > 8) {
			$('#length').removeClass('invalid').addClass('valid');
		} else {
			$('#length').removeClass('valid').addClass('invalid');
		}

		if (password.match(/[A-z]/)) {
			$('#letter').removeClass('invalid').addClass('valid');
		} else {
			$('#letter').removeClass('valid').addClass('invalid');
		}

		if (password.match(/[A-Z]/)) {
			$('#capital').removeClass('invalid').addClass('valid');
		} else {
			$('#capital').removeClass('valid').addClass('invalid');
		}

		if (password.match(/\d/)) {
			$('#number').removeClass('invalid').addClass('valid');
		} else {
			$('#number').removeClass('valid').addClass('invalid');
		}
		if (password.indexOf(' ') >= 0) {
  			$('#spaces').removeClass('valid').addClass('invalid');
		}else {
			$('#spaces').removeClass('invalid').addClass('valid');
		}

	}).focus(function() {
		$('#pswd_info').show();
	}).blur(function() {
		$('#pswd_info').hide();
	});
 		
 			
 			$("#currentPassword").on("blur change",function() {
				this.value = this.value.trim();
				$(this).parent().find('.errmsg').remove();
				if ($.trim($("#currentPassword").val()) == "") {
					if ($(this).parent().find('.errmsg').length == 0) {
						$(this).parent().append("<p class='errmsg'>"+errorMessage.currentempty+"</p>");

					}
					valid = valid & false;
					
								}  else {
					$(this).parent().find('.errmsg').remove();
					valid = valid & true;
					
				}
			});
			
			$("#newPassword").on("blur change",function() {
				this.value = this.value.trim();
				$(this).parent().find('.errmsg').remove();
				if ($.trim($("#newPassword").val()) == "") {
					if ($(this).parent().find('.errmsg').length == 0) {
						$(this).parent().append("<p class='errmsg'>"+errorMessage.newpwsdempty+"</p>");

					}
					valid = valid & false;
					
								} 
								else if (!/^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@*!_&#$^=+])(.{8,20})$/
							.test(this.value)) {
						if ($(this).parent().find('errmsg').length == 0) {
							$(this).parent().append(
									"<p class='errmsg'>"
											+ errorMessage.newpwdinvalid + "</p>");
										

						}
						valid = valid && false;
					}
								 else {
					$(this).parent().find('.errmsg').remove();
					valid = valid & true;
					
				}
			});
			
			$("#confirmPassword").on("blur change",function() {
				this.value = this.value.trim();
				$(this).parent().find('.errmsg').remove();
				if ($.trim($("#confirmPassword").val()) == "") {
					if ($(this).parent().find('.errmsg').length == 0) {
						$(this).parent().append("<p class='errmsg'>"+errorMessage.confirempwsdempty+"</p>");

					}
					valid = valid & false;
					
								}  
								else if ($.trim($("#confirmPassword").val()) != $.trim($("#newPassword").val())) {
								if ($(this).parent().find('errmsg').length == 0) {
									$(this).parent().append(
											"<p class='errmsg'>"
													+ errorMessage.confirmpwdmismatch
													+ "</p>");
												

								}
								valid = valid && false;
							}
								
								else {
					$(this).parent().find('.errmsg').remove();
					valid = valid & true;
					
				}
			});
			
			
					$('form').submit(function(event) {
						valid=true;
							console.log("Submit");
							
														
							$('input').blur();
							$('select').blur();
							if(!valid){
							event.preventDefault();
							}
							
			});
 			
 		});
 			function resetForm(){
 			 document.getElementById("myForm").reset();
 			}
 		</script>
 		
     
     <div class="banner-div">
   <div class="inner-banner">
         <h1><spring:message code="changepwsd"/></h1>
         <img src="/wps/contenthandler/dav/fs-type1/themes/PortalEGA/assets/images/portletbanners/contactus-banner.jpg" width="100%"> 
   </div>
</div>

<div class="form-wrapper directioncls">
 		
<div class="container">
 <div class="col-md-12">
 		 <c:if test="${oldpasswdwrong==true}">
     <div class="alert" style="color:red;">
      <spring:message code="userprofile.currentpswmismatch"/> </div> 
      </c:if>
 		 <div class="anonysums">
<h5><spring:message code="changepwsd" /></h5>

    
            
    <form:form action="${changePassword}" method="POST" name="changePassword" commandName="changePwsdCommand" id="myForm">
    		 <div class="row">
    		 
      <div class="col-md-4">
          <div class="input-groupdiv">
            <label><spring:message code="currentpwsd"/></label>
            <form:input type="password" path="currentPassword" id="currentPassword" class="form-control" maxlength="20" />
          </div>
        </div>
         <div class="col-md-4">
          <div class="input-groupdiv">
            <label><spring:message code="newpwsd"/></label>
            <form:input type="password" path="newPassword" id="newPassword" class="form-control" maxlength="20" />
            <div id="pswd_info" class=".arrow_box">
		<h4><spring:message code="password.include"/></h4>
		<ul>
			<!-- <li id="letter" class="invalid">At least <strong>one letter</strong></li>-->
			<li id="capital" class="invalid"><strong><spring:message code="password.one.capital.letter"/></strong></li>
			<li id="number" class="invalid"><strong><spring:message code="password.one.number"/></strong></li>
			<li id="length" class="invalid"><strong>8 - 20 <spring:message code="password.characters"/></strong></li>
			 <li id="spaces" class="invalid"><strong><spring:message code="password.no.spaces"/></strong></li> 
		</ul>
	</div>
            
          </div>
        </div>
         <div class="col-md-4">
          <div class="input-groupdiv">
            <label><spring:message code="confirmpwsd"/></label>
            <form:input type="password" path="confirmPassword" id="confirmPassword" class="form-control" maxlength="20" />
          </div>
        </div>
        
        
        </div>
        
         <div class="col-md-12 text-center"><br>
        <br>
        <a class="btn_cancel" style="padding: 13px 5%;" href="<portlet:renderURL><portlet:param name="action" value=""/>
	                </portlet:renderURL>" class="btn_cancel"><spring:message code="button.back" /></a>
        <input value="<spring:message code="userprofile.reset"/>" class="btn_cancel" type="button" onclick="resetForm()" >
        
        <input value="<spring:message code="userprofile.submit"/>" class="btn_submit" type="submit" >
       
        <br>
        <br>
      </div>  
        
    </form:form>
    
    </div>
 		
 </div>
 </div>
 </div>