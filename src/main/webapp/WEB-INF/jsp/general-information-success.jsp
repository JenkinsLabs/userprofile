<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>User Registration Success</title>
<%
	Locale locale = request.getLocale();
	String alignment = "left";
	String lang = "en";
	if (locale.getLanguage().equalsIgnoreCase("ar")) {
		alignment = "right";
		lang = "ar";
	}
	
%>


	</head>
	<body>
		<form:form  commandName="" name="" method="post" action="#">
		
		
 
      <div class="form_content success">
 
      <div class="form_title">
  
       <h2><spring:message code="user.edit.form" /></h2></div>
       <div class="form_fields_main"> 
       <span class="form_top"> </span> 
         <div class="form_mid">
       
      <p class="mandatory_top"></p>
		<div class="greenTickImage"><img alt="" src="<%=request.getContextPath() %>/images/green-tick-success.png"></div>
		<div class="sucess_msg">
		<h2 align="center"><spring:message code="thank.you"></spring:message></h2>
	 	<h3 align="center" class="activationSuccess"><spring:message code="edit.general.success"></spring:message></h3>
		</div>
	 <div class="success_notary_mid">
	 <aui:button-row>
            <%-- <input type="button" value="<spring:message code="home"/>"  id="buttonName" onClick="location.href='/<%=lang%>/web/rakportal/'"/> --%> 
      </aui:button-row>
	 </div> 
	 	</div>
         <span class="form_bot"> </span>
         
         <p class="homePageNewLink"><a href="/wps/myportal/rak" ><spring:message code="return.to.homepage" /></a></p>
          
         </div>
          </div>

      <div class="form_pagination_search">
        <div class="form_pagination_search_left"></div>
        <div class="form_pagination_search_middle"> </div>
        <div class="form_pagination_search_right"></div>
      </div>
  
		</form:form>
	</body>
</html>