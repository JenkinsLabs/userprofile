<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%
	String pageIndex = (String) request.getAttribute("pageIndex");
	String selectClass = "select";
%>

<portlet:defineObjects />
<portlet:renderURL var="showGeneralInformation">
<portlet:param name="action" value="showGeneralInformation"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="showIdsInformation">
<portlet:param name="action" value="showIdsInformation"></portlet:param>
</portlet:renderURL>


<portlet:renderURL var="showMyProfile">
<portlet:param name="action" value="showMyProfile"></portlet:param>
</portlet:renderURL>


<div class="left-info-bar">
	<ul style="min-height: 327px;margin-top: -2px;">
	      
		<li id="myProfileLinkLi" class="<%=(pageIndex != null && pageIndex.equals("1")) ? selectClass : "" %>"><a id="myProfileLink" href="${showMyProfile}" ><spring:message code="my.profile"/></a></li>
		<li class="<%=(pageIndex != null && pageIndex.equals("2")) ? selectClass : ""%> "><a id="generalLink" href="${showGeneralInformation}" ><spring:message code="update.general.information"/></a></li>
		<li class="<%=(pageIndex != null && pageIndex.equals("3")) ? selectClass : "" %>"><a id="idsLink" href="${showIdsInformation}" ><spring:message code="update.identity.information"/></a></li>
		
		
	</ul>
</div>



