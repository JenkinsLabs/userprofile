<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<div class="banner-div">
      <div class="inner-banner">
   <h1><spring:message code="user.edit.form" /></h1>
    <img src="/wps/contenthandler/dav/fs-type1/themes/PortalEGA/assets/images/portletbanners/contactus-banner.jpg" width="100%"> </div>
</div>
		
		<div class="form-wrapper">
<div class="container">
	
	<div class="form-div">
	<div class="row bottom-mar">
<div class="row">
<div class="thank_you text-center">
      <img src="assets/images/thankyou.png"><br><br>
   		
   		<div class="col-xs-2"></div>
   		<div class="col-xs-8">
   		
   		<p class="color_green" style="text-align:center">
   				<spring:message code="pswdupdated" />
   		</p>
   		
   		
   		<p class="color_green" style="text-align:center">
   				<a href="/wps/myportal/rak" ><spring:message code="return.to.homepage" /></a>
   		</p>
   		
   		</div>
   		    
    </div>
    
    </div>
    </div>
    </div>
	
</div>
</div>