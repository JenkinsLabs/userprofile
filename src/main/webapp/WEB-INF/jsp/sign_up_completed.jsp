<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib  uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<div class="banner-div">
      <div class="inner-banner">
   <h1><spring:message code="user.edit.form" /></h1>
    <img src="/wps/contenthandler/dav/fs-type1/themes/PortalEGA/assets/images/portletbanners/contactus-banner.jpg" width="100%"> </div>
</div>
		
		<div class="form-wrapper">
<div class="container">
	
	<div class="form-div">
	<div class="row bottom-mar">
<div class="row">
<div class="thank_you text-center">
      <img src="assets/images/thankyou.png"><br><br>
   		
   		<div class="col-xs-2"></div>
   		<div class="col-xs-8">
   		
   		<p class="color_green" style="text-align:center">
   				<spring:message code="thank.you" />
   		</p>
   		<p class="color_green" style="text-align:center">
   				<spring:message code="signup.complete"></spring:message><br/> <br/>
   		</p>
   		
   		<p class="color_green" style="text-align:center">
   				<a href="/wps/myportal/rak" ><spring:message code="return.to.homepage" /></a>
   		</p>
   		
   		</div>
   		    
    </div>
    
    </div>
    </div>
    </div>
	
</div>
</div>
		
		
		
     <%--  <div class="form_content success">
      <div class="form_title">
      <c:choose>
      
  <c:when test="${updateProfile== true}">
  <h2><spring:message code="user.edit.form" /></h2>
  </c:when>
  <c:otherwise>
  <h2><spring:message code="user.registration.form" /></h2>
  </c:otherwise>
  </c:choose>
       </div>
       <div class="form_fields_main"> 
        <span class="form_top"> </span> 
        <div class="form_mid">
      <p class="mandatory_top"></p>
		<c:if test="${registration== true}">
		<div class="greenTickImage"><img alt="" src="<%=request.getContextPath() %>/images/green-tick-success.png"></div>
		<div class="sucess_msg">	
		<h2 align="center"><spring:message code="thank.you"></spring:message></h2>
		</div>
		<div class="registrationSuccessmsg">
	 	<h2 align="center" ><spring:message code="registration.complete"></spring:message><br/> <br/>
	 	<c:if test="${dedRegSuccess==true}"><spring:message code="ded.registration.success"></spring:message></c:if><br/><br/>
	 	</h2>
	 	</div>
	 	
	 	
	 	<div class="ded_exception_msg">
	 	<h3 >
	 		<c:if test="${dedRegSuccess==false}"><spring:message code="ded.registration.failure"></spring:message></c:if>
	 	<c:if test="${smsSuccess==false}"><spring:message code="sms.failure"></spring:message></c:if> <br/>
	 	</h3>
	 	
	 	</div>
	 	
	 	</c:if>
	 	<c:if test="${registration== false}">
	 	<div class="exception_msg">
	 	<h2 align="center"><spring:message code="registration.error"></spring:message><br/> <br/>
	 	</h2>
	 	</div>
	 	</c:if>
	 	
	 	<c:if test="${updateProfile== true}">
	 	<div class="greenTickImage"><img alt="" src="<%=request.getContextPath() %>/images/green-tick-success.png"></div>
	 	<div class="sucess_msg">
	 	<h2 align="center"><spring:message code="thank.you"></spring:message></h2>
	 	</div>
	 	<div class="registrationSuccessmsg">
	 	<h2 align="center"><spring:message code="signup.complete"></spring:message><br/> <br/>
	 	</h2>
	 	</div>
	 	</c:if>
	 	</div>
	 	<span class="form_bot"> </span>
	 	<div class="homePage">
		<a href="/web/rakportal"><b><spring:message code="return.to.homepage" /></b></a></div>
		 <p class="homePageNewLink"><a href="/wps/myportal/rak" ><spring:message code="return.to.homepage" /></a></p> 
	 	</div> 
	 	
	 	
         
          </div> --%>

