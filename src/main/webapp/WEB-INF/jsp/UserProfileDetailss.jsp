<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ page import="java.util.Properties"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<script src="<%=request.getContextPath()%>/js/formatter.js"></script> 



<portlet:actionURL var="updateGeneralInformation">
	<portlet:param name="action" value="updateGeneralInformation"></portlet:param>
</portlet:actionURL>

<portlet:renderURL var="resetSignUpPage">
<portlet:param name="action" value="resetSignUpPage"></portlet:param>
</portlet:renderURL>
<portlet:actionURL var="saveUserUpdateRequest">
	<portlet:param name="action" value="saveUserUpdateRequest"></portlet:param>
</portlet:actionURL>
<style>
.arrow_box {
	position: relative;
	background: #88b7d5;
	border: 4px solid #c2e1f5;
}
.arrow_box:after, .arrow_box:before {
	bottom: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(136, 183, 213, 0);
	border-bottom-color: #88b7d5;
	border-width: 30px;
	margin-left: -30px;
}
.arrow_box:before {
	border-color: rgba(194, 225, 245, 0);
	border-bottom-color: #c2e1f5;
	border-width: 36px;
	margin-left: -36px;
}
.userrestric{
border: 0 !important;
}

.barclss{
  background: #999 none repeat scroll 0 0;
    border-radius: 5px;
    margin: 0 auto;
    padding: 5px 0;
    width: 500px;
}
.alert-progress1 .progress1 {
    background: none;
    padding: 10px 0;
    width: 500px;
    margin: 0 auto;
    margin-bottom: 20px;
}
.alert-progress1 .progress1 p {
	color:#333;
	font-size:18px;
	font-weight:normal;
}
.accnt-info-h {
	width:auto !important;
	display:inline-block !important;
}


@media screen and (max-width: 991px) {
.accnt-info-h {
	display:block !important;
	text-align:center !important;
}
.changepswd {
	display:block !important;
	text-align:center !important;
	float:none !important;
}
.alert-progress1 .progress1 {
	width:auto;
}
.alert-progress1 {
	margin-bottom:0 !important;
}
}







</style>
<%
	Locale locale = request.getLocale();
	String alignment = "left";
	String lang = "en";
	if (locale.getLanguage().equalsIgnoreCase("ar")) {
		alignment = "right";
		lang = "ar";
	}

%>
<style>
 <% if(lang.equalsIgnoreCase("en")){%>
 
 .changepswd{float: right;
    background: none;
    color: #0a51ad;
    font-size: 18px;
    text-decoration: underline; padding:10px 0 !important}
     .changepswd:hover,  .changepswd:focus {
     color: #0a51ad !important;
     }
.large.tooltip-inner{width:144px !important; min-width: 144px; }
<%}else{ %>
.large.tooltip-inner{width:144px !important; min-width: 144px; }
.tooltip.right{top:28px !important;}
.tolcls .tooltip-arrow {
    top: 30% !important;
}
.changepswd{float : left !important;}
<%}%>
</style>


<script>
$(document).ready(function() {

 $('#dateOfBirth').datepicker({
   language: '<%=lang%>',
   autoClose:true,
   maxDate: new Date() 
  }); 
  $('#emirateidexpiry').datepicker({
   language: '<%=lang%>',
   autoClose:true,
   minDate: new Date()
  });
   $('#residenceExpireDate').datepicker({
   language: '<%=lang%>',
   autoClose:true,
   minDate: new Date() 
  });

var valid = true;
var isEmirateValid = true;
$("#mobileNo").on("blur change",function() {
				this.value = this.value.trim();
				$(this).parent().find('.errormsg').remove();
				if ($("#mobileNo").val() == "") {
					if ($(this).parent().find('errormsg').length == 0) {
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.mobile+ "</p>");
					}
					valid = valid && false;
				} else if (!/^(([+]?971)|(00971)|0|(0971))[5][0-9]{8,9}$|^$/
						.test(this.value)) {
					if ($(this).parent().find('errormsg').length == 0)
						$(this).parent().append("<p class='errormsg'>"+ errorMessage.invalidMobile+ "</p>");

					valid = valid && false;
				} else {
					$(this).parent().find('.errormsg').remove();
					var result =checkMobileNumberAvailability();
					if(result){
							valid = valid && true;
							}
							else{
							valid = valid && false;
							}
				}
			});

$("#email").on("blur change",function() {
		
			this.value = this.value.trim();
			$(this).parent().find('.errormsg').remove();
			if ($("#email").val() == "") {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.email+ "</p>");

				}
				valid = valid & false;
			} else if (!/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
					.test(this.value)) {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.emailFormat+ "</p>");

				}
				valid = valid && false;
			} else {
				$(this).parent().find('errormsg').remove();
				var result = checkEmailAvailability(); // Ajax call to check the availability
				
				if(result){
							valid = valid && true;
							}
							else{
							valid = valid && false;
							}
			//	isEmailValid = true;
			}
		});
		
		$("#confirmEmail").on("blur change",function() {
			this.value = this.value.trim();
			$(this).parent().find('.errormsg')
					.remove();
			if ($("#confirmEmail").val() == "") {
				if ($(this).parent().find(
						'errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.confirmEmail+ "</p>");

				}
				valid = valid & false;
			} else if ($.trim($("#confirmEmail").val()) != $.trim($("#email").val())) {
				if ($(this).parent().find('errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.invalidConfirmEmail+ "</p>");

				}
				valid = valid && false;
			} else {
				$(this).parent().find('errormsg').remove();
				valid = valid && true;
			}
		});
		$("#nationality").on("blur change",function() {
			this.value = this.value.trim();
			$(this).parent().find('.errormsg')
					.remove();
			if ($("#nationality").val() == -1) {
				if ($(this).parent().find(
						'errormsg').length == 0) {
					$(this).parent().append("<p class='errormsg'>"+ errorMessage.nationality+ "</p>");

				}
				valid = valid & false;
			} else {
				$(this).parent().find(
						'errormsg').remove();
				valid = valid && true;
			}
		});
		$('#emiratesId').on('blur change', function() {
			 			if($('#nationality').val()!='-1'){
			 			this.value = this.value.trim();
						$(this).parent().find('.errormsg').remove();
			      		if(this.value == ''){
							   if($(this).parent().find('.errormsg').length == 0)
					    		$(this).parent().append("<p class='errormsg'>"+errorMessage.emirateId+"</p>");
					    		/* $(this).parent().find('#fail').css("display","block");
					    		$(this).parent().find('#success').css("display","none"); */
							   valid = valid && false;
							}else if(!isEmirateId(this.value)){
								if($(this).parent().find('.errormsg').length == 0)
					    		$(this).parent().append("<p class='errormsg'>"+errorMessage.invalidEmirateId+"</p>");
					    		/* $(this).parent().find('#fail').css("display","block");
					    		$(this).parent().find('#success').css("display","none"); */
						    	valid = valid && false;
						    }else{
			 		    	$(this).parent().find('.errormsg').remove();
			 		    	var result = checkEmirateIDAvailability();
			 		    	/* $(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
							if(result){
							valid = valid && true;
							}
							else{
							valid = valid && false;
							}
							}
			 			}else{
			 				
			 				if($.trim($('#extraEmirateCheckId').val())!=$.trim($('#emiratesId').val())){
			 					$('#emirateidexpiry').val("");
			 					$("#emirateidexpiry").parent().append("<p class='errormsg'>"+errorMessage.emirateExpiryDate+"</p>");
			 				}
			 				
			 		    	$(this).parent().find('.errormsg').remove();
			 		    /* 	$(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
			 		    	valid = valid && true;
			 			}
			 			
			 		});
			 		
			 		$('#emirateidexpiry').on('blur change', function() {
						if($('#nationality').val()!='-1'){
						this.value = this.value.trim();
					$(this).parent().find('.errormsg').remove();
						
						if(this.value == ''){
						   if($(this).parent().find('.errormsg').length == 0)
				    		$(this).parent().append("<p class='errormsg' style='position:absolute;left:0;bottom:-18px'>"+errorMessage.emirateExpiryDate+"</p>");
				    		/* $(this).parent().find('#fail').css("display","block");
				    		$(this).parent().find('#success').css("display","none"); */
						   valid = valid && false;
						}
						else{
					    	$(this).parent().find('.errormsg').remove();
					    	/* $(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
					    	valid = valid && true;
					    }
						}else{
					    	$(this).parent().find('.errormsg').remove();
					   /*  	$(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
					    	valid = valid && true;
					    }
						
					});
					
					$('#emirateAttachement').on('blur change', function() {  
						if($('#nationality').val()!='-1'){
							if(this.value == ''){
				    		
							console.log("--------residence attachment********   "+$(this).parent().find('#link_color').length );
								console.log("--------temp file length********   "+$(this).parent().find('#temp_file').length );
								
								//if($(this).parent().find('#temp_file').length ==0 && this.value == 0 ){
				    			if($(this).parent().find('#link_color').length ==0 && this.value == 0 ){
				    				console.log('inside link color');
				    				if($(this).parent().find('.errormsg').length == 0)
				    		    		$(this).parent().append("<p class='errormsg'>"+errorMessage.idCopyAttachement+"</p>");
				    		    	/* 	$(this).parent().find('#fail').css("display","block");
				    		    		$(this).parent().find('#success').css("display","none"); */
				    		    	valid = valid && false;
				    		    } 	
							}
							
							else if( !isFileSizeOK('emirateAttachement')){      				
								$(this).parent().find('.errormsg').remove();
						    	if($(this).parent().find('.errormsg').length == 0)
						    		$(this).parent().append("<p class='errormsg'>"+errorMessage.attachmentType+"</p>");
						    		/* $(this).parent().find('#fail').css("display","block");
						    		$(this).parent().find('#success').css("display","none"); */
						    		valid = valid && false;
						    }else if( !isFileLength('emirateAttachement')){     				
						    	$(this).parent().find('.errormsg').remove();
						    	if($(this).parent().find('.errormsg').length == 0)
						    		$(this).parent().append("<p class='errormsg'>"+errorMessage.fileLength+"</p>");
						    		/* $(this).parent().find('#fail').css("display","block");
						    		$(this).parent().find('#success').css("display","none"); */
						    		valid = valid && false;
						    		}      			
							else{
						    	$(this).parent().find('.errormsg').remove();
						    	/* $(this).parent().find('#fail').css("display","none");
								$(this).parent().find('#success').css("display","block"); */
						    	valid = valid && true;
						    }
						}else{
					    	$(this).parent().find('.errormsg').remove();
					    	/* $(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
					    	valid = valid && true;
					    }
						
						});
						
					$('#residenceId').on('blur change', function() {
						if($('#nationality').val()!='217' && $('#nationality').val()!='-1'){
						this.value = this.value.trim();
					$(this).parent().find('.errormsg').remove();
						
						if(this.value == ''){
						   if($(this).parent().find('.errormsg').length == 0)
				    		$(this).parent().append("<p class='errormsg'>"+errorMessage.residenceId+"</p>");
				    		/* $(this).parent().find('#fail').css("display","block");
				    		$(this).parent().find('#success').css("display","none"); */
						   valid = valid && false;
						}else if(!/^[A-Za-z0-9\u0600-\u06FF.]+$/.test(this.value)){
						//else if(!/^(?![0-9]*$)[a-zA-Z0-9._-]+$/.test(this.value)){
							if($(this).parent().find('.errormsg').length == 0)
				    		$(this).parent().append("<p class='errormsg'>"+errorMessage.invalidResidenceId+"</p>");
				    		/* $(this).parent().find('#fail').css("display","block");
				    		$(this).parent().find('#success').css("display","none"); */
					    	valid = valid && false;
					    }
						else{
					    	$(this).parent().find('.errormsg').remove();
					    	var result = checkResidenceIDAvailability();
					    	/* $(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
							if(result){
							valid = valid && true;
							}
							else{
							valid = valid && false;
							}
					    	
					    }
						
						}else{
					
				    	$(this).parent().find('.errormsg').remove();
				 /*    	$(this).parent().find('#fail').css("display","none");
						$(this).parent().find('#success').css("display","block"); */
				    	valid = valid && true;
				    }
					});
					
					$('#residenceExpireDate').on('blur change', function() {
						if($('#nationality').val()!='217' && $('#nationality').val()!='-1'){
						this.value = this.value.trim();
					$(this).parent().find('.errormsg').remove();
						
						if(this.value == ''){
						   if($(this).parent().find('.errormsg').length == 0)
				    		$(this).parent().append("<p class='errormsg' style='position:absolute;left:0;bottom:-18px'>"+errorMessage.residenceExpireDate+"</p>");
				    	/* 	$(this).parent().find('#fail').css("display","block");
				    		$(this).parent().find('#success').css("display","none"); */
						   valid = valid && false;
						}
						else{
					    	$(this).parent().find('.errormsg').remove();
					    /* 	$(this).parent().find('#fail').css("display","none");
							$(this).parent().find('#success').css("display","block"); */
					    	valid = valid && true;
					    }
						
						}else{
					
				    	$(this).parent().find('.errormsg').remove();
				    	/* $(this).parent().find('#fail').css("display","none");
						$(this).parent().find('#success').css("display","block"); */
				    	valid = valid && true;
				    }
					});	
					
					$('#residenceAttachement').on('blur change', function() {  
						if($('#nationality').val()!='217' && $('#nationality').val()!='-1' ){
							
							if(this.value == ''){
								console.log("--------residence attachment********   "+$(this).parent().find('#link_color').length );
								console.log("--------temp file length********   "+$(this).parent().find('#temp_file').length );
								
								//if($(this).parent().find('#temp_file').length ==0 && this.value == 0 ){
				    			if($(this).parent().find('#link_color').length ==0 && this.value == 0 ){
				    				console.log('inside link color');
				    				if($(this).parent().find('.errormsg').length == 0)
				    		    		$(this).parent().append("<p class='errormsg'>"+errorMessage.residenceCopyAttachement+"</p>");
				    		    	/* 	$(this).parent().find('#fail').css("display","block");
				    		    		$(this).parent().find('#success').css("display","none"); */
				    		    	valid = valid && false;
				    		    } 
				    		    
				    		    //}
								else{
									$(this).parent().find('.errormsg').remove();
									/* $(this).parent().find('#fail').css("display","none");
									$(this).parent().find('#success').css("display","block"); */
							    	valid = valid && true;
								}
							}
							
							else if( !isFileSizeOK('residenceAttachement')){      				
								$(this).parent().find('.errormsg').remove();
						    	if($(this).parent().find('.errormsg').length == 0)
						    		$(this).parent().append("<p class='errormsg'>"+errorMessage.attachmentType+"</p>");
						    	/* 	$(this).parent().find('#fail').css("display","block");
						    		$(this).parent().find('#success').css("display","none"); */
						    		valid = valid && false;
						    }else if( !isFileLength('residenceAttachement')){     				
						    	$(this).parent().find('.errormsg').remove();
						    	if($(this).parent().find('.errormsg').length == 0)
						    		$(this).parent().append("<p class='errormsg'>"+errorMessage.fileLength+"</p>");
						    	/* 	$(this).parent().find('#fail').css("display","block");
						    		$(this).parent().find('#success').css("display","none"); */
						    		valid = valid && false;
						    		}      			
							else{
						    	$(this).parent().find('.errormsg').remove();
						    //	$(this).parent().find('#fail').css("display","none");
							//	$(this).parent().find('#success').css("display","block");
						    	valid = valid && true;
						    }
							 
						}else{
					    	$(this).parent().find('.errormsg').remove();
					    //	$(this).parent().find('#fail').css("display","none");
						//	$(this).parent().find('#success').css("display","block");
					    	valid = valid && true;
					    }
						});
					
		

$("#firstName").on( "blur change", function() {
	
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();

    if ($("#firstName").val() == "") {
   	
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.firstName+"</p>");
          }
          valid=valid&false;
    }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
   	
        if($(this).parent().find('errormsg').length == 0){
            $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
        }
        valid = valid && false;              
    }else{
   	
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });

$("#secondName").on("blur change", function() {
	
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();
    if($("#secondName").val()==""){
   	
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.secondName+"</p>");
          }
          valid=valid&false;
    }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
   	
        if($(this).parent().find('errormsg').length == 0){
            $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
        }
        valid = valid && false;              
    }else{
   	
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });

$("#familyName").on("blur change", function() {
		
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();
    if($("#familyName").val()==""){
   	
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.familyName+"</p>");
          }
          valid=valid&false;
    }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
   	
        if($(this).parent().find('errormsg').length == 0){
            $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
        }
        valid = valid && false;              
    }else{
   	
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });


$("#dateOfBirth").on("blur change", function() {
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();
    if($("#dateOfBirth").val()==""){
   	
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.dateOfBirth+"</p>");
          }
          valid=valid&false;
    }else{
   	
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });

$("#maritalstatus").on("blur change", function() {
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();
    if($("#maritalstatus").val()==-1){
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.maritalStatus+"</p>");
          }
          valid=valid&false;
    }else{
   	
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });
$("#gender").on("blur change", function() {
    this.value = this.value.trim();
    $(this).parent().find('.errormsg').remove();
    if($("#gender").val()==-1){
        if($(this).parent().find('errormsg').length == 0){
              $(this).parent().append("<p class='errormsg'>"+errorMessage.gender+"</p>");
          }
          valid=valid&false;
    }else{
   
      $(this).parent().find('errormsg').remove();
      valid = valid && true;
    }
  });

$('form').submit(function(event) {

			valid = true;
			$('input').blur();
			$('select').blur();
			
			if (valid == false) {
						event.preventDefault();
				}
			
		});
		
		
		
			
		});

</script>

<script type="text/javascript">
  var errorMessage = {
			firstName : '<spring:message code="Empty.FamilyName"/>' ,
			firstNameFormat : '<spring:message code="error.firstNameFormat"/>' ,
			familyName : '<spring:message code="Empty.FamilyName"/>',
			dateOfBirth : '<spring:message code="Empty.DateOfBirth"/>' ,
			secondName: '<spring:message code="Empty.SecondName"/>' ,
			maritalStatus:'<spring:message code="error.marital.status"/>',
			gender:'<spring:message code="error.gender"/>',
			regType : '<spring:message code="error.select.registration"/>' ,
			nationality : '<spring:message code="error.select.nationality"/>' ,
			email : '<spring:message code="error.email"/>' ,
			emailFormat : '<spring:message code="error.emailFormat"/>' ,
			confirmEmail : '<spring:message code="error.confirmEmail"/>' ,
			mobile : '<spring:message code="error.mobileNo"/>' ,
			idExpireDate : '<spring:message code="error.idExpireDate"/>' , 
			idCopyAttachement : '<spring:message code="error.idCopyAttachement"/>' ,
			emiratesID : '<spring:message code="error.emiratesID"/>' ,
			passportNo : '<spring:message code="error.passportNo"/>' ,
			passportExpiredate : '<spring:message code="error.passportExpiredate"/>' ,
			passportCopyAttachement : '<spring:message code="error.passportCopyAttachement"/>' ,
			residenceId : '<spring:message code="error.residenceId"/>' ,
			invalidResidenceId: '<spring:message code="error.invalid.residenceId"/>' ,
			residenceExpiredate : '<spring:message code="error.residenceExpiredate"/>' , 
			residenceCopyAttachement : '<spring:message code="error.residenceCopyAttachement"/>' ,
			emirateExpiryDate: '<spring:message code="error.id.expiry.date"/>' ,
			residenceExpireDate: '<spring:message code="error.residence.id.expiry.date"/>' ,
			passportExpiryDate: '<spring:message code="error.passport.expiry.date"/>' ,
			passportAttachment:'<spring:message code="error.passportAttachment"/>' ,
			fileLength:'<spring:message code="error.invalid.file.length"/>',
			attachmentType:'<spring:message code="error.attachment.type"/>',
			invalidMobile:'<spring:message code="invalid.mobile"/>',
			invalidPassport:'<spring:message code="invalid.passport"/>',
			emirateId:'<spring:message code="error.emirateId"/>',
			invalidEmirateId:'<spring:message code="error.invalid.emirateId"/>',
			terms:'<spring:message code="Empty.Terms"/>',
			invalidConfirmEmail: '<spring:message code="invalid.error.confirmEmail"/>' ,
			invalidConfirmPassword:'<spring:message code="invalid.error.confirmPassword"/>' ,
			emptyPassword:'<spring:message code="empty.error.confirmPassword"/>' ,
			veryWeak:'<spring:message code="error.very.weak"/>' ,
			emailExist:'<spring:message code="email.exist"/>',
			passwordInclude:'<spring:message code="password.include"/>',
			characters:'<spring:message code="password.characters"/>',
			oneCapitalLetter:'<spring:message code="password.one.capital.letter"/>',
			oneNumber:'<spring:message code="password.one.number"/>',
			noSpaces:'<spring:message code="password.no.spaces"/>',
			emailIdExist:'<spring:message code="emirate.id.exist"/>',
			residenceIdExist:'<spring:message code="residence.id.exist"/>',
			mobileNumberExist:'<spring:message code="mobile.exist"/>',
			
		
	};
	</script>
	
	<script type="text/javascript">
	 $( document ).ready(function() {  
	<c:if test="${not empty displayMode}">
		$("#firstName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#secondName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#familyName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#dateOfBirth").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#gender").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#maritalstatus").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#email").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#confirmEmail").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#mobileNo").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#nationality").css('cursor','not-allowed').prop('disabled', 'disabled');
		$('#citizenExpatFieldsetId').find('input,radiobutton').prop('disabled', 'disabled');
		$('#formsubmitid').prop('disabled', 'disabled');
	</c:if>
	
	if($("#emiratesId").val()!=null){
	var formatted = new Formatter(document.getElementById('emiratesId'), {
		  'pattern': '{{999}}-{{9999}}-{{9999999}}-{{9}}'
		});
}



});


function isEmirateId(emiratsId) {
	var re = /^(784)-[0-9]{4}-[0-9]{7}-[0-9]{1}$|^$/;
	return re.test(emiratsId);
}

function isFileSizeOK(fileNodeID) {
	var fileNode = document.getElementById(fileNodeID);
	if (navigator.appName == "Microsoft Internet Explorer"
			&& navigator.appVersion.indexOf("MSIE 9.0")) {
	} else {
		for (var i = 0; i < fileNode.files.length; i++) {
			if (fileNode.files[i].size > 2097152) {
				return false;
			}
		}
	}
	return (/(\.(gif|jpg|jpeg|png|pdf|doc|docx|pdf)$)|^$/i)
			.test(fileNode.value);
}
function isFileLength(fileNodeID) {
	var fileNode = document.getElementById(fileNodeID);
	var fileName = fileNode.value;
	if ((navigator.appName == "Microsoft Internet Explorer" && (navigator.appVersion
			.indexOf("MSIE 9.0") || navigator.appVersion
			.indexOf("MSIE 10.0") > -1))
			|| (navigator.appName == "Netscape" && navigator.appVersion
					.indexOf("Trident/7.0") > -1)) {
		fileName = fileName.substring(fileName.lastIndexOf('\\') + 1,
				fileName.length);
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
	} else {
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
	}
	return fileName.length < 45;
}

function clearAttachment(nodeID) {
	console.log("--clear attachment--");
		    var control = $("#"+nodeID);
		    console.log('control--'+control);
		    if(isMSIE()){
		                    control.replaceWith( control = control.clone( true ) );
		            		emirateValidation();
		            		residentValidation();
		    }else{
		    	console.log('---else---');
		                    control.val('');
		    }
}

function checkEmirateIDAvailability(){
	console.log("---checkEmirateIDAvailability---"+id);
	var emirateId=document.getElementById("emiratesId").value;
	var extraEmirateCheck=document.getElementById('extraEmirateCheckId').value;
	var id = "emiratesId";
	var emiresult = 1;
	
	/* else{
	//alert("---else condition---");
	var extraEmirateCheck=document.getElementById('extraEmirateCheckGCCId').value;
	} */
	if(emirateId!=extraEmirateCheck){
	  $("#emiratesId").parent().find('.errormsg,.succesmsg').remove(); 
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkEmirateIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	emirateId: emirateId
        },
        success: function( data ) {
        	 console.log('id ------'+id);
        	if(data.isEmirateIdAvailable){
        		console.log('Emirate Id exists');
        		if($('#'+id).parent().find('.errormsg').length == 0){
        			$('#'+id).parent().append("<p class='failureImg	 emirateIdFailure'></p>");
        		$('#'+id).parent().append("<p class='errormsg'>"+data.emirateId+" <spring:message code='emirate.id.exist'/></p>");
        		}
        		isEmirateValid = false;
        		emiresult = 0;
        	}else{
        		console.log('Emirate Id doesnot exists');
        		if($('#'+id).parent().find('.errormsg').length == 0)
        		 $('#'+id).parent().append("<p class='succesmsg'>"+data.emirateId+" <spring:message code='emirate.id.doesnot.exist'/></p>"); 
        		/* $('#'+id).parent().append("<p class='succesmsg emirateIdSuccess'></p>"); */
        		isEmirateValid = true;
        		
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	console.log("some error in-----checkEmirateIDAvailability-------"+textStatus);
        }
      });
	}else{
		isEmirateValid = true;
	}
	return emiresult;
}



function checkResidenceIDAvailability(){
	console.log("---checkResidenceIDAvailability---");
	var residenceId=document.getElementById('residenceId').value;
	var extraResidenceCheck=document.getElementById('extraResidenceCheckId').value;
	console.log('residenceId--'+residenceId);
	var resiresult = 1;
	  $("#residenceId").parent().find('.errormsg,.succesmsg').remove(); 
	if(residenceId!=extraResidenceCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkResidenceIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	residenceId: residenceId
        },
        success: function( data ) {
        	 console.log('id ------'+data.isresidenceIdAvailable);
        	if(data.isresidenceIdAvailable){
        		console.log('Residence Id exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0){
        			/* $("#residenceId").parent().append("<p class='ur_failureImg emirateIdFailure'></p>"); */
        		$("#residenceId").parent().append("<p class='errormsg'>"+data.residenceId+" <spring:message code='residence.id.exist'/></p>");
        		}
        		isResidenceValid = false;
        		resiresult = 0;
        	}else{
        		console.log('Residence Id doesnot exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0)
        		 $("#residenceId").parent().append("<p class='succesmsg'>"+data.residenceId+" <spring:message code='residence.id.doesnot.exist'/></p>"); 
        		/* $("#residenceId").parent().append("<p class='succesmsg'></p>"); */
        		isResidenceValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		isResidenceValid = true;
	} 
	return resiresult;
}
 

function checkEmailAvailability(){
	var email=document.getElementById('email').value;
	var extraEmailCheck=document.getElementById('extraEmailCheckId').value;	
   console.log('email'+email);
   console.log('extraEmailCheck'+extraEmailCheck);  
   var emailResult = 1;
    $("#email").parent().find('.errormsg,.succesmsg').remove(); 
	if(email!=extraEmailCheck){
	//$( "#userName" ).parent().find('.ur_errormsg,.ur_succesmsg').remove();
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMailIdAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	email: email
        },
        success: function( data ) {        	
	        //	alert(data.isEmailIdAvailable);
	        	//if(data.value[0]){
	        	if(data.isEmailIdAvailable){
	        		if($("#email").parent().find('.errormsg').length == 0){
	        			/* $("#email").parent().append("<p class='errormsg'></p>"); */
	        		$("#email").parent().append("<p class='errormsg'>"+data.eMailId+" <spring:message code='email.exist'/></p>");
	        		}
	        		isEmailValid = false;
	        		emailResult = 0;
	        	}else{
	        		if($("#email").parent().find('.errormsg').length == 0)
	        		 $("#email").parent().append("<p class='succesmsg'>"+data.eMailId+" <spring:message code='email.doesnot.exist'/></p>"); 
	        		 isEmailValid = true;       		
	        		
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}
	return emailResult;
}



function checkMobileNumberAvailability(){
	var mobileNo=document.getElementById('mobileNo').value;
	var extraMobileCheck=document.getElementById('extraMobileCheckId').value;
	var index1 = extraMobileCheck.indexOf('5');
    var phoneSubString = extraMobileCheck.substring(index1);	
	var format1="0971"+phoneSubString;
	var format2="00971"+phoneSubString;
	var format3="971"+phoneSubString;
	var format4="0"+phoneSubString;
	var mobileResult = 1;
   console.log('mobileNo'+mobileNo);
   console.log('extraMobileCheck'+extraMobileCheck);
   $("#mobileNo").parent().find('.errormsg,.succesmsg').remove();
	/* if(mobileNo!=extraMobileCheck){ */
		if ( [ format1, format2, format3,format4 ].indexOf( mobileNo ) == -1 ) { 
	   //alert("------not matching--------------");
		console.log("-------inside mobile number ajax calling-------------");
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMobileNumberAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	mobileNo: mobileNo
        },
        success: function( data ) {        	
	        	console.log("----------data value-----"+data.isMobileAvailable);
	        	if(data.isMobileAvailable){
	        		console.log("--------mobileNo--exists-------");
	        		if($("#mobileNo").parent().find('errormsg').length == 0){
	        		/* 	$("#mobileNo").parent().append("<p class='errormsg'></p>"); */
	        		$("#mobileNo").parent().append("<p class='errormsg'>"+data.mobileNo+" <spring:message code='mobile.exist'/></p>");
	        		}
	        		isMobileValid = false;
	        		mobileResult = 0;
	        	}else{
	        		console.log("--------mobileNo-not-exists-------");
	        		if($("#mobileNo").parent().find('.errormsg').length == 0)
	        		 $("#mobileNo").parent().append("<p class='succesmsg'>"+data.mobileNo+" <spring:message code='mobile.doesnot.exist'/></p>"); 
	        		/* $("#mobileNo").parent().append("<p class='succesmsg'></p>"); */
	        		isMobileValid = true;
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}else{
		isMobileValid = true;
	}
	return mobileResult;
}

function checkPassportNoAvailability(){
	console.log("---checkPassportNoAvailability---");
	var passportNo=document.getElementById('passportNo').value;
	var extraPassportCheck=document.getElementById('extraPassportNoCheckId').value;
	console.log('passportNo--'+passportNo);
	 $("#passportNo").parent().find('.errormsg,.succesmsg').remove();
	if(passportNo!=extraPassportCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkPassportNoAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	passportNo: passportNo
        },
        success: function( data ) {
        	 console.log('id ------'+data.isPassportNoAvailable);
        	if(data.isPassportNoAvailable){
        		console.log('Passport Id exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0){
        		/* 	$("#passportNo").parent().append("<p class='errormsg'></p>"); */
        		$("#passportNo").parent().append("<p class='errormsg'>"+data.passportNo+" <spring:message code='passport.no.exist'/></p>");
        		}
        		isPassportValid = false;
        	}else{
        		console.log('passportNo doesnot exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0)
        		$("#passportNo").parent().append("<p class='succesmsg'>"+data.passportNo+" <spring:message code='passport.id.doesnot.exist'/></p>");
        	
        		isPassportValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		 isPassportValid = true;
	} 
}

	function showSelectType(value){
        	 console.log("--------value-----------"+value.value);
      	   if(value.value == "217"){
 				$("#citizenId").css("display", "block");
		$("#expatId").css("display", "none");
		$("#citizenExpatFieldsetId").css("display", "block");
	            
 			  } 
      	   else if(value.value == -1){
				$("#citizenId").css("display", "none");
			$("#expatId").css("display", "none");
			$("#citizenExpatFieldsetId").css("display", "none");		
			} else{
	       		$("#citizenId").css("display", "block");
		$("#expatId").css("display", "block");
		$("#citizenExpatFieldsetId").css("display", "block");
 			}
	}
	
	
function load() {
				var nationalityType = document.getElementById("nationality").value;
				
				 if(nationalityType == "217"){
	   				$("#citizenId").css("display", "block");
		$("#expatId").css("display", "none");
		$("#citizenExpatFieldsetId").css("display", "block");             
	   	       	
	   			}else if(nationalityType == -1){
	   				$("#citizenId").css("display", "none");
			$("#expatId").css("display", "none");
			$("#citizenExpatFieldsetId").css("display", "none");
				}else{
					$("#citizenId").css("display", "block");
		$("#expatId").css("display", "block");
		$("#citizenExpatFieldsetId").css("display", "block");
	   			}
				
			}
			
		
			
</script>
<div class="banner-div">
   <div class="inner-banner">
        	<h1><spring:message code="userprofile.banner"/></h1>
         <img src="/wps/contenthandler/dav/fs-type1/themes/PortalEGA/assets/images/portletbanners/contactus-banner.jpg" width="100%"> 
   </div>
</div>
<body onload=load()>

<div class="form-wrapper directioncls" style="padding:0;">

<form:form id="userSignUpForm" name="userSignUp" commandName="signUpBean" method="post"
			action="${saveUserUpdateRequest}" enctype="multipart/form-data" autocomplete="off">
			<form:hidden name="buttonName" path="buttonName" id="buttonNameId" />
			<form:hidden path="otpRedirectUrl" value="${redirectUrl}" />
			<form:hidden path="emirateFileId" value="${signUpBean.emirateFileId}"/>
			<form:hidden path="residenceFileId" value="${signUpBean.residenceFileId}"/>
			<form:hidden path="passportFileId" value="${signUpBean.passportFileId}"/>
			<form:hidden path="egaUserExtId" value="${signUpBean.egaUserExtId}"/>
			<form:hidden id="nationalityValue" path="nationalityValue"/>
			<form:hidden id="genderValue" path="genderValue"/>
			<form:hidden id="maritalStatusValue" path="maritalStatusValue"/>			
			<form:hidden  id="userNameEdit" path="userName" value="${signUpBean.userName}"/> 
			<form:hidden  id="userID" path="userId" value="${signUpBean.userId}"/> 						
			<form:hidden id="extraEmailCheckId" path="extraEmailCheck" value="${signUpBean.email}"/>
			<form:hidden id="extraEmirateCheckId" path="extraEmirateCheck" value="${signUpBean.emiratesId}"/>
			<form:hidden id="extraEmirateCheckGCCId" path="extraEmirateCheckGCC" value="${signUpBean.emirateIdGCC}"/>
			<form:hidden id="extraResidenceCheckId" path="extraResidenceCheck" value="${signUpBean.residenceId}"/>
			<form:hidden id="extraMobileCheckId" path="extraMobileCheck" value="${signUpBean.mobileNo}"/>
			<form:hidden id="extraPassportNoCheckId" path="extraPassportCheck" value="${signUpBean.passportNo}"/>
			<form:hidden id="emirateexpiridate" path="extraPassportCheck" value="${signUpBean.emirateExpireDate}"/>
			<form:hidden id="residenceexpdate" path="extraPassportCheck" value="${signUpBean.residenceExpireDate}"/>

		 <div class="">
      <div class="">
        <div class="registration">
        <c:if test="${not empty displayMode}">
        <div class="alert-progress1 userrestric">
			<div class="progress1 barclss">
				<p><spring:message code="update.request.progress"></spring:message></p>
			</div>
		</div>

	</c:if>
	
          
          	
          	 <div class="row registration-inner">
          	 	
          	 		<div class="col-md-12">
          	 		<h5 class="accnt-info-h"><spring:message code = "user.AccountInfo" /></h5>
          	 	<a class="btn_submit changepswd" style="padding: 13px 5%;" href="<portlet:renderURL><portlet:param name="action" value="ChangePassword"/>
	                </portlet:renderURL>" class="btn_cancel"><spring:message code="changepwsd" /></a>	
              <div class="input-groupdiv">
               <label><spring:message code="user.name" /></label>
                <input type="text" maxlength="30" 
						 id="userName" class="form-control" tabindex="1"
						autocomplete="off" disabled="disabled" style="cursor: not-allowed;" value="${signUpBean.userName}"/>
				
              </div>
            </div>
             <div class="col-md-12">
              <div class="input-groupdiv">
              <label><spring:message code="email" /><span>*</span>
                <form:input type="text" maxlength="45" name="email" path="email" id="email" class="form-control" tabindex="2" />
              <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="email.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information"aria-hidden="true"> </i>
</a>
              </label>
			
						
					<form:errors path="email" class="errormsg" />
               <%--  <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
             <div class="col-md-12">
              <div class="input-groupdiv">
              <label><spring:message code="confirm.email" /><span>*</span>
                <form:input type="text" maxlength="45" name="confirmEmail"
						path="confirmEmail" id="confirmEmail" class="form-control"
						tabindex="3" oncopy="return false" onpaste="return false" />
              <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="confirm.email.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information"aria-hidden="true"> </i>
</a>
              </label>
			
						
					<form:errors path="confirmEmail" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
          	 		
          	 </div>
          	 
          	 <hr>
          	<h5><spring:message code = "user.PersonalInfo" /></h5>
          <div class="row registration-inner">
          	<div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="nationality" /> <span>*</span> </label>
                <form:select id="nationality" path="nationality"
						class="selectpicker show-tick form-control" onchange="showSelectType(this)" tabindex="4">

						<form:option value="-1">
							<spring:message code="select" />
						</form:option>
						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${countryList}" itemValue="id"
							itemLabel="descriptionEnglish" />
						<%
							} else {
						%>
						<form:options items="${countryList}" itemValue="id"
							itemLabel="descriptionArabic" />
						<%
							}
						%>
					</form:select>
					<form:errors path="nationality" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            
            <div class="col-md-12">
              <div class="input-groupdiv">
              <label><spring:message code="gender" /> <span> *</span> </label>
			   <form:select id="gender" path="gender" class="selectpicker show-tick form-control"
						tabindex="5">
						<form:option value="-1">
							<spring:message code="select" />
						</form:option>
						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${genderList}" itemValue="genderId"
							itemLabel="genderEn" />
						<%
							} else {
						%>
						<form:options items="${genderList}" itemValue="genderId"
							itemLabel="genderAr" />
						<%
							}
						%>
					</form:select>
					<form:errors path="gender" class="errormsg" />
			<%-- 	<div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="marital.status" /> <span> *</span> </label>
			 <form:select id="maritalstatus" path="maritalStatus"
						class="selectpicker show-tick form-control" tabindex="6">

						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${maritalStatusList}"
							itemValue="maritalStatusId" itemLabel="maritalStatusEn" />
						<%
							} else {
						%>
						<form:options items="${maritalStatusList}"
							itemValue="maritalStatusId" itemLabel="maritalStatusAr" />
						<%
							}
						%>

					</form:select>
					<form:errors path="maritalStatus" class="errormsg" />
					<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
             <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="first.name" /><span>*</span>
                <form:input type="text" maxlength="35" name="firstName"
						path="firstName" id="firstName" class="form-control" tabindex="7" />
               <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="firstName.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information" aria-hidden="true"> </i>
</a>
               </label>
			 
						
					<form:errors path="firstName" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="second.name" /><span>*</span>
                <form:input type="text" maxlength="35" name="secondName"
						path="secondName" id="secondName" class="form-control"
						tabindex="8" />
               <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="secondName.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information" aria-hidden="true"> </i>
</a>
               
               </label>
			 
						
					<form:errors path="secondName" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="family.name" /><span>*</span>
                <form:input type="text" maxlength="35" name="familyName"
						path="familyName" id="familyName" class="form-control"
						tabindex="9" />
               <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="familyName.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information" aria-hidden="true"> </i>
</a>
               </label>
               
						
					<form:errors path="familyName" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-groupdiv tolcls">
              <label><spring:message code="mobile" /><span>*</span>
              <form:input type="text" maxlength="14" name="mobileNo"
						path="mobileNo" id="mobileNo" class="form-control"
						tabindex="10" />
              <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="mobile.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information"aria-hidden="true"> </i>
</a>
               </label>
                
						
					<form:errors path="mobileNo" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
            <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="dob" /><span>*</span> </label>
               <!--  <div class='input-group date' id='datetimepicker1'> -->
                    <form:input type="text" name="dateOfBirth" placeholder="DD/MM/YYYY" path="dateOfBirth"
										id="dateOfBirth" readonly="true" class="form-control" tabindex="11" data-date-format="dd/mm/yyyy"/>
					<form:errors path="dateOfBirth" class="errormsg" />
                   <!--  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
                <!-- </div> -->
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
            </div>
          	
          </div>
          
          </div>
          <c:set var="emirid"><spring:message code="emirates.id" /></c:set>
            <c:set var="emirattach"><spring:message code="copy.attachement" /></c:set>
          <div id="citizenExpatFieldsetId" style="display: none">
			<div class="ids-info">
			<h5><spring:message code="id.information" /></h5>
				<div class="row registration-inner">
					<div id="citizenId" style="display: block;">
					
						<div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="emirates.id" /><span>*</span>
                <form:input type="text" name="emiratesId" placeholder="${emirid}" path="emiratesId"
								id="emiratesId" class="form-control" tabindex="12" maxlength="20" />
               
               <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="emirate.id.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information"aria-hidden="true"> </i>
</a>
                </label>
			 
				<form:errors path="emiratesId" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
        </div>
        
        <div class="col-md-12">
              <div class="input-groupdiv">
			 
			  <label><spring:message code="expire.date" /><span>*</span> </label>
			  <!--  <div class='input-group date' id='datetimepicker2'> -->
			  <form:input type="text" name="emirateExpireDate"
								path="emirateExpireDate" id="emirateidexpiry" placeholder="DD/MM/YYYY" 
								class="form-control" readonly="true" tabindex="13" data-date-format="dd/mm/yyyy"/>
							<form:errors path="emirateExpireDate" class="errormsg" />
					<!-- <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
				<!-- </div> -->
              </div>
        </div>
        <div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="copy.attachement" /><span>*</span> </label>
               <form:input type="file" maxlength="35" placeholder='${emirattach}' name="emirateAttachement"
								path="emirateAttachement" id="emirateAttachement"
								class="inputfile inputfile-2" data-multiple-caption="{count} files selected" tabindex="14" />

							<form:errors path="emirateAttachement" class="errormsg" />
					<label for="emirateAttachement"><img src="assets/svgicons/attach.svg" style="height:15px;"><span>${emirattach}</span></label>
					<span class="message-inst"><spring:message code="attachment.tooltip" /></span>
				<c:if test="${not empty signUpBean.emirateFileName}">
							<a id="link_color" href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.emirateFileId}"/></portlet:resourceURL>'>
													<spring:message code="emirates.id"></spring:message></a>
													</c:if>
              <%--   <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
        </div>
	
					</div>
					<c:set var="unifid"><spring:message code="residence.id" /></c:set>
					<c:set var="unifattach"><spring:message code="residence.id" /></c:set>
					<div id="expatId" style="display: block;">
							
							<div class="col-md-12">
              <div class="input-groupdiv">
               <label><spring:message code="residence.id" /><span>*</span> 
                <form:input type="text" maxlength="15" name="residenceId"
								path="residenceId" id="residenceId" class="form-control"
								tabindex="15" placeholder='${unifid}'/>
               <a data-toggle="tooltip" rel="tooltip" class="tt_large" data-placement="right" title="<spring:message code="residence.id.tooltip" />">
	<i class="glyphicon glyphicon-info-sign information"aria-hidden="true"> </i>
</a>
               </label>
			 
		
							<form:errors path="residenceId" class="errormsg" />
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              </div>
        </div>
        <div class="col-md-12">
              <div class="input-groupdiv">
              <label><spring:message code="expat.expire.date" /><span>*</span> </label>
             <!--  <div class='input-group date' id='datetimepicker2'> -->
			  <form:input type="text" placeholder="DD/MM/YYYY" name="ResidenceExpireDate"
								path="residenceExpireDate" id="residenceExpireDate"
								class="form-control" readonly="true" tabindex="16" data-date-format="dd/mm/yyyy"/>
							<form:errors path="residenceExpireDate" class="errormsg" />
                  <!--   <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span> -->
				<%-- <div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              <!-- </div> -->
              </div>
        </div>
        <div class="col-md-12">
              <div class="input-groupdiv">
              <label><spring:message code="residence.copy.attachement" /><span>*</span> </label>
               <form:input type="file" maxlength="35" name="residenceAttachement" placeholder='${unifattach}' data-multiple-caption="{count} files selected" path="residenceAttachement" id="residenceAttachement" class="inputfile inputfile-3" tabindex="17" />
							<form:errors path="residenceAttachement" class="errormsg" />

					<label for="residenceAttachement"><img src="assets/svgicons/attach.svg" style="height:15px;"><span>${unifattach}</span></label>
					<span class="message-inst"><spring:message code="attachment.tooltip" /></span>
					<c:if test="${not empty signUpBean.residenceFileName}">
							<label><a id="link_color" href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.residenceFileId}"/></portlet:resourceURL>'>
													<spring:message code="residence.id"></spring:message></a></label>
													</c:if>
			<%-- 	<div class="tick-icon" id="success" style="display:none;"><img src="<%=request.getContextPath()%>/images/checked.svg"></div>
				<div class="delete-icon" id="fail" style="display:none;"><img src="<%=request.getContextPath()%>/images/cancel-button.svg"></div> --%>
              
              </div>
        </div>
        
							
					</div>
					
				</div>
			</div>
			</div>
			
			<div class="registration">			
          <div class="row registration-inner">
          		
          		<div class="col-md-12">
              <div class="input-groupdiv">
              		<p class="text-center">  
  <a class="btn_cancel" style="padding: 13px 5%;" href="/wps/myportal/rak"><spring:message code="button.cancel" /></a>
<%-- <input value="<spring:message code="button.cancel"/>" class="btn_cancel" id="resetForm" type="button"> --%>
<input type="submit" value="<spring:message code="button.submit"/>" class="btn_submit" id="formsubmitid">
 
  </p>
              </div>
              </div>
          		
          </div>
          </div>
          
          </div>
          </div>
          
          </form:form>
</div>

</body>
