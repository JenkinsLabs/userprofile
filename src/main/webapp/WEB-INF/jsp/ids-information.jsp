
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ page import="java.util.Properties"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="javax.portlet.PortletSession"%>
<%@ page import="javax.portlet.ResourceURL"%>
<%@ page import="javax.portlet.PortletURL"%>


<%@ page import="java.util.Locale"%>
<%@ page import="java.util.ResourceBundle"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<style >
	.form-group .form_note{
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

 .form-group:hover .form_note{
    visibility: visible;
}
</style>

<portlet:actionURL var="saveUserUpdateRequest">
	<portlet:param name="action" value="saveUserUpdateRequest"></portlet:param>
</portlet:actionURL>

<portlet:renderURL var="resetSignUpPage">
<portlet:param name="action" value="resetSignUpPage"></portlet:param>
</portlet:renderURL>


<%
	Locale locale = request.getLocale();
	String alignment = "left";
	String lang = "en";
	if (locale.getLanguage().equalsIgnoreCase("ar")) {
		alignment = "right";
		lang = "ar";
	}

%>
 <script src="<%=request.getContextPath() %>/js/id_information_validation.js"></script>
<script>
var App = 	{ 
				basePath : "${pageContext.request.contextPath}"
			};
</script>
<script>
$( document ).ready(function() {  
	<c:if test="${not empty displayMode}">
		$("#mobileNo").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#nationality").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#confirmEmail").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#email").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#nationality").css('cursor','not-allowed').prop('disabled', 'disabled');
		$('#citizenExpatFieldsetId').find('input,radiobutton').prop('disabled', 'disabled');
		$('#formsubmitid').prop('disabled', 'disabled');
		
			
	</c:if>
});


/*  Script */ 
 
 

function checkEmirateIDAvailability(id){
	console.log("---checkEmirateIDAvailability---"+id);
	var emirateId=document.getElementById(id).value;
			
	
	if(id=='emiratesId'){
    //alert("-emiratesId condition--");
	var extraEmirateCheck=document.getElementById('extraEmirateCheckId').value;
	}else{
	//alert("---else condition---");
	var extraEmirateCheck=document.getElementById('extraEmirateCheckGCCId').value;
	}
	if(emirateId!=extraEmirateCheck){
	  $("#emiratesId").parent().find('.errormsg,.succesmsg').remove(); 
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkEmirateIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	emirateId: emirateId
        },
        success: function( data ) {
        	 console.log('id ------'+id);
        	if(data.isEmirateIdAvailable){
        		console.log('Emirate Id exists');
        		if($('#'+id).parent().find('.errormsg').length == 0){
        			$('#'+id).parent().append("<p class='failureImg	 emirateIdFailure'></p>");
        		$('#'+id).parent().append("<p class='errormsg'>"+data.emirateId+" <spring:message code='emirate.id.exist'/></p>");
        		}
        		isEmirateValid = false;
        	}else{
        		console.log('Emirate Id doesnot exists');
        		if($('#'+id).parent().find('.errormsg').length == 0)
        		 $('#'+id).parent().append("<p class='succesmsg'>"+data.emirateId+" <spring:message code='emirate.id.doesnot.exist'/></p>"); 
        		/* $('#'+id).parent().append("<p class='succesmsg emirateIdSuccess'></p>"); */
        		isEmirateValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	console.log("some error in-----checkEmirateIDAvailability-------"+textStatus);
        }
      });
	}else{
		isEmirateValid = true;
	}
}



function checkResidenceIDAvailability(){
	console.log("---checkResidenceIDAvailability---");
	var residenceId=document.getElementById('residenceId').value;
	var extraResidenceCheck=document.getElementById('extraResidenceCheckId').value;
	console.log('residenceId--'+residenceId);
	  $("#residenceId").parent().find('.errormsg,.succesmsg').remove(); 
	if(residenceId!=extraResidenceCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkResidenceIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	residenceId: residenceId
        },
        success: function( data ) {
        	 console.log('id ------'+data.isresidenceIdAvailable);
        	if(data.isresidenceIdAvailable){
        		console.log('Residence Id exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0){
        			/* $("#residenceId").parent().append("<p class='ur_failureImg emirateIdFailure'></p>"); */
        		$("#residenceId").parent().append("<p class='errormsg'>"+data.residenceId+" <spring:message code='residence.id.exist'/></p>");
        		}
        		isResidenceValid = false;
        	}else{
        		console.log('Residence Id doesnot exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0)
        		 $("#residenceId").parent().append("<p class='succesmsg'>"+data.residenceId+" <spring:message code='residence.id.doesnot.exist'/></p>"); 
        		/* $("#residenceId").parent().append("<p class='succesmsg'></p>"); */
        		isResidenceValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		isResidenceValid = true;
	} 
}
 

function checkEmailAvailability(){
	var email=document.getElementById('email').value;
	var extraEmailCheck=document.getElementById('extraEmailCheckId').value;	
   console.log('email'+email);
   console.log('extraEmailCheck'+extraEmailCheck);  
    $("#email").parent().find('.errormsg,.succesmsg').remove(); 
	if(email!=extraEmailCheck){
	//$( "#userName" ).parent().find('.ur_errormsg,.ur_succesmsg').remove();
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMailIdAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	email: email
        },
        success: function( data ) {        	
	        //	alert(data.isEmailIdAvailable);
	        	//if(data.value[0]){
	        	if(data.isEmailIdAvailable){
	        		if($("#email").parent().find('.errormsg').length == 0){
	        			/* $("#email").parent().append("<p class='errormsg'></p>"); */
	        		$("#email").parent().append("<p class='errormsg'>"+data.eMailId+" <spring:message code='email.exist'/></p>");
	        		}
	        		isEmailValid = false;
	        		return false;
	        	}else{
	        		if($("#email").parent().find('.errormsg').length == 0)
	        		 $("#email").parent().append("<p class='succesmsg'>"+data.eMailId+" <spring:message code='email.doesnot.exist'/></p>"); 
	        		 isEmailValid = true;       		
	        		
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}
}



function checkMobileNumberAvailability(){
	var mobileNo=document.getElementById('mobileNo').value;
	var extraMobileCheck=document.getElementById('extraMobileCheckId').value;
	var index1 = extraMobileCheck.indexOf('5');
    var phoneSubString = extraMobileCheck.substring(index1);	
	var format1="0971"+phoneSubString;
	var format2="00971"+phoneSubString;
	var format3="971"+phoneSubString;
	var format4="0"+phoneSubString;
   console.log('mobileNo'+mobileNo);
   console.log('extraMobileCheck'+extraMobileCheck);
   $("#mobileNo").parent().find('.errormsg,.succesmsg').remove();
	/* if(mobileNo!=extraMobileCheck){ */
		if ( [ format1, format2, format3,format4 ].indexOf( mobileNo ) == -1 ) { 
	   //alert("------not matching--------------");
		console.log("-------inside mobile number ajax calling-------------");
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMobileNumberAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	mobileNo: mobileNo
        },
        success: function( data ) {        	
	        	console.log("----------data value-----"+data.isMobileAvailable);
	        	if(data.isMobileAvailable){
	        		console.log("--------mobileNo--exists-------");
	        		if($("#mobileNo").parent().find('errormsg').length == 0){
	        		/* 	$("#mobileNo").parent().append("<p class='errormsg'></p>"); */
	        		$("#mobileNo").parent().append("<p class='errormsg'>"+data.mobileNo+" <spring:message code='mobile.exist'/></p>");
	        		}
	        		isMobileValid = false;
	        	}else{
	        		console.log("--------mobileNo-not-exists-------");
	        		if($("#mobileNo").parent().find('.errormsg').length == 0)
	        		 $("#mobileNo").parent().append("<p class='succesmsg'>"+data.mobileNo+" <spring:message code='mobile.doesnot.exist'/></p>"); 
	        		/* $("#mobileNo").parent().append("<p class='succesmsg'></p>"); */
	        		isMobileValid = true;
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}else{
		isMobileValid = true;
	}
}

function checkPassportNoAvailability(){
	console.log("---checkPassportNoAvailability---");
	var passportNo=document.getElementById('passportNo').value;
	var extraPassportCheck=document.getElementById('extraPassportNoCheckId').value;
	console.log('passportNo--'+passportNo);
	 $("#passportNo").parent().find('.errormsg,.succesmsg').remove();
	if(passportNo!=extraPassportCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkPassportNoAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	passportNo: passportNo
        },
        success: function( data ) {
        	 console.log('id ------'+data.isPassportNoAvailable);
        	if(data.isPassportNoAvailable){
        		console.log('Passport Id exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0){
        		/* 	$("#passportNo").parent().append("<p class='errormsg'></p>"); */
        		$("#passportNo").parent().append("<p class='errormsg'>"+data.passportNo+" <spring:message code='passport.no.exist'/></p>");
        		}
        		isPassportValid = false;
        	}else{
        		console.log('passportNo doesnot exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0)
        		$("#passportNo").parent().append("<p class='succesmsg'>"+data.passportNo+" <spring:message code='passport.id.doesnot.exist'/></p>");
        	
        		isPassportValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		 isPassportValid = true;
	} 
}

</script>

<script type="text/javascript">
  var errorMessage = {
			regType : '<spring:message code="error.select.registration"/>' ,
			nationality : '<spring:message code="error.select.nationality"/>' ,
			email : '<spring:message code="error.email"/>' ,
			emailFormat : '<spring:message code="error.emailFormat"/>' ,
			confirmEmail : '<spring:message code="error.confirmEmail"/>' ,
			mobile : '<spring:message code="error.mobileNo"/>' ,
			idExpireDate : '<spring:message code="error.idExpireDate"/>' , 
			idCopyAttachement : '<spring:message code="error.idCopyAttachement"/>' ,
			emiratesID : '<spring:message code="error.emiratesID"/>' ,
			passportNo : '<spring:message code="error.passportNo"/>' ,
			passportExpiredate : '<spring:message code="error.passportExpiredate"/>' ,
			passportCopyAttachement : '<spring:message code="error.passportCopyAttachement"/>' ,
			residenceId : '<spring:message code="error.residenceId"/>' ,
			invalidResidenceId: '<spring:message code="error.invalid.residenceId"/>' ,
			residenceExpiredate : '<spring:message code="error.residenceExpiredate"/>' , 
			residenceCopyAttachement : '<spring:message code="error.residenceCopyAttachement"/>' ,
			emirateExpiryDate: '<spring:message code="error.id.expiry.date"/>' ,
			residenceExpireDate: '<spring:message code="error.residence.id.expiry.date"/>' ,
			passportExpiryDate: '<spring:message code="error.passport.expiry.date"/>' ,
			passportAttachment:'<spring:message code="error.passportAttachment"/>' ,
			fileLength:'<spring:message code="error.invalid.file.length"/>',
			attachmentType:'<spring:message code="error.attachment.type"/>',
			invalidMobile:'<spring:message code="invalid.mobile"/>',
			invalidPassport:'<spring:message code="invalid.passport"/>',
			emirateId:'<spring:message code="error.emirateId"/>',
			invalidEmirateId:'<spring:message code="error.invalid.emirateId"/>',
			terms:'<spring:message code="Empty.Terms"/>',
			invalidConfirmEmail: '<spring:message code="invalid.error.confirmEmail"/>' ,
			invalidConfirmPassword:'<spring:message code="invalid.error.confirmPassword"/>' ,
			emptyPassword:'<spring:message code="empty.error.confirmPassword"/>' ,
			veryWeak:'<spring:message code="error.very.weak"/>' ,
			weak:'<spring:message code="error.weak"/>' ,
			medium:'<spring:message code="error.medium"/>' ,
			strong:'<spring:message code="error.strong"/>' ,
			userExist:'<spring:message code="user.exist"/>',
			emailExist:'<spring:message code="email.exist"/>',
			passwordInclude:'<spring:message code="password.include"/>',
			characters:'<spring:message code="password.characters"/>',
			oneCapitalLetter:'<spring:message code="password.one.capital.letter"/>',
			oneNumber:'<spring:message code="password.one.number"/>',
			noSpaces:'<spring:message code="password.no.spaces"/>',
			emailIdExist:'<spring:message code="emirate.id.exist"/>',
			residenceIdExist:'<spring:message code="residence.id.exist"/>',
			mobileNumberExist:'<spring:message code="mobile.exist"/>',
			passportNoExist:'<spring:message code="passport.no.exist"/>',	
			
			
			
	};

  var rootUrl='${pageContext.request.contextPath}';
  //document.getElementById("secondNameId").style.display='none';
        function showSelectType(value){
        	 console.log("--------value-----------"+value.value);
      	   if(value.value == "187" || value.value == "119" || value.value == "36" || value.value == "165" || value.value == "176"){
 				/* document.getElementById("citizenId").style.display='block';
 	       		document.getElementById("expatId").style.display='none';
 	       	    document.getElementById("passportId").style.display='block';  	       	
 	       		document.getElementById("citizenExpatFieldsetId").style.display = 'block';
 	       	    document.getElementById("idRadioButton").style.display = 'none';
 	       	    document.getElementById("emiratesId").style.display = "none";
 	         	document.getElementById("emirateIdGCC").style.display = "block";
 	         	$('#emiratesId').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
 	  	          $('#emirateIdGCC').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove(); */
 	  	          
 	  	          displayNonGccIds();
 		         
 			}else if(value.value == "217"){
 				document.getElementById("citizenId").style.display='block';
 	       		document.getElementById("expatId").style.display='none';
 	       		document.getElementById("citizenExpatFieldsetId").style.display = 'block';
 	       	    document.getElementById("idRadioButton").style.display = 'none';
 	       	    document.getElementById("passportId").style.display='none'; 
 	            document.getElementById("emiratesId").style.display = "block";
	       	    document.getElementById("emirateIdGCC").style.display = "none";
	       	 $('#emiratesId').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
 	          $('#emirateIdGCC').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove(); 
	            
 			  } 
      	   else if(value.value == -1){
				document.getElementById("citizenId").style.display = 'none';
				document.getElementById("expatId").style.display = 'none';
				document.getElementById("citizenExpatFieldsetId").style.display = 'none';
				document.getElementById("passportId").style.display='none'; 				
			} else{
	       		displayIds();
 			}
	}
       
			function load() {
				var nationalityType = document.getElementById("nationality").value;
				
				 if(nationalityType == "187" || nationalityType == "119" || nationalityType == "36" || nationalityType == "165" || nationalityType == "176"){
					 document.getElementById("citizenId").style.display='block';
		   	       		document.getElementById("expatId").style.display='none';
		   	       	    document.getElementById("passportId").style.display='block';  	       	
		   	       		document.getElementById("citizenExpatFieldsetId").style.display = 'block';
		   	       	    document.getElementById("idRadioButton").style.display = 'none';
		   	       	 document.getElementById("emiratesId").style.display = "none";
		   	       	document.getElementById("emirateIdGCC").style.display = "block";
		   	     $('#emiratesId').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
	  	          $('#emirateIdGCC').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
		             
		   
				}else if(nationalityType == "217"){
	   				document.getElementById("citizenId").style.display='block';
	   	       		document.getElementById("expatId").style.display='none';
	   	       		document.getElementById("citizenExpatFieldsetId").style.display = 'block';
	   	       	    document.getElementById("idRadioButton").style.display = 'none';
	   	       	    document.getElementById("passportId").style.display='none'; 
	   	       	document.getElementById("emiratesId").style.display = "block";
	   	       	document.getElementById("emirateIdGCC").style.display = "none";
	   	    $('#emiratesId').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
 	          $('#emirateIdGCC').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
	             
	   	       	
	   			}else if(nationalityType == -1){
	   				document.getElementById("citizenId").style.display = 'none';
					document.getElementById("expatId").style.display = 'none';
					document.getElementById("citizenExpatFieldsetId").style.display = 'none';
					document.getElementById("passportId").style.display='none'; 
				}else{
					displayIds();
	   			}
				
				$('#idsLink').css('color','#B5251C').css('font-weight', 'bold');
					
					
				

			}
			

	
	function getButtonValue(button){
	alert("in getbutton" + button);
		var element = document.getElementById("buttonNameId");
		element.value = button;
		//document.userSignUp.submit();	
		/* jQuery_1_9_1('form').find('input[type=submit]').prop('disabled', true);
		jQuery_1_9_1('form').find('input[type=button]').prop('disabled', true); */
		
		
	}	
	
	function ieSpinnerFix() { 
		$('.spinnerIdInfo').show(); 
	}
	 function displayIds(){		
		 document.getElementById("citizenExpatFieldsetId").style.display = 'block';
         document.getElementById("idRadioButton").style.display = 'block';
         document.getElementById("emiratesId").style.display = "block";
	       	document.getElementById("emirateIdGCC").style.display = "none";
	       	$("#idRadioNonGCCButton").css("display", "none"); 
	       	
	       	/* $('#emiratesId').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
	          $('#emirateIdGCC').parent().find('.ur_errormsg,.ur_succesImg,.ur_failureImg').remove();
	             */
         
		    if (document.userSignUp.radioId[0].checked == true) {	
		    	document.getElementById('citizenId').style.display='block';
		    	document.getElementById('expatId').style.display='none';   
		    	document.getElementById("passportId").style.display='none';  	
		    }
		    else if(document.userSignUp.radioId[1].checked == true){	
		    	document.getElementById('citizenId').style.display='none';
		    	document.getElementById('expatId').style.display='block';		
		    	document.getElementById("passportId").style.display='block';  	
		        }			    			    
		    
		    }			
	function displayNonGccIds(){	

       $("#citizenExpatFieldsetId").css("display", "block");
       $("#idRadioNonGCCButton").css("display", "block");      
       $("#idRadioButton").css("display", "none");
       $("#emiratesId").css("display", "none");
       $("#emirateIdGCC").css("display", "block");
	    if (document.userSignUp.gccRadioId[0].checked == true) {	
	    	$("#citizenId").css("display", "block");
	    	$("#expatId").css("display", "none");
	    	$("#passportId").css("display", "none"); 	
	    }
	    else if(document.userSignUp.gccRadioId[1].checked == true){		    	
	    	$("#citizenId").css("display", "block");
	    	$("#expatId").css("display", "none");		
	    	$("#passportId").css("display", "block"); 	
	        }			    
	    
	    }

</script>
<c:if test="${not empty displayMode}">
	<p class="updateRequest"><spring:message code="update.request.progress"></spring:message></p>
</c:if>
<body onload=load()>
	<div class="form_main">
		<form:form id="userSignUpForm" name="userSignUp" commandName="signUpBean" method="post"
			action="${saveUserUpdateRequest}" enctype="multipart/form-data" autocomplete="off">
			<input type="hidden" name="buttonName" value="" id="buttonNameId"/>
			<%-- <form:hidden name="buttonName" path="buttonName" id="buttonNameId" /> --%>
			<form:hidden path="otpRedirectUrl" value="${redirectUrl}" />
			<form:hidden path="emirateFileId" value="${signUpBean.emirateFileId}"/>
			<form:hidden path="residenceFileId" value="${signUpBean.residenceFileId}"/>
			<form:hidden path="passportFileId" value="${signUpBean.passportFileId}"/>
			<form:hidden path="egaUserExtId" value="${signUpBean.egaUserExtId}"/>
			<form:hidden id="extraEmailCheckId" path="extraEmailCheck" value="${signUpBean.email}"/>
			<form:hidden id="extraUserCheckId" path="extraUserCheck" value="${signUpBean.userName}"/>
			<form:hidden id="extraEmirateCheckId" path="extraEmirateCheck" value="${signUpBean.emiratesId}"/>
			<form:hidden id="extraEmirateCheckGCCId" path="extraEmirateCheckGCC" value="${signUpBean.emirateIdGCC}"/>
			<form:hidden id="extraResidenceCheckId" path="extraResidenceCheck" value="${signUpBean.residenceId}"/>
			<form:hidden id="extraMobileCheckId" path="extraMobileCheck" value="${signUpBean.mobileNo}"/>
			<form:hidden id="extraPassportNoCheckId" path="extraPassportCheck" value="${signUpBean.passportNo}"/>
			
			<form:hidden id="nationalityValue" path="nationalityValue"/>
			<form:hidden id="genderValue" path="genderValue"/>
			<form:hidden id="maritalStatusValue" path="maritalStatusValue"/>
			<%-- <form:hidden id="userNameHidden" path="userName"/> --%>
			
			<c:if test="${signUpBean.userId!= 0}">
			<form:hidden  id="userNameEdit" path="userName" value="${signUpBean.userName}"/> 			
			</c:if>				
			<div class="form_content">
			
		
			<%@ include file="/WEB-INF/jsp/left_info_bar.jsp" %>
			
				
					<legend>
						<spring:message code="user.registration" />
					</legend>
					
					<div class="form_details_left idsInfo">
						
					<div class="col-sm-6">
				<div class="form-group">	
					<label><spring:message code="email" /></label> <span><form:input
										type="text" maxlength="45" name="email" path="email"
										id="email" class="form001" tabindex="5"/><b class="clr_red">*</b> 
									
										<i class="form_note"><spring:message code="email.tooltip"  /></i>
										<form:errors path="email" class="errormsg" /> </span>
								</div>
								</div>	
								<div class="col-sm-6">
				<div class="form-group">	
										<label><spring:message code="confirm.email" /></label> <span><form:input
										type="text" maxlength="45" name="confirmEmail"
										path="confirmEmail" id="confirmEmail" class="form001" tabindex="6" oncopy="return false" onpaste="return false"/><b
									class="clr_red">*</b>
									
									<i class="form_note"><spring:message code="confirm.email.tooltip"  /></i>
									<form:errors path="confirmEmail" class="errormsg" /> </span>
															
								</div>
								</div>	
								<div class="col-sm-6">
				<div class="form-group">		
											<label><spring:message code="mobile" /></label> <span><form:input
										type="text" maxlength="14" name="mobileNo" path="mobileNo"
										id="mobileNo" class="form001" tabindex="10"/><b class="clr_red">*</b>
										
										<i class="form_note"><spring:message code="mobile.tooltip"  /></i>
										 <form:errors path="mobileNo" class="errormsg" /> </span>
										 </div>
								</div>
										 <div class="col-sm-6">
				<div class="form-group">
										 <label><spring:message code="nationality" /></label> <span>
									<form:select id="nationality" path="nationality"
										class="select001" tabindex="10" onchange="showSelectType(this)">

										<form:option value="-1">
											<spring:message code="select" />
										</form:option>
										<%
													if (lang.equals("en")) {
												%>
										<form:options items="${countryList}" itemValue="id"
											itemLabel="descriptionEnglish" />
										<%
													} else {
												%>
										<form:options items="${countryList}" itemValue="id"
											itemLabel="descriptionArabic" />
										<%
													}
												%>
									</form:select> <b class="clr_red">*</b> 
									<form:errors path="nationality"	class="errormsg" />
							</span>
							 </div>
								</div>
										
							
						
					</div>

<div id="citizenExpatFieldsetId" style="display: none">
			<b><spring:message code="id.information" /></b>
			<div id="idRadioButton" class="form_details_left radioButtons"
				style="display: none;">
				<ul>
					<li><label><spring:message code="provide.id" /></label> <span><form:radiobutton
								id="radioId" name="radioId" path="emirateRadioPath" value="0"
								onchange="displayIds()" checked="checked" /><label><spring:message code="label.national.id" /></label> <form:radiobutton id="radioId"
								name="radioId" path="emirateRadioPath" value="1"
								onchange="displayIds()" /><label><spring:message code="label.unified.passport.id" /></label> </span></li>
				</ul>
			</div>
			
			<div id="idRadioNonGCCButton" class="form_details_left radioButtons" style="display: none;">
<ul>
<li>
<label>Providing ID Info</label>
<span><input id="gccRadioId" name="emirateRadioGccPath" checked="checked" onchange="displayNonGccIds()" value="0" type="radio"><label>National ID</label>
<input id="gccRadioId" name="emirateRadioGccPath" onchange="displayNonGccIds()" value="1" type="radio"><label>National ID and Passport ID</label>
</span>
</li>
</ul>
</div>

			<div id="citizenId" style="display: none;">
				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="emirates.id" /></label>
							<form:input type="text" name="emiratesId" path="emiratesId"
								id="emiratesId" class="form-control" tabindex="15" maxlength="20" />
								
								<form:input type="text" name="emirateIdGCC" path="emirateIdGCC"
								id="emirateIdGCC" class="form-control" tabindex="15" maxlength="20" />
								
							<form:errors path="emiratesId" class="errormsg" />
							<b class="clr_red">*</b> <i class="form_note"><spring:message code="emirate.id.tooltip" /></i>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="expire.date" /></label>
							<form:input type="text" name="emirateExpireDate"
								path="emirateExpireDate" id="emirateidexpiry"
								class="form-control" tabindex="16" />
							<form:errors path="emirateExpireDate" class="errormsg" />
							<b class="clr_red">*</b>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="copy.attachement" /></label>
							<form:input type="file" maxlength="35" name="emirateAttachement"
								path="emirateAttachement" id="emirateAttachement"
								class="form-control" tabindex="17" />
							<form:errors path="emirateAttachement" class="errormsg" />
							<label id="removeAttachmentId" class="removeAttachmentClass"
								onclick="clearAttachment('emirateAttachement')"><spring:message code="clear" /> </label> <span class="fileTypeClass"><spring:message code="attachment.tooltip" /><br></span> <b class="clr_red">*</b>
						</div>
					</div>
				</div>
			</div>


			<div id="expatId" style="display: none;">
				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="residence.id" /></label>
							<form:input type="text" maxlength="15" name="residenceId"
								path="residenceId" id="residenceId" class="form-control"
								tabindex="18" />
							<form:errors path="residenceId" class="errormsg" />
							<b class="clr_red">*</b> <i class="form_note"><spring:message code="residence.id.tooltip" /></i>
						</div>
					</div>


					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="expat.expire.date" /></label>
							<form:input type="text" name="residenceExpireDate"
								path="residenceExpireDate" id="residenceExpireDate"
								class="form-control" tabindex="19" />
							<form:errors path="residenceExpireDate" class="errormsg" />
							<b class="clr_red">*</b>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="residence.copy.attachement" /></label>
							<form:input type="file" maxlength="35"
								name="residenceAttachement" path="residenceAttachement"
								id="residenceAttachement" class="form-control" tabindex="20" />
							<form:errors path="residenceAttachement" class="errormsg" />
							<label id="removeAttachmentId" class="removeAttachmentClass"
								onclick="clearAttachment('unifiedidAttachement')"><spring:message code="clear" /></label> <b class="clr_red">*</b> <span
								class="fileTypeClass"><spring:message code="attachment.tooltip" /></span>
						</div>
					</div>

				</div>
			</div>


			<div id="passportId" style="display: none;">

				<div class="row">


					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="passport.no" /></label>
							<form:input type="text" maxlength="20" name="passportNo"
								path="passportNo" id="passportNo" class="form-control"
								tabindex="21" />
							<form:errors path="passportNo" class="errormsg" />
							<b class="clr_red">*</b> <i class="form_note"><spring:message code="passport.tooltip" /></i>
						</div>
					</div>


					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="passport.expire.date" /></label>
							<form:input type="text" maxlength="35" name="passportExpiryDate"
								path="passportExpiryDate" id="passportExpiryDate"
								class="form-control" tabindex="22" />
							<form:errors path="passportExpiryDate" class="errormsg" />
							<b class="clr_red">*</b>
						</div>
					</div>


					<div class="col-sm-4">
						<div class="form-group">
							<label for="usr"><spring:message code="passport.copy.attachement" /></label>
							<form:input type="file" maxlength="35" name="passportAttachement"
								path="passportAttachement" id="passportAttachement"
								class="form-control" tabindex="23" />
							<form:errors path="passportAttachement" class="errormsg" />
							<label id="removeAttachmentId" class="removeAttachmentClass"
								onclick="clearAttachment('passportAttachement')"><spring:message code="clear" /></label> <b class="clr_red">*</b> <span
								class="fileTypeClass"><spring:message code="attachment.tooltip" /></span>
						</div>
					</div>

				</div>

			</div>
		</div>
				<div class="form_search_buttons idsInfo">
					<input id="formsubmitid" type="submit" id="save" value='<spring:message code="button.submit"/>' tabindex="25" onclick="getButtonValue('submit')"/>
					<%-- <c:if test="${signUpBean.userId le 0}">
					<input type="button" id="resetId" value='<spring:message code="button.reset"/>' onClick="location.href='${resetSignUpPage}'" tabindex="26"/>
					</c:if>	 --%>				
		     	    <input type="button" id="cancel" value='<spring:message code="button.cancel"/>' onClick="location.href='/wps/myportal/rak'" tabindex="27"/>
				</div>
				  <input type="hidden" id="cancel" name="userNameOld" id="userNameOld"   />
			</div>
			<div class="form_pagination_search">
				<div class="form_pagination_search_left"></div>
				<div class="form_pagination_search_middle"></div>
				<div class="form_pagination_search_right"></div>
			</div>
		</form:form>
	</div>
</body>