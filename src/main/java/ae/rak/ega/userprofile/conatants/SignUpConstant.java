package ae.rak.ega.userprofile.conatants;

/**
 * @author Sandeep Kumar Puvvada
 *
 * Class SignUpConstant
 * Version 1.0
 * 
 */
public class SignUpConstant {

  public static final String ENGLISH = "en";
  public static final String ARABIC = "ar";
  public static final String SLASH = "/";
  public static final String ATTHERATE_STRING = "@";
  public static final char ATTHERATE_CHAR = '@';

  public static final String ZERO_STRING = "0";
  public static final String ONE_STRING = "1";
  public static final String TWO_STRING = "2";
  public static final String UAE = "217";
  public static final String THREE_STRING = "3";

  public static final String DBRK = "<br><br>";
  public static final String SBRK = "<br>";

  public static final String LINE_SEPERATOR = "line.separator";

  public static final String CONFIRM_MAIL = "confirmEmail";
  public static final String IS_EMAIL_ID_AVAILABLE = "isEmailIdAvailable";
  public static final String IS_USER_NAME_AVAILABLE = "isUserNameAvailable";
  public static final String IS_MOBILE_NUMBER_AVAILABLE = "isMobileAvailable";
  public static final String IS_EMIRATE_ID_AVAILABLE = "isEmirateIdAvailable";
  public static final String IS_RESIDENCE_ID_AVAILABLE = "isresidenceIdAvailable";
  public static final String IS_PASSPORT_NO_AVAILABLE = "isPassportNoAvailable";
  public static final String APPLICATION_JSON = "application/json";
  public static final String BUTTON_NAME = "buttonName";
  public static final String SUCCESS = "success";
  public static final String VALUE = "value";

  public static final String ACTION = "action";
  public static final String MY_ACTION = "myaction";
  public static final String OTP_FAILURE = "otp_failure";
  public static final String REGISTRATION = "registration";
  public static final String SIGNUP_BEAN = "signUpBean";
  public static final String SIGNUP = "sign_up";
  public static final String USER_ID = "userId";
  public static final String USER_NAME = "userName";

  public static final String DOC = ".doc";
  public static final String DOCX = ".docx";
  public static final String PDF = ".pdf";

  public static final int ZERO = 0;
  public static final int ONE = 1;
  public static final int TWO = 2;
  public static final int THREE = 3;
  public static final int SIX = 6;
  public static final int SEVEN = 7;
  public static final int EIGHT = 8;
  public static final int SIXTEEN = 16;
  public static final int TWENTY_FOUR = 24;
  public static final int SIXTY = 60;
  public static final int THOUSAND = 1000;
  public static final int THOUSAND_TWENTY_FOUR = 1024;
  public static final int MOBILE_ID = 11008;
  public static final int SIXTY_THOUSAND = 60000;
  public static final int ONE_LAKH = 100000;
  public static final int TWO_MB = 2097152;
  public static final String EMIRATE_ID = "EMIRATE_ID";
  public static final String RESIDENCE = "RESIDENCE";
  public static final String PASSPORT = "PASSPORT";
  public static final String STATUS_INPROGRESS = "1";
  public static final String STATUS_APPROVED = "2";
  public static final String STATUS_REJECTED = "3";
  public static final String UPDATE_REQUEST_USER_MAIL_SUBJ = "user.update.request";
  public static final String UPDATE_REQUEST_USER_MAIL_BODY = "update.request.user.mail.body";
  public static final String UPDATE_REQUEST_ADMIN_MAIL_BODY = "update.request.admin.mail.body";
  public static final String USER_PROFILE_EDIT = "User Profile EDit";
  public static final String SMS_SERVICE_URL = "sms.service.url";
  public static final String UPDATE_REQUEST_USER_SMS = "update.request.user.sms";
  public static final String TEMP_EMIRATE_FILE_NAME = "tempEmirateFileName";
  public static final String TEMP_RESIDENCE_FILE_NAME = "tempResidenceFileName";
  public static final String TEMP_PASSPORT_FILE_NAME = "tempPassportFileName";
  public static final String EMIRATE_EXT = "emirateExt";
  public static final String RESIDENCE_EXT = "residenceExt";
  public static final String PASSPORT_EXT = "passportExt";
  public static final String REMOVE_EMIRATE_FILE = "removeEmirateFile";
  public static final String REMOVE_RESIDENCE_FILE = "removeResidenceFile";
  public static final String REMOVE_PASSPORT_FILE = "passportResidenceFile";
  public static final String TEMP_LOCATION = "C:\\UsersData\\";
  public static final String IS_OTP_VERIFIED = "isOTPVerified";
  public static final String RESIDENCENO = "ResidenceNo";

  public static final String APPLICATION_FORM_URL_ENCODED = "application/x-www-form-urlencoded";
  public static final String CONTENT_TYPE = "content-type";
  public static final String UTF_8 = "utf-8";
  public static final String MAIL_ID = "mailId";

  public static final String RESIDENCE_NO = "ResidenceNo";

  private SignUpConstant() {
  }

}
