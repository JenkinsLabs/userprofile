package ae.rak.ega.userprofile.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ega_user_ded_info")
public class DedUserInfoEntity implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private int id;

  @Column(name = "user_id")
  private long userId;

  @Column(name = "subscriber_id")
  private String subscriberId;

  @Column(name = "owner_id")
  private String ownerId;

  @Column(name = "full_name")
  private String fullName;

  @Column(name = "pin_code")
  private String pinCode;

  @Column(name = "email")
  private String email;

  @Column(name = "national_id")
  private String nationalId;

  @Column(name = "mobile_number")
  private String mobileNumber;

  @Column(name = "date_of_birth")
  private String dateOfBirth;

  @Column(name = "family_book_no")
  private String familyBookNumber;

  @Column(name = "residence_number")
  private String residencNumber;

  @Column(name = "passport_expiry_date")
  private String passportExpiryDate;

  @Column(name = "visa_expiry_date")
  private String visaExpiryDate;

  @Column(name = "national_id_expiry_date")
  private String nationalIdExpiryDate;

  @Column(name = "nationality_country_id")
  private String nationalityCountryId;

  @Column(name = "passport_no")
  private String passportNumber;

  @Column(name = "preffered_language_id")
  private String prefferedLanguageId;

  @Column(name = "nationality_id")
  private String nationalityId;

  @Column(name = "visa_no")
  private String visaNumber;

  @Column(name = "approved_use")
  private String approvedUse;

  @Column(name = "passport_file_id")
  private String passportFileId;

  @Column(name = "family_book_file_id")
  private String familyBookFileId;

  @Column(name = "national_id_file_id")
  private String nationalIdFileId;

  @Column(name = "service_request_id")
  private String serviceRequestId;

  @Column(name = "residence_expiry_date")
  private String residenceExpiryDate;

  @Column(name = "residence_file_id")
  private String residenceFileId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getSubscriberId() {
    return subscriberId;
  }

  public void setSubscriberId(String subscriberId) {
    this.subscriberId = subscriberId;
  }

  public String getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(String ownerId) {
    this.ownerId = ownerId;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getPinCode() {
    return pinCode;
  }

  public void setPinCode(String pinCode) {
    this.pinCode = pinCode;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNationalId() {
    return nationalId;
  }

  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getFamilyBookNumber() {
    return familyBookNumber;
  }

  public void setFamilyBookNumber(String familyBookNumber) {
    this.familyBookNumber = familyBookNumber;
  }

  public String getResidencNumber() {
    return residencNumber;
  }

  public void setResidencNumber(String residencNumber) {
    this.residencNumber = residencNumber;
  }

  public String getPassportExpiryDate() {
    return passportExpiryDate;
  }

  public void setPassportExpiryDate(String passportExpiryDate) {
    this.passportExpiryDate = passportExpiryDate;
  }

  public String getVisaExpiryDate() {
    return visaExpiryDate;
  }

  public void setVisaExpiryDate(String visaExpiryDate) {
    this.visaExpiryDate = visaExpiryDate;
  }

  public String getNationalIdExpiryDate() {
    return nationalIdExpiryDate;
  }

  public void setNationalIdExpiryDate(String nationalIdExpiryDate) {
    this.nationalIdExpiryDate = nationalIdExpiryDate;
  }

  public String getNationalityCountryId() {
    return nationalityCountryId;
  }

  public void setNationalityCountryId(String nationalityCountryId) {
    this.nationalityCountryId = nationalityCountryId;
  }

  public String getPassportNumber() {
    return passportNumber;
  }

  public void setPassportNumber(String passportNumber) {
    this.passportNumber = passportNumber;
  }

  public String getPrefferedLanguageId() {
    return prefferedLanguageId;
  }

  public void setPrefferedLanguageId(String prefferedLanguageId) {
    this.prefferedLanguageId = prefferedLanguageId;
  }

  public String getNationalityId() {
    return nationalityId;
  }

  public void setNationalityId(String nationalityId) {
    this.nationalityId = nationalityId;
  }

  public String getVisaNumber() {
    return visaNumber;
  }

  public void setVisaNumber(String visaNumber) {
    this.visaNumber = visaNumber;
  }

  public String getApprovedUse() {
    return approvedUse;
  }

  public void setApprovedUse(String approvedUse) {
    this.approvedUse = approvedUse;
  }

  public String getPassportFileId() {
    return passportFileId;
  }

  public void setPassportFileId(String passportFileId) {
    this.passportFileId = passportFileId;
  }

  public String getFamilyBookFileId() {
    return familyBookFileId;
  }

  public void setFamilyBookFileId(String familyBookFileId) {
    this.familyBookFileId = familyBookFileId;
  }

  public String getNationalIdFileId() {
    return nationalIdFileId;
  }

  public void setNationalIdFileId(String nationalIdFileId) {
    this.nationalIdFileId = nationalIdFileId;
  }

  public String getServiceRequestId() {
    return serviceRequestId;
  }

  public void setServiceRequestId(String serviceRequestId) {
    this.serviceRequestId = serviceRequestId;
  }

  public String getResidenceExpiryDate() {
    return residenceExpiryDate;
  }

  public void setResidenceExpiryDate(String residenceExpiryDate) {
    this.residenceExpiryDate = residenceExpiryDate;
  }

  public String getResidenceFileId() {
    return residenceFileId;
  }

  public void setResidenceFileId(String residenceFileId) {
    this.residenceFileId = residenceFileId;
  }

  @Override
  public String toString() {
    return "DedUserInfoEntity [id=" + id + ", userId=" + userId + ", subscriberId=" + subscriberId + ", ownerId="
        + ownerId + ", fullName=" + fullName + ", pinCode=" + pinCode + ", email=" + email + ", nationalId="
        + nationalId + ", mobileNumber=" + mobileNumber + ", dateOfBirth=" + dateOfBirth + ", familyBookNumber="
        + familyBookNumber + ", residencNumber=" + residencNumber + ", passportExpiryDate=" + passportExpiryDate
        + ", visaExpiryDate=" + visaExpiryDate + ", nationalIdExpiryDate=" + nationalIdExpiryDate
        + ", nationalityCountryId=" + nationalityCountryId + ", passportNumber=" + passportNumber
        + ", prefferedLanguageId=" + prefferedLanguageId + ", nationalityId=" + nationalityId + ", visaNumber="
        + visaNumber + ", approvedUse=" + approvedUse + ", passportFileId=" + passportFileId + ", familyBookFileId="
        + familyBookFileId + ", nationalIdFileId=" + nationalIdFileId + ", serviceRequestId=" + serviceRequestId + "]";
  }

}
