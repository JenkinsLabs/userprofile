package ae.rak.ega.userprofile.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ega_user_ext_marital_status")
public class MaritalStatusEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "marital_status_id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int maritalStatusId;

  @Column(name = "marital_status_en")
  private String maritalStatusEn;

  @Column(name = "marital_status_ar")
  private String maritalStatusAr;

  public MaritalStatusEntity() {

  }

  public MaritalStatusEntity(int id) {
    this.maritalStatusId = id;
  }

  public int getMaritalStatusId() {
    return maritalStatusId;
  }

  public void setMaritalStatusId(int maritalStatusId) {
    this.maritalStatusId = maritalStatusId;
  }

  public String getMaritalStatusEn() {
    return maritalStatusEn;
  }

  public void setMaritalStatusEn(String maritalStatusEn) {
    this.maritalStatusEn = maritalStatusEn;
  }

  public String getMaritalStatusAr() {
    return maritalStatusAr;
  }

  public void setMaritalStatusAr(String maritalStatusAr) {
    this.maritalStatusAr = maritalStatusAr;
  }

}
