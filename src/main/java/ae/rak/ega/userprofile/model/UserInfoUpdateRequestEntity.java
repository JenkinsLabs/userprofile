package ae.rak.ega.userprofile.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "ega_user_info_update_request")
public class UserInfoUpdateRequestEntity implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "request_id")
  private int requestId;

  @Column(name = "user_id")
  private long userId;

  @Column(name = "admin_user_id")
  private long adminUserId;

  @Column(name = "user_name")
  private String userName;

  @Column(name = "password")
  private String password;

  @Column(name = "email_id")
  private String emailId;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "second_name")
  private String secondName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "mobile_number")
  private String mobileNumber;

  @Column(name = "date_of_birth")
  private String dob;

  @ManyToOne
  @JoinColumn(name = "nationality")
  private LiferayCountryEntity nationality;

  @ManyToOne
  @JoinColumn(name = "marital_status")
  private MaritalStatusEntity maritalStatus;

  @ManyToOne
  @JoinColumn(name = "gender")
  private GenderEntity gender;

  @Column(name = "status")
  private String status;

  @Column(name = "admin_comments")
  private String adminComments;

  @Column(name = "created_date")
  private String createdDate;

  @Column(name = "modified_date")
  private String modifiedDate;

  @Column(name = "is_email_changed")
  private boolean isEmailChanged;

  @Column(name = "is_first_name_changed")
  private boolean isFirstNameChanged;

  @Column(name = "is_second_name_changed")
  private boolean isSecondNameChanged;

  @Column(name = "is_last_name_changed")
  private boolean isLastNameChanged;

  @Column(name = "is_mobile_no_changed")
  private boolean isMobileNoChanged;

  @Column(name = "is_dob_changed")
  private boolean isDobChanged;

  @Column(name = "is_nationality_changed")
  private boolean isNationalityChanged;

  @Column(name = "is_marital_status_changed")
  private boolean isMaritalStatusChanged;

  @Column(name = "is_gender_changed")
  private boolean isGenderChanged;

  @Column(name = "is_emirate_id_changed")
  private boolean isEmirateIdChanged;

  @Column(name = "is_emirate_expiry_date_changed")
  private boolean isEmirateExpiryDateChanged;

  @Column(name = "is_emirate_attachment_changed")
  private boolean isEmirateAttachmentChanged;

  @Column(name = "is_residence_id_changed")
  private boolean isResidenceIdChanged;

  @Column(name = "is_residence_expiry_date_changed")
  private boolean isResidenceExpiryDateChanged;

  @Column(name = "is_residence_attachment_changed")
  private boolean isResidenceAttachmentChanged;

 /* @Column(name = "is_passport_no_changed")
  private boolean isPassportNoChanged;

  @Column(name = "is_passport_expiry_date_changed")
  private boolean isPassportExpiryDateChanged;

  @Column(name = "is_passport_attachment_changed")
  private boolean isPassportAttachmentChanged;*/

  @Column(name = "password_change")
  private boolean passwordChange;

  /*@Column(name = "email_old_value")
  private String emailOldValue;

  @Column(name = "mobile_old_value")
  private String mobileOldValue;

  @Column(name = "nationality_old_value")
  private String nationalityOldValue;

  @Column(name = "providing_id_info")
  private String providingIdInfo;*/

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "request_id", referencedColumnName = "request_id")
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<UserUpdateRequestAttachmentEntity> userRequestAttachmentList = new ArrayList<UserUpdateRequestAttachmentEntity>();

  public int getRequestId() {
    return requestId;
  }

  public void setRequestId(int requestId) {
    this.requestId = requestId;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public long getAdminUserId() {
    return adminUserId;
  }

  public void setAdminUserId(long adminUserId) {
    this.adminUserId = adminUserId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmailId() {
    return emailId;
  }

  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public MaritalStatusEntity getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(MaritalStatusEntity maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getAdminComments() {
    return adminComments;
  }

  public void setAdminComments(String adminComments) {
    this.adminComments = adminComments;
  }

  public String getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  public String getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(String modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public LiferayCountryEntity getNationality() {
    return nationality;
  }

  public void setNationality(LiferayCountryEntity nationality) {
    this.nationality = nationality;
  }

  public GenderEntity getGender() {
    return gender;
  }

  public void setGender(GenderEntity gender) {
    this.gender = gender;
  }

  public List<UserUpdateRequestAttachmentEntity> getUserRequestAttachmentList() {
    return userRequestAttachmentList;
  }

  public void setUserRequestAttachmentList(List<UserUpdateRequestAttachmentEntity> userRequestAttachmentList) {
    this.userRequestAttachmentList = userRequestAttachmentList;
  }

  public boolean isEmailChanged() {
    return isEmailChanged;
  }

  public void setEmailChanged(boolean isEmailChanged) {
    this.isEmailChanged = isEmailChanged;
  }

  public boolean isFirstNameChanged() {
    return isFirstNameChanged;
  }

  public void setFirstNameChanged(boolean isFirstNameChanged) {
    this.isFirstNameChanged = isFirstNameChanged;
  }

  public boolean isSecondNameChanged() {
    return isSecondNameChanged;
  }

  public void setSecondNameChanged(boolean isSecondNameChanged) {
    this.isSecondNameChanged = isSecondNameChanged;
  }

  public boolean isLastNameChanged() {
    return isLastNameChanged;
  }

  public void setLastNameChanged(boolean isLastNameChanged) {
    this.isLastNameChanged = isLastNameChanged;
  }

  public boolean isMobileNoChanged() {
    return isMobileNoChanged;
  }

  public void setMobileNoChanged(boolean isMobileNoChanged) {
    this.isMobileNoChanged = isMobileNoChanged;
  }

  public boolean isDobChanged() {
    return isDobChanged;
  }

  public void setDobChanged(boolean isDobChanged) {
    this.isDobChanged = isDobChanged;
  }

  public boolean isNationalityChanged() {
    return isNationalityChanged;
  }

  public void setNationalityChanged(boolean isNationalityChanged) {
    this.isNationalityChanged = isNationalityChanged;
  }

  public boolean isMaritalStatusChanged() {
    return isMaritalStatusChanged;
  }

  public void setMaritalStatusChanged(boolean isMaritalStatusChanged) {
    this.isMaritalStatusChanged = isMaritalStatusChanged;
  }

  public boolean isGenderChanged() {
    return isGenderChanged;
  }

  public void setGenderChanged(boolean isGenderChanged) {
    this.isGenderChanged = isGenderChanged;
  }

  public boolean isEmirateIdChanged() {
    return isEmirateIdChanged;
  }

  public void setEmirateIdChanged(boolean isEmirateIdChanged) {
    this.isEmirateIdChanged = isEmirateIdChanged;
  }

  public boolean isEmirateExpiryDateChanged() {
    return isEmirateExpiryDateChanged;
  }

  public void setEmirateExpiryDateChanged(boolean isEmirateExpiryDateChanged) {
    this.isEmirateExpiryDateChanged = isEmirateExpiryDateChanged;
  }

  public boolean isEmirateAttachmentChanged() {
    return isEmirateAttachmentChanged;
  }

  public void setEmirateAttachmentChanged(boolean isEmirateAttachmentChanged) {
    this.isEmirateAttachmentChanged = isEmirateAttachmentChanged;
  }

  public boolean isResidenceIdChanged() {
    return isResidenceIdChanged;
  }

  public void setResidenceIdChanged(boolean isResidenceIdChanged) {
    this.isResidenceIdChanged = isResidenceIdChanged;
  }

  public boolean isResidenceExpiryDateChanged() {
    return isResidenceExpiryDateChanged;
  }

  public void setResidenceExpiryDateChanged(boolean isResidenceExpiryDateChanged) {
    this.isResidenceExpiryDateChanged = isResidenceExpiryDateChanged;
  }

  public boolean isResidenceAttachmentChanged() {
    return isResidenceAttachmentChanged;
  }

  public void setResidenceAttachmentChanged(boolean isResidenceAttachmentChanged) {
    this.isResidenceAttachmentChanged = isResidenceAttachmentChanged;
  }

  public boolean isPasswordChange() {
    return passwordChange;
  }

  public void setPasswordChange(boolean passwordChange) {
    this.passwordChange = passwordChange;
  }

 /* public String getEmailOldValue() {
    return emailOldValue;
  }

  public void setEmailOldValue(String emailOldValue) {
    this.emailOldValue = emailOldValue;
  }

  public String getMobileOldValue() {
    return mobileOldValue;
  }

  public void setMobileOldValue(String mobileOldValue) {
    this.mobileOldValue = mobileOldValue;
  }

  public String getNationalityOldValue() {
    return nationalityOldValue;
  }

  public void setNationalityOldValue(String nationalityOldValue) {
    this.nationalityOldValue = nationalityOldValue;
  }*/

 /* public boolean isPassportNoChanged() {
    return isPassportNoChanged;
  }

  public void setPassportNoChanged(boolean isPassportNoChanged) {
    this.isPassportNoChanged = isPassportNoChanged;
  }

  public boolean isPassportExpiryDateChanged() {
    return isPassportExpiryDateChanged;
  }

  public void setPassportExpiryDateChanged(boolean isPassportExpiryDateChanged) {
    this.isPassportExpiryDateChanged = isPassportExpiryDateChanged;
  }

  public boolean isPassportAttachmentChanged() {
    return isPassportAttachmentChanged;
  }

  public void setPassportAttachmentChanged(boolean isPassportAttachmentChanged) {
    this.isPassportAttachmentChanged = isPassportAttachmentChanged;
  }

  public String getProvidingIdInfo() {
    return providingIdInfo;
  }

  public void setProvidingIdInfo(String providingIdInfo) {
    this.providingIdInfo = providingIdInfo;
  }*/

}
