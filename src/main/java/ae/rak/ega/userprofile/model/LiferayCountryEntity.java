package ae.rak.ega.userprofile.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Sandeep Kumar Puvvada
 *
 * Class LiferayCountryEntity
 * Version 1.0
 * 
 */
@Entity
@Table(name = "country")
public class LiferayCountryEntity implements Serializable {

  private static final long serialVersionUID = -6006724139538545420L;

  @Id
  @Column(name = "countryId")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name")
  private String descriptionEnglish;

  @Column(name = "name_ar")
  private String descriptionArabic;

  @Column(name = "active_")
  private int active;

  public LiferayCountryEntity() {

  }

  public LiferayCountryEntity(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDescriptionEnglish() {
    return descriptionEnglish;
  }

  public void setDescriptionEnglish(String descriptionEnglish) {
    this.descriptionEnglish = descriptionEnglish;
  }

  public String getDescriptionArabic() {
    return descriptionArabic;
  }

  public void setDescriptionArabic(String descriptionArabic) {
    this.descriptionArabic = descriptionArabic;
  }

  public int getActive() {
    return active;
  }

  public void setActive(int active) {
    this.active = active;
  }

}
