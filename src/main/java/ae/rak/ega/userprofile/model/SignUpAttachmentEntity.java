package ae.rak.ega.userprofile.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Satish 
 *
 * Class SignUpAttachmentEntity
 * Version 1.0
 * 
 */
@Entity
@Table(name = "USER_ATTACHMENTS")
public class SignUpAttachmentEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "DOC_KEY", unique = true, nullable = false)
  private Integer documentId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "USERID", nullable = false)
  private SignUpEntity userId;

  @Column(name = "DOC_NAME")
  private String docName;

  @Column(name = "DOC_EXTENSION")
  private String docExtension;

  @Column(name = "DOC_CONTENT")
  private byte[] content;

  @Column(name = "DOC_CONTENT_TYPE")
  private String contentType;

  @Column(name = "DOC_EXP_DATE")
  private Timestamp validTo;

  @Column(name = "DOC_ID_NUMBER")
  private String idNumber;
  
  @Column(name = "DED_FILE_ID")
  private String dedFieldID;

  public Integer getDocumentId() {
    return documentId;
  }

  public void setDocumentId(Integer documentId) {
    this.documentId = documentId;
  }

  public SignUpEntity getUserId() {
    return userId;
  }

  public void setUserId(SignUpEntity userId) {
    this.userId = userId;
  }

  public String getDocName() {
    return docName;
  }

  public void setDocName(String docName) {
    this.docName = docName;
  }

  public String getDocExtension() {
    return docExtension;
  }

  public void setDocExtension(String docExtension) {
    this.docExtension = docExtension;
  }

  public byte[] getContent() {
    return content;
  }

  public void setContent(byte[] content) {
    this.content = content;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public Timestamp getValidTo() {
    return validTo;
  }

  public void setValidTo(Timestamp validTo) {
    this.validTo = validTo;
  }

  public String getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }

  @Override
  public String toString() {
    return "SignUpAttachmentEntity [documentId=" + documentId + ", userId=" + userId + ", docName=" + docName
        + ", docExtension=" + docExtension + ", contentType=" + contentType + ", validTo=" + validTo + ", idNumber="
        + idNumber + "]";
  }

}
