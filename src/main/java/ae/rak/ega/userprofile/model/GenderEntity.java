package ae.rak.ega.userprofile.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ega_user_ext_gender")
public class GenderEntity implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "gender_id")
  //@GeneratedValue(strategy = GenerationType.AUTO)
  private int genderId;

  @Column(name = "gender_en")
  private String genderEn;

  @Column(name = "gender_ar")
  private String genderAr;

  public int getGenderId() {
    return genderId;
  }

  public void setGenderId(int genderId) {
    this.genderId = genderId;
  }

  public String getGenderEn() {
    return genderEn;
  }

  public void setGenderEn(String genderEn) {
    this.genderEn = genderEn;
  }

  public String getGenderAr() {
    return genderAr;
  }

  public void setGenderAr(String genderAr) {
    this.genderAr = genderAr;
  }

}
