package ae.rak.ega.userprofile.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Satish
 *
 * Class SignUpAttachmentEntity
 * Version 1.0
 * 
 */
@Entity
@Table(name = "ega_user_info_update_request_attachments")
public class UserUpdateRequestAttachmentEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "DOCUMENT_ID", unique = true, nullable = false)
  private Integer documentId;

  @Column(name = "user_id")
  private long userId;

  @Column(name = "doc_name")
  private String docName;

  @Column(name = "doc_extension")
  private String docExtension;

  @Column(name = "content")
  private byte[] content;

  @Column(name = "content_type")
  private String contentType;

  @Column(name = "valid_to")
  private String validTo;

  @Column(name = "id_number")
  private String idNumber;

  /*@Column(name = "doc_name_old")
  private String docNameOld;

  @Column(name = "doc_extension_old")
  private String docExtensionOld;

  @Column(name = "content_old")
  private byte[] contentOld;

  @Column(name = "content_type_old")
  private String contentTypeOld;

  @Column(name = "valid_to_old")
  private String validToOld;

  @Column(name = "id_number_old")
  private String idNumberOld;*/

  public Integer getDocumentId() {
    return documentId;
  }

  public void setDocumentId(Integer documentId) {
    this.documentId = documentId;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getDocName() {
    return docName;
  }

  public void setDocName(String docName) {
    this.docName = docName;
  }

  public String getDocExtension() {
    return docExtension;
  }

  public void setDocExtension(String docExtension) {
    this.docExtension = docExtension;
  }

  public byte[] getContent() {
    return content;
  }

  public void setContent(byte[] content) {
    this.content = content;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public String getValidTo() {
    return validTo;
  }

  public void setValidTo(String validTo) {
    this.validTo = validTo;
  }

  public String getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }

 /* public String getDocNameOld() {
    return docNameOld;
  }

  public void setDocNameOld(String docNameOld) {
    this.docNameOld = docNameOld;
  }

  public String getDocExtensionOld() {
    return docExtensionOld;
  }

  public void setDocExtensionOld(String docExtensionOld) {
    this.docExtensionOld = docExtensionOld;
  }

  public byte[] getContentOld() {
    return contentOld;
  }

  public void setContentOld(byte[] contentOld) {
    this.contentOld = contentOld;
  }

  public String getContentTypeOld() {
    return contentTypeOld;
  }

  public void setContentTypeOld(String contentTypeOld) {
    this.contentTypeOld = contentTypeOld;
  }

  public String getValidToOld() {
    return validToOld;
  }

  public void setValidToOld(String validToOld) {
    this.validToOld = validToOld;
  }

  public String getIdNumberOld() {
    return idNumberOld;
  }

  public void setIdNumberOld(String idNumberOld) {
    this.idNumberOld = idNumberOld;
  }

  @Override
  public String toString() {
    return "UserUpdateRequestAttachmentEntity [documentId=" + documentId + ", userId=" + userId + ", docName="
        + docName + ", docExtension=" + docExtension + ", contentType=" + contentType + ", validTo=" + validTo
        + ", idNumber=" + idNumber + ", docNameOld=" + docNameOld + ", docExtensionOld=" + docExtensionOld
        + ", contentTypeOld=" + contentTypeOld + ", validToOld=" + validToOld + ", idNumberOld=" + idNumberOld + "]";
  }*/

}
