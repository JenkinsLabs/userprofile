package ae.rak.ega.userprofile.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


/**
 * @author satish 30/10/2016
 *
 * Class SignUpEntity
 * Version 1.0
 * 
 */
@Entity
@Table(name = "USER_DETAILS")
@NamedQueries({
    @NamedQuery(name = "upgateGeneralInfo", query = "update SignUpEntity set dateOfBirth= :dateOfBirth,firstName=:firstName"
            + ", secondName=:secondName,familyName=:familyName,gender=:gender,maritalStatus=:maritalStatus where userId= :userId ")
})
public class SignUpEntity implements Serializable {

	  /**
	   * 
	   */
	  private static final long serialVersionUID = 1L;

	
	private long userId;
	private String userName;
	private Date createDate;
	private Date modifiedDate;
	private String dateOfBirth;
	private int passwordreset;
	private Date passwordModifiedDate;
//	private String maritalStatus;
	//private String nationality;
	private String emirate;
	private String mobileNo;
	private String email;
	//private String gender;
	private String firstName;
	private String languageId;
	private String secondName;
	private String familyName;
	private Date loginDate;	
	private String loginIP;	
	private Date lastLoginDate;
	private String lastLoginIP;
	private String terms;
	private int emailVerified;
	private String otpPwd;
	private Date otpCreatedDate;
	private Date otpModifiedDate;
	private String dedSubscriberID;
	private String sapSubscriberID;
	  private LiferayCountryEntity nationality;
	  private MaritalStatusEntity maritalStatus; 
	  private GenderEntity gender;
	  
	  
	  private String password;
	  private String status;
	  
	  private List<SignUpAttachmentEntity> attachmentList = new ArrayList<SignUpAttachmentEntity>();

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "USERID")
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	@Column(name = "USERNAME")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATEDATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIEDDATE")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Column(name = "DOB")
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	@Column(name = "PASSWORDRESET")
	public int getPasswordreset() {
		return passwordreset;
	}
	public void setPasswordreset(int passwordreset) {
		this.passwordreset = passwordreset;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PASSWORDMODIFIEDDATE")
	public Date getPasswordModifiedDate() {
		return passwordModifiedDate;
	}
	public void setPasswordModifiedDate(Date passwordModifiedDate) {
		this.passwordModifiedDate = passwordModifiedDate;
	}
	
	/*@Column(name = "NATIONALITY")
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}*/
	@Column(name = "EMIRATE")
	public String getEmirate() {
		return emirate;
	}
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	@Column(name = "MOBILE_NUMBER")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Column(name = "EMAILADDRESS")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "FIRSTNAME")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Column(name = "LANGUAGEID")
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	@Column(name = "SECONDNAME")
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	@Column(name = "FAMILYNAME")
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LOGINDATE")
	public Date getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	@Column(name = "LOGINIP")
	public String getLoginIP() {
		return loginIP;
	}
	public void setLoginIP(String loginIP) {
		this.loginIP = loginIP;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LASTLOGINDATE")
	public Date getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	@Column(name = "LASTLOGINIP")
	public String getLastLoginIP() {
		return lastLoginIP;
	}
	public void setLastLoginIP(String lastLoginIP) {
		this.lastLoginIP = lastLoginIP;
	}
	@Column(name = "AGREEDTOTERMSOFUSE")
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
	@Column(name = "EMAILADDRESSVERIFIED")
	public int getEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(int emailVerified) {
		this.emailVerified = emailVerified;
	}
	@Column(name = "OTP")
	public String getOtpPwd() {
		return otpPwd;
	}
	public void setOtpPwd(String otpPwd) {
		this.otpPwd = otpPwd;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OTP_CREATED_DATE")
	public Date getOtpCreatedDate() {
		return otpCreatedDate;
	}
	public void setOtpCreatedDate(Date otpCreatedDate) {
		this.otpCreatedDate = otpCreatedDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "OTP_MODIFIED_DATE")
	public Date getOtpModifiedDate() {
		return otpModifiedDate;
	}
	public void setOtpModifiedDate(Date otpModifiedDate) {
		this.otpModifiedDate = otpModifiedDate;
	}
	@Column(name = "DED_SUBSCRIBER_ID")
	public String getDedSubscriberID() {
		return dedSubscriberID;
	}
	public void setDedSubscriberID(String dedSubscriberID) {
		this.dedSubscriberID = dedSubscriberID;
	}
	@Column(name = "SAP_SUBSCRIBER_ID")
	public String getSapSubscriberID() {
		return sapSubscriberID;
	}
	public void setSapSubscriberID(String sapSubscriberID) {
		this.sapSubscriberID = sapSubscriberID;
	}
/*	@Column(name = "MARITAL_STATUS")
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}*/
/*	@Column(name = "NATIONALITY")
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}*/
	/*@Column(name = "GENDER")
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}*/
	
	@ManyToOne
	@JoinColumn(name = "nationality")
	public LiferayCountryEntity getNationality() {
		return nationality;
	}
	public void setNationality(LiferayCountryEntity nationality) {
		this.nationality = nationality;
	}
	
	 @OneToMany(cascade = CascadeType.ALL)
	  @JoinColumn(name = "USERID" , referencedColumnName = "USERID")
	  @LazyCollection(LazyCollectionOption.FALSE)
	public List<SignUpAttachmentEntity> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<SignUpAttachmentEntity> attachmentList) {
		this.attachmentList = attachmentList;
	}
	
	  @ManyToOne
	  @JoinColumn(name = "marital_status")
	public MaritalStatusEntity getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(MaritalStatusEntity maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
		
		@ManyToOne
		@JoinColumn(name = "gender")
		public GenderEntity getGender() {
		return gender;
	}
	public void setGender(GenderEntity gender) {
		this.gender = gender;
	}
	
	@Column(name = "PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
