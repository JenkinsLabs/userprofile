package ae.rak.ega.userprofile.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateError;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ae.rak.ega.userprofile.conatants.SignUpConstant;
import ae.rak.ega.userprofile.model.DedUserInfoEntity;
import ae.rak.ega.userprofile.model.GenderEntity;
import ae.rak.ega.userprofile.model.LiferayCountryEntity;
import ae.rak.ega.userprofile.model.MaritalStatusEntity;
import ae.rak.ega.userprofile.model.SignUpAttachmentEntity;
import ae.rak.ega.userprofile.model.SignUpEntity;
import ae.rak.ega.userprofile.model.UserInfoUpdateRequestEntity;


@Repository
public class UserProfileDaoImpl implements UserProfileDao{
	private Logger LOGGER=Logger.getLogger(UserProfileDaoImpl.class);
	@Autowired
    SessionFactory sessionFactory;
    Query query = null;
    
    
	public SignUpEntity getProfileData(String userName){
	Session	session = sessionFactory.openSession();
		
		 SignUpEntity entity = null;
		    LOGGER.info("pulling user extension data for user with id " + userName);
		    try {
		      Criteria criteria = session.createCriteria(SignUpEntity.class);
		      criteria.add(Restrictions.eq("userName", userName));
		      entity = (SignUpEntity) criteria.uniqueResult();
		    } catch (Exception e) {
		      LOGGER.error("Exception while retrieving the users:", e);
		    } finally {
		      session.close();
		    }
		    LOGGER.info("Entity >>" + entity);
		    return entity;
	}
	
	 @SuppressWarnings("unchecked")
	  public List<LiferayCountryEntity> getCountries(String locale) {
	    List<LiferayCountryEntity> countryList = new ArrayList<LiferayCountryEntity>();
	    Session session =sessionFactory.openSession();
	    try {
	      Criteria criteria = session.createCriteria(LiferayCountryEntity.class);
	      criteria.add(Restrictions.eq("active", 1));
	      if (SignUpConstant.ENGLISH.equals(locale)) {
	        criteria.addOrder(Order.asc("descriptionEnglish"));
	      } else {
	        criteria.addOrder(Order.asc("descriptionArabic"));
	      }
	      countryList = criteria.list();
	    } catch (Exception e) {
	      LOGGER.error("Exception while getting coruntries list : ", e);
	    } finally {
	      session.close();
	    }
	    return countryList;
	  }
	 
	 public List<MaritalStatusEntity> getMaritalStatusList() {
		    List<MaritalStatusEntity> entityList = new ArrayList<MaritalStatusEntity>();
		    Session session = sessionFactory.openSession();
		    try {
		      Criteria criteria = session.createCriteria(MaritalStatusEntity.class);
		      entityList = criteria.list();
		    } catch (Exception e) {
		      LOGGER.error("Exception while retrieving the marital status entity", e);
		    } finally {
		      session.close();
		    }
		    return entityList;
		  }

		
		  public List<GenderEntity> getGenderEntityList() {
		    List<GenderEntity> entityList = new ArrayList<GenderEntity>();
		    Session session = sessionFactory.openSession();
		    try {
		      Criteria criteria = session.createCriteria(GenderEntity.class);
		      entityList = criteria.list();
		    } catch (Exception e) {
		      LOGGER.error("Exception while retrieving the Gender entity", e);
		    } finally {
		      session.close();
		    }
		    return entityList;
		  }
	
		
		  public GenderEntity getGenderEntityById(int id) {
			  
			    GenderEntity genderEntity =null;
			    Session session = sessionFactory.openSession();
			    try {
			    	
			    genderEntity= (GenderEntity) session.get(GenderEntity.class, id);
			      
			    } catch (Exception e) {
			      LOGGER.error("Exception while retrieving the Gender entity", e);
			    } finally {
			      session.close();
			    }
			    return genderEntity;
			  }
		  
		
		  public SignUpAttachmentEntity getAttachmentById(int id) {
		    Session session = null;
		    SignUpAttachmentEntity attachments = null;
		    try {
		      session = sessionFactory.openSession();
		      Criteria crit = session.createCriteria(SignUpAttachmentEntity.class);
		      crit.add(Restrictions.eq("documentId", id));
		      attachments = (SignUpAttachmentEntity) crit.uniqueResult();
		    } catch (Exception e) {
		      LOGGER.error("-----Exception while getting attachments----", e);
		    } finally {
		      session.close();
		    }
		    return attachments;
		  }
		  
		  public void updateUserInfo(SignUpEntity signUpEntity) {
			    LOGGER.info("------START---updateGeneralDetails-------");
			   Session  session = sessionFactory.openSession();
			    org.hibernate.Transaction trans = session.beginTransaction();
			   /* Query query = session.createSQLQuery("UPDATE SignUpEntity set dateOfBirth=:dateOfBirth,firstName:firstName,secondName:secondName,familyName:familyName" +
			    		"gender where userId=:userId");*/
			    
			    try {
			    	 Query query = session.getNamedQuery("upgateGeneralInfo");
			      query.setString("dateOfBirth", signUpEntity.getDateOfBirth());
			      query.setString("firstName", signUpEntity.getFirstName());
			      query.setString("secondName", signUpEntity.getSecondName());
			      query.setString("familyName", signUpEntity.getFamilyName());
			      query.setEntity("gender", signUpEntity.getGender());
			      query.setEntity("maritalStatus", signUpEntity.getMaritalStatus());
			      query.setLong("userId", signUpEntity.getUserId());
			      query.executeUpdate();
			      trans.commit();
			      LOGGER.info("----END---updating general info-------");
			    } catch (HibernateError e) {
			      LOGGER.error("exception while updating general info the user details", e);
			    } finally {
			      session.close();
			    }
			  }
		  public UserInfoUpdateRequestEntity getUserUpdateRequestInfo(long userId) {
			    LOGGER.info("--------status in dao-----------" + userId);
			    Session session = null;
			    UserInfoUpdateRequestEntity userInfoUpdateRequestEntity = new UserInfoUpdateRequestEntity();
			    try {
			      session = sessionFactory.openSession();
			      Criteria criteria = session.createCriteria(UserInfoUpdateRequestEntity.class);
			      criteria.add(Restrictions.eq(SignUpConstant.USER_ID, userId));
			      criteria.add(Restrictions.eq("status", "1"));
			      userInfoUpdateRequestEntity = (UserInfoUpdateRequestEntity) criteria.uniqueResult();
			    } catch (Exception e) {
			      LOGGER.error("----Exception while getting update request details----------------", e);
			    } finally {
			      session.close();
			    }
			    return userInfoUpdateRequestEntity;
			  }
		  
		  public boolean isEmailAvailable(String email) {
			  LOGGER.info("isEmailAvailable >>> " + email);
			  boolean isEmailIdAvailable = false;
			  SignUpEntity entity = null;
			  Session session = sessionFactory.openSession();
			  Criteria criteria = session.createCriteria(SignUpEntity.class);
		      criteria.add(Restrictions.eq("email", email.toString()));
		      entity = (SignUpEntity) criteria.uniqueResult();
		      
		      if(entity != null)
		    	  isEmailIdAvailable = true;
		      
		      LOGGER.info(" isEmailIdAvailable >>> " + isEmailIdAvailable);
		      session.close();
		      return isEmailIdAvailable;
		  }
		  
		  public boolean isMobileNUmberAvailable(String mobile) {
			  LOGGER.info(" isMobileNUmberAvailable >>> " + mobile);
			  boolean isMobileNoAvailable = false;
			  SignUpEntity entity = null;
			 Session  session = sessionFactory.openSession();
			  Criteria criteria = session.createCriteria(SignUpEntity.class);
		      criteria.add(Restrictions.eq("mobileNo", mobile.toString()));
		      entity = (SignUpEntity) criteria.uniqueResult();
		      
		      if(entity != null)
		    	  isMobileNoAvailable = true;
		      
		      LOGGER.info(" isMobileNoAvailable >>> " + isMobileNoAvailable);
		      
		      return isMobileNoAvailable;
		  }
		  
		  public Long isUniqueIdAvailable(String emirateId) {
			    LOGGER.info("------emirateId--------" + emirateId);
			    Session session = null;
			    Long emirateIdCount = 0L;
			    try {
			      session = sessionFactory.openSession();
			      String query = "select count(*) from SignUpAttachmentEntity where idNumber=:idNumber";
			      Query query1 = session.createQuery(query);
			      query1.setParameter("idNumber", emirateId);
			      emirateIdCount = (Long) query1.uniqueResult();
			      LOGGER.info("----emirateIdCount----------" + emirateIdCount);

			    } catch (Exception e) {
			      LOGGER.error("------Exception while getting emirate Id details----------", e);
			    }
			    finally{
	  		    	session.close();
	  		    }
			    return emirateIdCount;
			  }
		  
		 
		  public String saveUserUpdateRequest(UserInfoUpdateRequestEntity userInfoUpdateRequestEntity) {
		    Session session = null;
		    Transaction tx = null;
		    String refNum = null;
		    try {
		      session = sessionFactory.openSession();
		      tx = session.beginTransaction();
		      LOGGER.info("1--");
		      session.saveOrUpdate(userInfoUpdateRequestEntity.getGender());
		      LOGGER.info("2--");
		      session.save(userInfoUpdateRequestEntity);
		      LOGGER.info("--------refnum after saving---------------" + userInfoUpdateRequestEntity.getRequestId());
		      refNum = String.valueOf(userInfoUpdateRequestEntity.getRequestId());
		      tx.commit();
		    } catch (Exception e) {
		      LOGGER.info("-------Unable to save the update request details-----------", e);
		    } finally {
		      session.close();
		    }
		    return refNum;
		  }
		  
		  @Override
		  public DedUserInfoEntity getLocalDedData(long userId) {
		    Session session = null;
		    DedUserInfoEntity entity = null;
		    try {
		      session = sessionFactory.openSession();
		      Criteria crit = session.createCriteria(DedUserInfoEntity.class);
		      crit.add(Restrictions.eq(SignUpConstant.USER_ID, userId));
		      entity = (DedUserInfoEntity) crit.uniqueResult();
		    } catch (Exception e) {
		      LOGGER.error("-----Exception while getting getLocalDedData----", e);
		    } finally {
		      session.close();
		    }
		    return entity;
		  }
		  
		  @Override
		  public void updateLocalDedData(DedUserInfoEntity dedUserInfoEntity) {
		    Session session = sessionFactory.openSession();
		    Transaction trans = session.beginTransaction();
		    try {
		      session.update(dedUserInfoEntity);
		      trans.commit();
		      LOGGER.info("updated successfully::");
		    } catch (Exception e) {
		      trans.rollback();
		      LOGGER.error("Exception while saving the data in local ded:::", e);
		    } finally {
		      session.close();
		    }

		  }
		  
		@Override
		public void updateUserpasswordInDB(String userName, String password)throws Exception {
			 Session session = null;
			try {
				   LOGGER.info("userName - " + userName);
				   session = sessionFactory.openSession();
				   
				   String hqlUpdate = "UPDATE SignUpEntity c set c.password = :password where c.userName = :userName";
				   query = session.createQuery(hqlUpdate);
				   query.setString("password", password);
				   query.setString("userName", userName);
				   int rowCount = query.executeUpdate();
				   LOGGER.info("Rows affected: " + rowCount);
				  } catch (HibernateException e) {
					  LOGGER.error("exception while updating password in DB - ", e);
				  }
			 	finally{
	  		    	session.close();
	  		    }
		}
		
}
