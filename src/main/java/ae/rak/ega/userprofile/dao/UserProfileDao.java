package ae.rak.ega.userprofile.dao;

import java.util.List;

import ae.rak.ega.userprofile.model.DedUserInfoEntity;
import ae.rak.ega.userprofile.model.GenderEntity;
import ae.rak.ega.userprofile.model.LiferayCountryEntity;
import ae.rak.ega.userprofile.model.MaritalStatusEntity;
import ae.rak.ega.userprofile.model.SignUpAttachmentEntity;
import ae.rak.ega.userprofile.model.SignUpEntity;
import ae.rak.ega.userprofile.model.UserInfoUpdateRequestEntity;

public interface UserProfileDao {
	public SignUpEntity getProfileData(String userName);
	public List<LiferayCountryEntity> getCountries(String locale);
	 public SignUpAttachmentEntity getAttachmentById(int id);
	 public List<GenderEntity> getGenderEntityList();
	 public List<MaritalStatusEntity> getMaritalStatusList();
	 public void updateUserInfo(SignUpEntity signUpEntity);
	 public UserInfoUpdateRequestEntity getUserUpdateRequestInfo(long userId);
	 public boolean isEmailAvailable(String email);
	 public boolean isMobileNUmberAvailable(String mobile);
	 public Long isUniqueIdAvailable(String emirateId);
	 public String saveUserUpdateRequest(UserInfoUpdateRequestEntity userInfoUpdateRequestEntity);
	 public DedUserInfoEntity getLocalDedData(long userId);
	 public void updateLocalDedData(DedUserInfoEntity dedUserInfoEntity);
	 public GenderEntity getGenderEntityById(int id);
	 
	 public void updateUserpasswordInDB(String userName, String userPassword) throws Exception;
	 
}
