package ae.rak.ega.userprofile.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

/*
*<p>
* Trivial implementation of the {@link MultipartFile} interface to wrap a byte[]
*</p>
*/
public class MultipartFileUtil implements MultipartFile {

  private final byte[] imgContent;
  private final String name;
  private final String contentType;

  public MultipartFileUtil(String fileName, byte[] fileContent, String contentType, String fileExt) {
    this.imgContent = fileContent;
    this.name = (fileName == null ? "xyz" : fileName) + fileExt;
    this.contentType = contentType;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getOriginalFilename() {
    return name;
  }

  @Override
  public String getContentType() {
    return contentType;
  }

  @Override
  public boolean isEmpty() {
    return imgContent == null || imgContent.length == 0;
  }

  @Override
  public long getSize() {
    return imgContent.length;
  }

  @Override
  public byte[] getBytes() throws IOException {
    return imgContent;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return new ByteArrayInputStream(imgContent);
  }

  @Override
  public void transferTo(File dest) throws IOException, IllegalStateException {
    new FileOutputStream(dest).write(imgContent);
  }
}
