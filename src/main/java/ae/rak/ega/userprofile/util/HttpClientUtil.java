package ae.rak.ega.userprofile.util;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import ae.rak.ega.userprofile.command.DedSubscribeBean;
import ae.rak.ega.userprofile.command.SignUpBean;
import ae.rak.ega.userprofile.conatants.SignUpConstant;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Satish
 *
 * Class HttpClientUtil
 * Version 1.0
 * 
 */
@Component
public class HttpClientUtil {

  private static final Logger LOGGER = Logger.getLogger(HttpClientUtil.class);
  private static final String APP_TYPE = "W";
  private static final String LANG_E = "1";
  private static final String LANG_A = "2";

  private HttpClientUtil() {

  }

  public String uploadImage(MultipartFile multipartFile) throws RemoteException {

    String fileID = null;
    InputStream inputStream = null;
    HttpURLConnection conn = null;
    Properties properties = new Properties();
    if ((multipartFile == null) || (multipartFile.isEmpty()) || (multipartFile.getSize() == 0)) {
      LOGGER.info("MultipartFile file is null or empty.... returning null fileID");
      return fileID;
    }

    LOGGER.info(">>>>>> uploading MultipartFile in DED system.... getOriginalFilename name :"
        + multipartFile.getOriginalFilename());
    LOGGER.info("MultiparFile Size :" + multipartFile.getSize());

    try {
      properties = getEnvironmentProperties();
      URL obj = new URL(properties.getProperty("ded.upload.image.file"));
      conn = (HttpURLConnection) obj.openConnection();
      conn.setRequestMethod("POST");
      conn.setDoOutput(true);
      inputStream = multipartFile.getInputStream();
      String boundary = "--" + System.currentTimeMillis();
      conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
      boundary = "--" + boundary;

      conn.setRequestProperty("Content-Length", "" + multipartFile.getBytes().length);
      conn.setReadTimeout(SignUpConstant.SIXTY_THOUSAND);

      OutputStream outPutStream = conn.getOutputStream();

      byte[] buffer = new byte[SignUpConstant.THOUSAND_TWENTY_FOUR];
      int bytesRead = 0;
      buffer = (boundary + System.getProperty(SignUpConstant.LINE_SEPERATOR)).getBytes();
      outPutStream.write(buffer, 0, buffer.length);
      String bufferString = String.format("Content-Disposition: form-data; name=%s; filename=%s %s", "Image",
          multipartFile.getOriginalFilename(), System.getProperty(SignUpConstant.LINE_SEPERATOR));
      buffer = bufferString.getBytes();

      outPutStream.write(buffer, 0, buffer.length);

      bufferString = String.format("Content-Type: %s%s%s", multipartFile.getContentType(),
          System.getProperty(SignUpConstant.LINE_SEPERATOR),
          System.getProperty(SignUpConstant.LINE_SEPERATOR));

      buffer = bufferString.getBytes();

      outPutStream.write(buffer, 0, buffer.length);

      while ((bytesRead = inputStream.read(buffer)) > 0) {
        outPutStream.write(buffer, 0, bytesRead);
      }

      buffer = System.getProperty(SignUpConstant.LINE_SEPERATOR).getBytes();
      outPutStream.write(buffer, 0, buffer.length);
      buffer = boundary.getBytes();
      outPutStream.write(buffer, 0, buffer.length);

      inputStream = conn.getInputStream();
      StringBuilder sb = new StringBuilder();

      BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
      String read = bf.readLine();
      while ((read = bf.readLine()) != null) {
        sb.append(read);
      }

      LOGGER.info("File Upload Response - " + sb.toString());

      DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document doc = docBuilder.parse(new ByteArrayInputStream(sb.toString().getBytes()));
      fileID = doc.getChildNodes().item(0).getTextContent();
      LOGGER.info("Uploaded File ID - " + fileID);

    } catch (SocketException e) {
      LOGGER.info("IOException while uploading file ", e);
    } catch (IOException e) {
      LOGGER.info("IOException while uploading file ", e);
    } catch (Exception e) {
      LOGGER.info("Exception while uploading file ", e);
    } finally {
      try {
        inputStream.close();
        conn.disconnect();
      } catch (IOException e) {
        LOGGER.error("Error while closing file stream ", e);
      }

    }

    return fileID;
  }

  private static HttpParams getHttpParam() {
    HttpParams params = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(params, SignUpConstant.SIXTY_THOUSAND);
    HttpConnectionParams.setSoTimeout(params, SignUpConstant.SIXTY_THOUSAND);
    return params;
  }

    

  public int updateUser(DedSubscribeBean form) throws ParserConfigurationException, SAXException, IOException {

    Properties properties = new Properties();
    properties = getEnvironmentProperties();
    String xml = HttpClientUtil.updateSubscribeRestService(properties.getProperty("ded.update.subscribe"), form);
    int response = 0;
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    LOGGER.info("XML Data:::" + xml);
    Document document = builder.parse(new InputSource(new StringReader(xml)));
    Element rootElement = document.getDocumentElement();
    LOGGER.info("response:::" + rootElement.getTextContent());
    if (rootElement != null) {
      response = Integer.parseInt(rootElement.getTextContent());
    }

    return response;
  }

  public static String updateSubscribeRestService(String url, DedSubscribeBean form) {
    HttpClient client = new DefaultHttpClient(getHttpParam());
    HttpPost request = new HttpPost(url);
    request.setHeader(SignUpConstant.CONTENT_TYPE, SignUpConstant.APPLICATION_FORM_URL_ENCODED);
    try {
      List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

      urlParameters.add(new BasicNameValuePair("subscriber_id", form.getSubscriberId()));

      urlParameters.add(new BasicNameValuePair("NationalNo", form.getEmiratesId()));
      urlParameters.add(new BasicNameValuePair("ArName1", form.getFirstName()));

      urlParameters.add(new BasicNameValuePair("ArName2", form.getSecondName()));
      urlParameters.add(new BasicNameValuePair("ArName3", form.getFamilyName()));
      urlParameters.add(new BasicNameValuePair("CountryId", form.getNationalityCountryId()));
      urlParameters.add(new BasicNameValuePair("MobileNo", form.getMobileNo()));
      urlParameters.add(new BasicNameValuePair("DOB", form.getDateOfBirth()));
      urlParameters.add(new BasicNameValuePair("familyBook", ""));
      if (form.getResidenceId() != null && !form.getResidenceId().isEmpty()) {
        urlParameters.add(new BasicNameValuePair(SignUpConstant.RESIDENCENO, form.getResidenceId()));
      } else {
        urlParameters.add(new BasicNameValuePair(SignUpConstant.RESIDENCENO, ""));
      }
      urlParameters.add(new BasicNameValuePair("PassportExpiry", ""));
      urlParameters.add(new BasicNameValuePair("NationalIdExpiry", form.getEmirateExpireDate()));
      urlParameters.add(new BasicNameValuePair("passport_no", ""));
      urlParameters.add(new BasicNameValuePair("visa_expiry", ""));
      urlParameters.add(new BasicNameValuePair("Email", form.getEmail()));
      urlParameters.add(new BasicNameValuePair("passportFileId", ""));
      urlParameters.add(new BasicNameValuePair("nationalIdFileId", form.getEmirateImageId()));
      urlParameters.add(new BasicNameValuePair("familyBookFileId", ""));
      LOGGER.info("urlParameters:::" + urlParameters + "request:::" + request);
      request.setEntity(new UrlEncodedFormEntity(urlParameters, SignUpConstant.UTF_8));
      HttpResponse httpResponse = client.execute(request);
      BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
      String line = "";
      StringBuilder builder = new StringBuilder();
      while ((line = rd.readLine()) != null) {
        builder.append(line);
      }
      String response = builder.toString();
      EntityUtils.consumeQuietly(httpResponse.getEntity());
      return response;
    } catch (ClientProtocolException e) {
      LOGGER.error("ClientProtocolException while updateSubscribeRestService the web service ", e);
    } catch (IOException e) {
      LOGGER.error("IOException while updateSubscribeRestService web service ", e);
    } finally {
      request.releaseConnection();
    }

    return StringUtils.EMPTY;
  }

  public DedSubscribeBean prepareSignUpForm(SignUpBean signUpBean) {
	  LOGGER.info("IN prepareSignUpForm >>>>>>>>>>>>>> ");
	    DedSubscribeBean bean = new DedSubscribeBean();
	    bean.setAppType(APP_TYPE);
	    if (SignUpConstant.ENGLISH.equalsIgnoreCase(signUpBean.getLocale().getLanguage())) {
	      bean.setPreferredLanguage(LANG_E);
	      bean.setInterfaceLang(LANG_E);
	    } else {
	      bean.setPreferredLanguage(LANG_A);
	      bean.setInterfaceLang(LANG_A);
	    }
	    bean.setFirstName(signUpBean.getFirstName());
	    bean.setSecondName(signUpBean.getSecondName());
	    bean.setFamilyName(signUpBean.getFamilyName());
	    bean.setEmail(signUpBean.getEmail());
	    bean.setResidenceImageId(signUpBean.getResidenceImageId());
	    bean.setMobileNo(signUpBean.getMobileNo());
	    bean.setUserName(signUpBean.getUserName());
	    bean.setEmiratesId(signUpBean.getEmiratesId());
	    bean.setEmirateImageId(signUpBean.getEmirateImageId() == null ? "0" : signUpBean.getEmirateImageId());
	    bean.setEmirateExpireDate(formateDate(signUpBean.getEmirateExpireDate()));
	    LOGGER.info("Nationality >>>> " + signUpBean.getNationality());
	    bean.setNationalityCountryId(signUpBean.getNationality());
	    bean.setPassportNo(signUpBean.getPassportNo());
	    bean.setPassportExpiryDate(formateDate(signUpBean.getPassportExpiryDate()));
	    bean.setPassportImageId(signUpBean.getPassportImageId());
	    bean.setResidenceId(signUpBean.getResidenceId());
	    bean.setResidenceExpireDate(signUpBean.getResidenceExpireDate());
	    bean.setResidenceImageId(signUpBean.getResidenceImageId());
	    bean.setDateOfBirth(formateDate(signUpBean.getDateOfBirth()));
	    bean.setLocale(signUpBean.getLocale());
	    bean.setUserId(String.valueOf(signUpBean.getUserId()));
	    bean.setSubscriberId(signUpBean.getSubscriberId());
	    bean.setPassportImageId("0");
	    bean.setResidenceImageId(signUpBean.getResidenceImageId() == null ? "0" : signUpBean.getResidenceImageId());
	    LOGGER.info("Data passing to the web service:::" + bean.toString());
	    return bean;
	  }
  
  private Properties getEnvironmentProperties() throws IOException {

    Properties properties = new Properties();
    String environment = System.getProperty("app.env");
    properties.load(getClass().getResourceAsStream("/environment/" + environment + ".properties"));
    return properties;
  }

  

  public String formateDate(String strDate) {
    String date = null;
    if (strDate == null || strDate.isEmpty()) {
      return StringUtils.EMPTY;
    }
    DateFormat srcDf = new SimpleDateFormat("dd/MM/yyyy");

    try {
      Date date1 = srcDf.parse(strDate);
      DateFormat destDf = new SimpleDateFormat("dd-MM-yyyy");
      date = destDf.format(date1);
    } catch (ParseException e) {
      LOGGER.error("Exception while formatting date - " + date, e);
    }

    return date;

  }

  
 
}
