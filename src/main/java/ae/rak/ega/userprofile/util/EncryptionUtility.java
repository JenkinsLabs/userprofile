package ae.rak.ega.userprofile.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.axis.encoding.Base64;

public class EncryptionUtility {

	  private EncryptionUtility() {

	  }
	  
	  private final static String SHARED_KEY = "DEDAGL@3GA@R4K@S3RICES!@";
	  private final static String IV = "DED@EGA@";

	  public static String encrypt(String plaintext) throws Exception {
	    Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	    c.init(Cipher.ENCRYPT_MODE, getSecretKey(SHARED_KEY.getBytes()), new IvParameterSpec(IV.getBytes()));
	    byte[] encrypted = c.doFinal(plaintext.getBytes("UTF-8"));
	    return Base64.encode(encrypted);
	  }
	  
	  /**
	   * This method will give the decrypted value of encrypted text by using Triple DES Algorithm.
	   * @param ciphertext
	   * @return
	   * @throws Exception
	   */
	  public static String decrypt(String ciphertext) throws Exception {
	    Cipher c = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	    c.init(Cipher.DECRYPT_MODE, getSecretKey(SHARED_KEY.getBytes()), new IvParameterSpec(IV.getBytes()));
	    byte[] decrypted = c.doFinal(Base64.decode(ciphertext));
	    return new String(decrypted, "UTF-8");
	  }

	  /**
	   * This method will return secretKey in 24 byte format.
	   * @param encryptionKey
	   * @return
	   */
	  public static SecretKey getSecretKey(byte[] encryptionKey) {
	    SecretKey secretKey = null;
	    if (encryptionKey == null) {
	      return null;
	    }

	    byte[] keyValue = new byte[24];

	    if (encryptionKey.length == 16) {
	      System.arraycopy(encryptionKey, 0, keyValue, 0, 16);
	      System.arraycopy(encryptionKey, 0, keyValue, 16, 8);

	    } else if (encryptionKey.length != 24) {
	      throw new IllegalArgumentException("A TripleDES key should be 24 bytes long");

	    } else {
	      keyValue = encryptionKey;
	    }
	    DESedeKeySpec keySpec;
	    try {
	      keySpec = new DESedeKeySpec(keyValue);
	      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
	      secretKey = keyFactory.generateSecret(keySpec);
	    } catch (Exception e) {
	      throw new RuntimeException("Error in key Generation", e);
	    }
	    return secretKey;
	  }
	  
	 /*public static void main(String args[]){
		  try {
			String enc = "PMLCIhsaaewznEizooxhFQ==";
			//System.out.println("  " + enc);
			System.out.println(" dec " +decrypt(enc));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }*/
}
