package ae.rak.ega.userprofile.util;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.Context;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import ae.rak.ega.userprofile.controller.UserProfile;

@Component
public class TDSConnection {
	private static Logger LOGGER=Logger.getLogger(TDSConnection.class);
	public LdapContext getConnection(String env) {
		LOGGER.info("Environment !! " + env);
      Properties Props = new Properties();
      InitialLdapContext context = null;
      String ldapurl = null;
		String ldapport = null;
		String ldappassword = null;
      try {
    	  Props.load(getClass().getResourceAsStream("/environment/" + env + ".properties"));
			 ldapurl = Props.getProperty("ladap.ip");
			 ldapport = Props.getProperty("port");
			 ldappassword = Props.getProperty("pswd");
          Hashtable hashing = new Hashtable();
          hashing.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
          hashing.put(Context.PROVIDER_URL, "ldap://"+ldapurl+":"+ldapport);
          hashing.put(Context.SECURITY_PRINCIPAL, "cn=root");
          hashing.put(Context.SECURITY_CREDENTIALS, ldappassword);
          hashing.put(Context.SECURITY_AUTHENTICATION,"simple");
          context = new InitialLdapContext(hashing, null);
          LOGGER.info("TDS Connection Sucessful   " + context);
      } catch (Exception e) {
    	  LOGGER.error("An Exception Occured in TDSConnection Classs   " , e);
         
      }
      return context;
  }

}
