package ae.rak.ega.userprofile.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;


import ae.rak.ega.sendmailutility.SendMailUtility;
import ae.rak.ega.sms.util.SMSUtils;
import ae.rak.ega.userprofile.command.SignUpBean;
import ae.rak.ega.userprofile.conatants.SignUpConstant;
import ae.rak.ega.userprofile.dao.UserProfileDao;
import ae.rak.ega.userprofile.model.DedUserInfoEntity;
import ae.rak.ega.userprofile.model.GenderEntity;
import ae.rak.ega.userprofile.model.LiferayCountryEntity;
import ae.rak.ega.userprofile.model.MaritalStatusEntity;
import ae.rak.ega.userprofile.model.SignUpAttachmentEntity;
import ae.rak.ega.userprofile.model.SignUpEntity;
import ae.rak.ega.userprofile.model.UserInfoUpdateRequestEntity;
import ae.rak.ega.userprofile.model.UserUpdateRequestAttachmentEntity;
import ae.rak.ega.userprofile.util.EncryptionUtility;
import ae.rak.ega.userprofile.util.HttpClientUtil;
import ae.rak.ega.userprofile.util.MultipartFileUtil;


@Service
public class UserProfileServiceImpl implements UserProfileService{
	private Logger LOGGER=Logger.getLogger(UserProfileServiceImpl.class);
	
	 private static final String LANG_E = "1";
	  private static final String LANG_A = "2";
	
	@Autowired
	private UserProfileDao profileDao;
	
	@Autowired
	private HttpClientUtil httpClientUtil;
	
	@Override
	public SignUpBean getUserProfile(String userName,PortletRequest request){
		
		SignUpBean bean = null;
	    String lang = request.getLocale().getLanguage();
	    try {
	    
	      bean = prepareSignUpBean(profileDao.getProfileData(userName),lang);
	    } catch (Exception e) {
	      LOGGER.error("Exception while retrieving the user details::", e);
	    }
	    return bean;

	}
	
	private SignUpBean prepareSignUpBean(SignUpEntity entity,String lang){
  SignUpBean bean = new SignUpBean();
  try{
  bean.setUserName(entity.getUserName());
  LOGGER.info(">>>>>>>>>>>>>>>>> Mobile" + entity.getMobileNo());
  bean.setEmail(entity.getEmail());
  bean.setConfirmEmail(entity.getEmail());
  bean.setMobileNo(entity.getMobileNo());
  
  bean.setFirstName(entity.getFirstName());
  bean.setSecondName(entity.getSecondName());
  bean.setFamilyName(entity.getFamilyName());
  LOGGER.info("Date of birth " + entity.getDateOfBirth());
  bean.setDateOfBirth(dateToString(entity.getDateOfBirth()));

//  bean.setGender(entity.getGender().getGenderEn()); 
  /*if("1".equals(entity.getGender()))
		  bean.setGender("Male"); 
  else
	  bean.setGender("Female"); */
//  LOGGER.info("getting gender data::" + entity.getGender().getGenderEn());
  LOGGER.info(">>>>>>>>>>>>>>>>> gender" + entity.getGender().getGenderEn());
  LOGGER.info(">>>>>>>>>>>>>>>>> Nationality" + entity.getNationality().getDescriptionEnglish());
  bean.setGender(entity.getGender().getGenderId()==1 ? "1" : "0");
  if (entity != null && entity.getNationality() != null) {
    bean.setNationality(String.valueOf(entity.getNationality().getId()));
    if ("en".equalsIgnoreCase(lang) || "en_US".equalsIgnoreCase(lang)) {
      bean.setNationalityValue(entity.getNationality().getDescriptionEnglish());
    } else {
      bean.setNationalityValue(entity.getNationality().getDescriptionArabic());
    }
  }
  
  bean.setUserId(entity.getUserId());
  LOGGER.info("Marital status !! " + entity.getMaritalStatus().getMaritalStatusEn());
  LOGGER.info("Marital status !! " + entity.getMaritalStatus().getMaritalStatusId());
  if (entity != null && entity.getMaritalStatus() != null) {
    bean.setMaritalStatus(String.valueOf(entity.getMaritalStatus().getMaritalStatusId()));
    if ("en".equalsIgnoreCase(lang) || "en_US".equalsIgnoreCase(lang)) {
      bean.setMaritalStatusValue(entity.getMaritalStatus().getMaritalStatusEn());
    } else {
      bean.setMaritalStatusValue(entity.getMaritalStatus().getMaritalStatusAr());
    }
  }
  LOGGER.info("Marital status >>>>>>>>>> " + bean.getMaritalStatus());
  LOGGER.info("Marital status Value!! " + bean.getMaritalStatusValue());
  if (entity != null && entity.getAttachmentList() != null) {
	 if(entity.getAttachmentList().size() > 0){
		 LOGGER.info("IN Attachment if");
		 bean.setEmirateRadioPath("Yes"); 
    setAttachmentList(entity.getAttachmentList(), bean, entity);
	 }
   
  }
  }
  catch(Exception e){
	  LOGGER.error("Exceptionn while getting profile data " , e);
  }
  return bean;
}
	
	private void setAttachmentList(List<SignUpAttachmentEntity> entityList, SignUpBean bean, SignUpEntity signUpEntity) {
	    for (SignUpAttachmentEntity entity : entityList) {
	      LOGGER.info("----------setAttachmentList--in user signup------signUpEntity.getNationality().getId()---------"
	          + signUpEntity.getNationality().getId());

	      if (SignUpConstant.EMIRATE_ID.equals(entity.getDocName())) {
	    	  LOGGER.info("In amirate ID Attachment " );
	       /* if (187 == signUpEntity.getNationality().getId() || 119 == signUpEntity.getNationality().getId()
	            || 36 == signUpEntity.getNationality().getId() || 165 == signUpEntity.getNationality().getId()
	            || 176 == signUpEntity.getNationality().getId()) {
	          bean.setEmirateIdGCC(entity.getIdNumber());
	          LOGGER.info("In emirate id if");
	        } else {
	          bean.setEmiratesId(emirateIdFormat(entity.getIdNumber()));
	        }*/
	        bean.setEmiratesId(emirateIdFormat(entity.getIdNumber()));
	        LOGGER.info("EmirateFileId >>>>> " + entity.getDocumentId());
	        bean.setEmirateFileId(entity.getDocumentId());
	      //  bean.setEmirateFileContent(entity.getContent());
	        bean.setEmirateExpireDate(getStringDate(entity.getValidTo()));
	        bean.setEmirateFileExt(entity.getDocExtension());
	        LOGGER.info(" EmirateFileName " + entity.getDocName());
	        bean.setEmirateFileName(entity.getDocName());
	        bean.setEmirateFileContentType(entity.getContentType());
	      }

	      if (SignUpConstant.RESIDENCE.equals(entity.getDocName())) {
	    	  LOGGER.info("In RESIDENCE ID Attachment " );
	        bean.setResidenceId(entity.getIdNumber());
	        LOGGER.info("ResidenceFileId >>>>> " + entity.getDocumentId());
	        bean.setResidenceFileId(entity.getDocumentId());
	      //  bean.setResidenceFileContent(entity.getContent());
	        bean.setResidenceExpireDate(getStringDate(entity.getValidTo()));
	        bean.setResidenceFileExt(entity.getDocExtension());
	        LOGGER.info(" EmirateFileName " + entity.getDocName());
	        bean.setResidenceFileName(entity.getDocName());
	        bean.setResidenceFileContentType(entity.getContentType());

	      }
	      
	      if (SignUpConstant.PASSPORT.equals(entity.getDocName())) {
	    	  LOGGER.info("In PASSPORT ID Attachment " );
	        bean.setPassportNo(entity.getIdNumber());
	        bean.setPassportFileId(entity.getDocumentId());
	    //    bean.setPassportFileContent(entity.getContent());
	        bean.setPassportExpiryDate(getStringDate(entity.getValidTo()));
	        bean.setPassportFileExt(entity.getDocExtension());
	        bean.setPassportFileName(entity.getDocName());
	        bean.setPassportFileContentType(entity.getContentType());

	      }
	    } 
	  }
	
	 private String emirateIdFormat(String emirateId) {
		    String emirateValue = null;
		    if (StringUtils.isNotEmpty(emirateId) && emirateId.length() > 14 && !emirateId.contains("-")) {
		      StringBuilder builder = new StringBuilder(emirateId);
		      builder.insert(3, '-');
		      builder.insert(8, '-');
		      builder.insert(16, '-');
		      emirateValue = builder.toString();
		      return emirateValue;
		    } else {
		      return emirateId;
		    }

		  }
	 public String getStringDate(Date date){
		
			    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			    return formatter.format(date);
			  
	 }
	
	 
		public String dateToString(String date) {
			LOGGER.info("formating date !! " + date); //2016-03-15 18:25:29.0
			//  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			 SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
			 Date dateFormat = null;
			if(date != null){
				date = date.substring(0, 10);
			try {
				
				dateFormat = formatter.parse(date);
			} catch (ParseException e) {
				 LOGGER.error("Exception while retrieving the Date of Birth::", e);
			}
			
			
			LOGGER.info("After format >>>>>>... " + dateFormatter.format(dateFormat));
			}	
		    return dateFormatter.format(dateFormat);
		  }
		
		@Override
		public void updateGeneralUserInfo(SignUpBean signUpBean, String userName) {
		    SignUpEntity entity = new SignUpEntity();
		    LOGGER.info("user details updated successfully:::" + userName);
		    LOGGER.info("User ID" + signUpBean.getUserId());
		    LOGGER.info("User ID" + signUpBean.getFirstName());
		    LOGGER.info("User ID" + signUpBean.getSecondName());
		    LOGGER.info("User ID" + signUpBean.getFamilyName());
		    LOGGER.info("User ID" + signUpBean.getDateOfBirth());
		    LOGGER.info("User ID" + signUpBean.getGender());
		    LOGGER.info("User ID" + signUpBean.getMaritalStatus());
		    entity.setUserId(signUpBean.getUserId());
		    entity.setFirstName((signUpBean.getFirstName()));
		    entity.setSecondName((signUpBean.getSecondName()));
		    entity.setFamilyName(signUpBean.getFamilyName());
		    LOGGER.info("Update general info DB !! "+ signUpBean.getDateOfBirth());
		    entity.setDateOfBirth(signUpBean.getDateOfBirth());
		    GenderEntity gen = new GenderEntity();
		    LOGGER.info("Gender (Bean)="+ signUpBean.getGender());
		    gen.setGenderId(Integer.parseInt(signUpBean.getGender()));
		    LOGGER.info("Gender Value ="+ gen.getGenderEn());
		    entity.setGender(gen);
		    MaritalStatusEntity marstatusEntity = new MaritalStatusEntity();
		    marstatusEntity.setMaritalStatusId(Integer.parseInt(signUpBean.getMaritalStatus()));
		    entity.setMaritalStatus(marstatusEntity);
		    profileDao.updateUserInfo(entity);
		
		    DedUserInfoEntity dedEntity = profileDao.getLocalDedData(entity.getUserId());
		    SignUpEntity signupentity = profileDao.getProfileData(userName);
		    SignUpBean bean = prepareSignUpBean(signupentity, signUpBean.getLocale().getLanguage());
		    bean.setLocale(signUpBean.getLocale());
		    bean.setUserId(signupentity.getUserId());
		    bean.setEnvironment(signUpBean.getEnvironment());
		    if (dedEntity != null && dedEntity.getSubscriberId() != null) {
		      bean.setSubscriberId(dedEntity.getSubscriberId());
		      saveOrUpdateDedData(bean);
		      profileDao.updateLocalDedData(preparedLocalDedData(bean));
		    }
		   
		  }
		  public List<LiferayCountryEntity> getCountries(String locale) {
		    return profileDao.getCountries(locale);
		  }
		 
		  @Override
		  public SignUpAttachmentEntity getAttachmentById(int id) {
		    return profileDao.getAttachmentById(id);
		  }

		 
		  public List<MaritalStatusEntity> getMaritalStatusList() {
		    return profileDao.getMaritalStatusList();
		  }

		  
		  public List<GenderEntity> getGenderEntityList() {
		    return profileDao.getGenderEntityList();
		  }
		  public boolean isEmailAvailable(String email){
			  return profileDao.isEmailAvailable(email);
		  }
		  
		  public boolean isMobileNUmberAvailable(String mobile){
			  return profileDao.isMobileNUmberAvailable(mobile);
		  }
		  public SignUpBean getUserUpdateRequestInfo(long userId) {
			    return prepareUserInfoRequest(profileDao.getUserUpdateRequestInfo(userId));
			  }
		  private SignUpBean prepareUserInfoRequest(UserInfoUpdateRequestEntity userUpdateRequestInfo) {
			    SignUpBean signUpBean = new SignUpBean();
			    if (userUpdateRequestInfo != null && userUpdateRequestInfo.getUserId() > 0) {
			      LOGGER.info("------inside condition--------------");
			      signUpBean.setStatus(userUpdateRequestInfo.getStatus());
			    }
			    return signUpBean;
			  }
		  public boolean isUniqueIdAvailable(String uniquid){
			  
			  boolean isEmirateIdAvailable = false;
			    try {
			      Long emirateIdCount = profileDao.isUniqueIdAvailable(uniquid);
			      LOGGER.info("---------emirateIdCount--------------" + emirateIdCount);
			      if (emirateIdCount > 0) {
			        isEmirateIdAvailable = true;
			      }
			    } catch (Exception e) {
			      LOGGER.error("Exception while getting signup attachment details by emirate Id " + uniquid, e);
			    }
			    
			    return isEmirateIdAvailable;
			  
			
		  }
		  
		  @Override
		  public String saveUserUpdateRequest(SignUpBean signUpBean, String userName,Locale locale) {
			  
			 
			 SignUpEntity entity = profileDao.getProfileData(userName);
			 LOGGER.info("signUpBean mobile" + entity.getMobileNo());
			  LOGGER.info("signUpBean email" + entity.getEmail());
			    String refNum = profileDao.saveUserUpdateRequest(prepareUserUpdateRequestInfo(signUpBean, userName,locale.getLanguage()));
			    
			    LOGGER.info("refNum >>> " + refNum);
			  

			    Properties envProperties = new Properties();
			    Properties langProperties = new Properties();
			    String environment = System.getProperty("app.env");
			    String adminMail = null;
			    try {
			    	envProperties.load(getClass().getResourceAsStream("/environment/" + environment + ".properties"));
			      if ("en".equalsIgnoreCase(locale.getLanguage()) || "en_US".equalsIgnoreCase(locale.getLanguage())) {
					  LOGGER.info("In sendAdminSmsNoticfication if");
					  langProperties.load(getClass().getResourceAsStream("/signup_en.properties"));
				    }
				  else{
					  LOGGER.info("In sendAdminSmsNoticfication else");
					  langProperties.load(getClass().getResourceAsStream("/signup_ar.properties"));
				  }
			    } catch (IOException e) {
			      LOGGER.error(e);
			    }
			    adminMail = envProperties.getProperty("admin.mail.id");

			    sendUserRequestEmail(entity.getEmail(), SignUpConstant.UPDATE_REQUEST_USER_MAIL_SUBJ,
			        SignUpConstant.UPDATE_REQUEST_USER_MAIL_BODY, refNum, "mail.signature1",
			        entity.getFirstName(),langProperties,locale);

			    sendAdminEmailForRequest(adminMail, SignUpConstant.UPDATE_REQUEST_USER_MAIL_SUBJ,
			        SignUpConstant.UPDATE_REQUEST_ADMIN_MAIL_BODY, refNum, "mail.signature1", null,langProperties,locale);

			    try {
			      sendSmsNoticficationForUpdateRequest(SignUpConstant.UPDATE_REQUEST_USER_SMS,entity.getMobileNo(), refNum, signUpBean.getLocale().getLanguage(),langProperties);
			    } catch (Exception e) {
			      LOGGER.info("-------Exception while sending sms-------------", e);
			    }

			    return "refNum";
			  }
		  
		  private UserInfoUpdateRequestEntity prepareUserUpdateRequestInfo(SignUpBean signUpBean,
			      String userName,String lang) {
			  LOGGER.info("Preparing UserInfoUpdateRequestEntity >>>>>>>>>>>>>>>>>");
			    UserInfoUpdateRequestEntity userInfoUpdateRequestEntity = new UserInfoUpdateRequestEntity();
			  //  long userId = user.getUserId();
			    
			    SignUpBean oldData = prepareSignUpBean(profileDao.getProfileData(userName),lang);
			    LOGGER.info("After getting old values request from>>>>>>>>>>" + oldData.getUserId());
			   // SignUpEntity oldData = getUserInfo(userId, request);
			    userInfoUpdateRequestEntity.setUserName(signUpBean.getUserName());
			    userInfoUpdateRequestEntity.setUserId(oldData.getUserId());
			    userInfoUpdateRequestEntity.setEmailId(signUpBean.getEmail());
			    userInfoUpdateRequestEntity.setMobileNumber(signUpBean.getMobileNo());
			    
			    userInfoUpdateRequestEntity.setFirstName(signUpBean.getFirstName());
			    userInfoUpdateRequestEntity.setLastName(signUpBean.getFamilyName());
			    userInfoUpdateRequestEntity.setSecondName(signUpBean.getSecondName());
			    //GenderEntity gen = new GenderEntity();
			    LOGGER.info("Gender(Bean = )"+signUpBean.getGender());
			    
			    GenderEntity gen=profileDao.getGenderEntityById(Integer.parseInt(signUpBean.getGender()));
			    //gen.setGenderId(Integer.parseInt(signUpBean.getGender()));
			    LOGGER.info("New Gender = "+ gen.getGenderEn()+"--"+gen.getGenderId());
			    userInfoUpdateRequestEntity.setGender(gen);
			    MaritalStatusEntity marstatusEntity = new MaritalStatusEntity();
			    marstatusEntity.setMaritalStatusId(Integer.parseInt(signUpBean.getMaritalStatus()));
			    userInfoUpdateRequestEntity.setMaritalStatus(marstatusEntity);
			    userInfoUpdateRequestEntity.setDob(dateToStringYYYYMMDD(signUpBean.getDateOfBirth()));
			    
			    
			    LOGGER.info("----------signUpBean.getNationality()-------------" + signUpBean.getNationality());
			    LOGGER.info("----------signUpBean.getNationality()-------------" + signUpBean.getNationality());
			    userInfoUpdateRequestEntity.setNationality(new LiferayCountryEntity(Integer.parseInt(signUpBean.getNationality())));
			    userInfoUpdateRequestEntity.setStatus(SignUpConstant.STATUS_INPROGRESS);
			  
			    LOGGER.info("----------SignUpEntity.getNationality().getDescriptionEnglish()-------------" + oldData.getNationality());
			    userInfoUpdateRequestEntity.setCreatedDate(dateToStringYYYYMMDD(new Date()));
			  //  userInfoUpdateRequestEntity.setProvidingIdInfo(signUpBean.getEmirateRadioPath());
			//    setOldValues(oldData, userInfoUpdateRequestEntity);
			    setFlagOnChange(oldData, signUpBean, userInfoUpdateRequestEntity);
			    userInfoUpdateRequestEntity.setUserRequestAttachmentList(prepareUpdateRequestAttachmentEntity(signUpBean, oldData,userInfoUpdateRequestEntity));
			    return userInfoUpdateRequestEntity;
			  }
		  
		 /* private void setOldValues(SignUpBean oldData, UserInfoUpdateRequestEntity userInfoUpdateRequestEntity) {
			    LOGGER.info("-----inside setOldValues------------" + oldData.getNationality());
			    if (null != oldData.getEmail()) {
			      userInfoUpdateRequestEntity.setEmailOldValue(oldData.getEmail());
			    }
			    if (null != oldData.getMobileNo()) {
			      userInfoUpdateRequestEntity.setMobileOldValue(oldData.getMobileNo());
			    }
			    if (null != oldData.getNationality()) {
			      userInfoUpdateRequestEntity.setNationalityOldValue(oldData.getNationality());
			    }

			  }*/
		  
		  private void setFlagOnChange(SignUpBean oldData, SignUpBean signUpBean,
			      UserInfoUpdateRequestEntity userInfoUpdateRequestEntity) {
			
			    String oldEmirateId = oldData.getEmiratesId();
			    String newEmirateId = signUpBean.getEmiratesId();
			    LOGGER.info(" >>> oldEmirateId <<<<<<<< " + oldEmirateId + " newEmirateId >>> " + newEmirateId);
			    int emirateIdIndex = 0;
			    if (oldEmirateId != null) {
			      emirateIdIndex = oldEmirateId.indexOf('-');
			      oldEmirateId = oldEmirateId.replaceAll("-", "");
			    }
			    LOGGER.info("-----emirate Index-------" + emirateIdIndex);
			    if (newEmirateId != null) {
			      newEmirateId = newEmirateId.replaceAll("-", "");
			    }
			  
			    userInfoUpdateRequestEntity.setFirstNameChanged(verifyModifiedValues(oldData.getFirstName(),signUpBean.getFirstName()));
			    userInfoUpdateRequestEntity.setSecondNameChanged(verifyModifiedValues(oldData.getSecondName(), signUpBean.getSecondName()));
			    userInfoUpdateRequestEntity.setLastNameChanged(verifyModifiedValues(oldData.getFamilyName(), signUpBean.getFamilyName()));
			    userInfoUpdateRequestEntity.setGenderChanged(verifyModifiedValues(oldData.getGender(), signUpBean.getGender()));
			    userInfoUpdateRequestEntity.setDobChanged(verifyModifiedValues(oldData.getDateOfBirth(), signUpBean.getDateOfBirth()));
			    userInfoUpdateRequestEntity.setMaritalStatusChanged(verifyModifiedValues(oldData.getMaritalStatus(), signUpBean.getMaritalStatus()));
			    
			    userInfoUpdateRequestEntity.setEmailChanged(verifyModifiedValues(oldData.getEmail(), signUpBean.getEmail()));

			    userInfoUpdateRequestEntity
			        .setMobileNoChanged(verifyModifiedValues(oldData.getMobileNo(), signUpBean.getMobileNo()));

			    userInfoUpdateRequestEntity.setNationalityChanged(verifyModifiedValues(oldData.getNationality(),
			        signUpBean.getNationality()));
			    if ("217".equals(signUpBean.getNationality()))
			   {
			      userInfoUpdateRequestEntity.setEmirateIdChanged(verifyModifiedValues(oldEmirateId, newEmirateId));
			    }
			    else{
			    	  userInfoUpdateRequestEntity.setResidenceIdChanged(verifyModifiedValues(oldData.getResidenceId(), signUpBean.getResidenceId()));
			    	  userInfoUpdateRequestEntity.setEmirateIdChanged(verifyModifiedValues(oldEmirateId, newEmirateId));
			    }

			  }
		  
		  private boolean verifyModifiedValues(String oldValue, String newValue) {
			    if (StringUtils.isEmpty(newValue)) {
			      LOGGER.info("--------new value empty------------");
			      return false;
			    }
			    LOGGER.info("-----");
			    LOGGER.info("OLD Value " + oldValue  + " New Value " + newValue);
			    if (oldValue == null) {
			      LOGGER.info("--------returning true when old value is null------------");
			      return true;
			    } else if (!oldValue.equalsIgnoreCase(newValue)) {
			      LOGGER.info("--------returning true when old value is not matching with new value------------");
			      return true;
			    } else {
			      LOGGER.info("--------returning false ------------");
			      return false;
			    }

			  }
		  
		  private List<UserUpdateRequestAttachmentEntity> prepareUpdateRequestAttachmentEntity(SignUpBean signUpBean,
			      SignUpBean oldData, UserInfoUpdateRequestEntity userInfoUpdateRequestEntity) {
			    List<UserUpdateRequestAttachmentEntity> entityList = new ArrayList<UserUpdateRequestAttachmentEntity>();

			    UserUpdateRequestAttachmentEntity emirateAttachmentNew = new UserUpdateRequestAttachmentEntity();
			    UserUpdateRequestAttachmentEntity residenceAttachmentNew = new UserUpdateRequestAttachmentEntity();
			    
			    if ("217".equals(signUpBean.getNationality())){
			    	
			    	 if (signUpBean.getEmirateFileContent() != null && signUpBean.getEmirateFileContent().length > 0) {
					        userInfoUpdateRequestEntity.setEmirateAttachmentChanged(true);
					      }
			    	 userInfoUpdateRequestEntity.setEmirateExpiryDateChanged(verifyModifiedValues(oldData.getEmirateExpireDate(),
					          signUpBean.getEmirateExpireDate()));
			    	 emirateAttachmentNew = setUpdateRequestEmirateAttachments(signUpBean, oldData.getUserId(), emirateAttachmentNew);
				      entityList.add(emirateAttachmentNew);

			    }else{

			    	 if (signUpBean.getEmirateFileContent() != null && signUpBean.getEmirateFileContent().length > 0) {
					        userInfoUpdateRequestEntity.setEmirateAttachmentChanged(true);
					      }
			    	  if (signUpBean.getResidenceFileContent() != null && signUpBean.getResidenceFileContent().length > 0) {
					        userInfoUpdateRequestEntity.setResidenceAttachmentChanged(true);
					      }
			    	  userInfoUpdateRequestEntity.setEmirateExpiryDateChanged(verifyModifiedValues(oldData.getEmirateExpireDate(),
					          signUpBean.getEmirateExpireDate()));
			    	  userInfoUpdateRequestEntity.setResidenceExpiryDateChanged(verifyModifiedValues(oldData.getResidenceExpireDate(),
					          signUpBean.getResidenceExpireDate()));

			    	  residenceAttachmentNew = setUpdateResidentialAttachments(signUpBean, oldData.getUserId(), residenceAttachmentNew);
				      entityList.add(residenceAttachmentNew);
				      emirateAttachmentNew = setUpdateRequestEmirateAttachments(signUpBean, oldData.getUserId(), emirateAttachmentNew);
				      entityList.add(emirateAttachmentNew);
				      
			    }

			    return entityList;

			  }
		  
		  private UserUpdateRequestAttachmentEntity setUpdateRequestEmirateAttachments(SignUpBean signUpBean, long userId,
			      UserUpdateRequestAttachmentEntity emirateAttachmentNew) {
			    SignUpAttachmentEntity emirateAttachment = null;
			    if (signUpBean.getEmirateFileContent() != null && signUpBean.getEmirateFileContent().length > 0) {
			      LOGGER.info("---------inside new saving attachment condition------------");
			      emirateAttachmentNew.setUserId(userId);
			      emirateAttachmentNew.setContent(signUpBean.getEmirateFileContent());
			      emirateAttachmentNew.setContentType(signUpBean.getEmirateFileContentType());
			      emirateAttachmentNew.setDocExtension(signUpBean.getEmirateFileExt());
			      emirateAttachmentNew.setDocName(SignUpConstant.EMIRATE_ID);
			      emirateAttachmentNew.setIdNumber(signUpBean.getEmiratesId());
			      emirateAttachmentNew.setValidTo(dateToStringYYYYMMDD(signUpBean.getEmirateExpireDate()));

			    } else {
			      emirateAttachment = profileDao.getAttachmentById(signUpBean.getEmirateFileId());
			      if (null != emirateAttachment) {

			        LOGGER.info("-------inside else condition of attachment---content-------" + emirateAttachment.getContent());
			        emirateAttachmentNew.setUserId(userId);
			        emirateAttachmentNew.setContent(emirateAttachment.getContent());
			        emirateAttachmentNew.setContentType(emirateAttachment.getContentType());
			        emirateAttachmentNew.setDocExtension(emirateAttachment.getDocExtension());
			        emirateAttachmentNew.setDocName(emirateAttachment.getDocName());
			        emirateAttachmentNew.setIdNumber(signUpBean.getEmiratesId());
			        emirateAttachmentNew.setValidTo(dateToStringYYYYMMDD(signUpBean.getEmirateExpireDate()));

			      }
			    }

			    return emirateAttachmentNew;
			  }
		  
		  private UserUpdateRequestAttachmentEntity setUpdateResidentialAttachments(SignUpBean signUpBean, long userId,
			      UserUpdateRequestAttachmentEntity residenceAttachmentNew) {
			  SignUpAttachmentEntity residenceAttachment = null;
			    if (signUpBean.getResidenceFileContent() != null && signUpBean.getResidenceFileContent().length > 0) {
			        residenceAttachmentNew = new UserUpdateRequestAttachmentEntity();
			        residenceAttachmentNew.setUserId(userId);
			        residenceAttachmentNew.setContent(signUpBean.getResidenceFileContent());
			        residenceAttachmentNew.setContentType(signUpBean.getResidenceFileContentType());
			        residenceAttachmentNew.setDocExtension(signUpBean.getResidenceFileExt());
			        residenceAttachmentNew.setDocName(SignUpConstant.RESIDENCE);
			        residenceAttachmentNew.setIdNumber(signUpBean.getResidenceId());
			        residenceAttachmentNew.setValidTo(dateToStringYYYYMMDD(signUpBean.getResidenceExpireDate()));
			      } else {
			        residenceAttachment = profileDao.getAttachmentById(signUpBean.getResidenceFileId());
			        if (null != residenceAttachment) {
			          residenceAttachmentNew.setUserId(userId);
			          residenceAttachmentNew.setContent(residenceAttachment.getContent());
			          residenceAttachmentNew.setContentType(residenceAttachment.getContentType());
			          residenceAttachmentNew.setDocExtension(residenceAttachment.getDocExtension());
			          residenceAttachmentNew.setDocName(residenceAttachment.getDocName());
			          residenceAttachmentNew.setIdNumber(signUpBean.getResidenceId());
			          residenceAttachmentNew.setValidTo(dateToStringYYYYMMDD(signUpBean.getResidenceExpireDate()));
			        }
			      }
			    
			    return residenceAttachmentNew;
			  }
		  
		  public static String dateToStringYYYYMMDD(Date date) {
			    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			    return formatter.format(date);
			  }
		  
		  
		  
		  public String dateToStringYYYYMMDD(String strDate) {
			    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			    return formatter.format(stringDDMMYYYYToDate(strDate));
			  }
		  
		  public Date stringDDMMYYYYToDate(String strDate) {
			    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			    Date date = null;;
			    try {
			      date = formatter.parse(strDate);
			    } catch (ParseException e) {
			      LOGGER.error("Exception while parsing date " + strDate + " : " + e);
			    }
			    return date;
			  }
		  
		
		  
		  public void sendUserRequestEmail(String to, String subjectMessageKey, String bodyMessageKey, String refID,
			      String regarding, String userName,Properties appProperties,Locale locale) {
			  LOGGER.info("Sending user request email and user " + to + " userName >>> " + userName);
			  SendMailUtility mailService = new SendMailUtility();
			  
			  try{
				String mailSubject = appProperties.getProperty(subjectMessageKey);
				String emailBody =  MessageFormat.format(appProperties.getProperty(bodyMessageKey),userName);
				LOGGER.info("Email body >> " + emailBody);
				String regards = appProperties.getProperty(regarding);
				  mailService.sendMail("noreply@ega.rak.ae", to, mailSubject, emailBody, locale, regards);
			  }catch (Exception e) {
			      LOGGER.error("Exception while sending mail to user for update request", e);
			    }
		  }
		  
		  public void sendAdminEmailForRequest(String to, String subjectMessageKey, String bodyMessageKey, String refID,
			      String regarding, String userName,Properties appProperties,Locale locale) {
			  LOGGER.info("Sending Admin request email ");
			  SendMailUtility mailService = new SendMailUtility();
			  try{
					String mailSubject = appProperties.getProperty(subjectMessageKey);
					String emailBody =  MessageFormat.format(appProperties.getProperty(bodyMessageKey),refID);
					String regards = appProperties.getProperty(regarding);
					LOGGER.info("admin email body >> " + emailBody);
					  mailService.sendMail("noreply@ega.rak.ae", to, mailSubject, emailBody, locale, regards);
				  }catch (Exception e) {
				      LOGGER.error("Exception while sending mail to user for update request", e);
				    }
		  }
		  
		  private void sendSmsNoticficationForUpdateRequest(String messageKey, String mobileNumber, String refID, String lang,Properties appProperties)
			     {
			  
			    LOGGER.info("in sendSmsNoticficationForrequest # Sending..... SMS notification to mobile number:" + mobileNumber);
			   // String message = appProperties.getProperty(messageKey);
			    
			      String message = MessageFormat.format(appProperties.getProperty(messageKey), refID);
			      LOGGER.info("message >>> " + message);
			      
			      SMSUtils sms = new SMSUtils();
			      String response = sms.sendSMS(mobileNumber, message);
			      LOGGER.info("SMS notification has been sent successfully and received message " + response);

			  }
		  
		  public String saveOrUpdateDedData(SignUpBean signUpBean) {
			    boolean dedRegSuccess = false;
			    String wsResponse = null;
			    try {
			      String emirateImageId = "0";
			      String passportImageId = "0";
			      String residenceImageId = "0";

			      if (signUpBean.getEmirateFileContent() != null && signUpBean.getEmirateFileContent().length > 0) {
			        MultipartFileUtil emirateDocument = new MultipartFileUtil(signUpBean.getEmirateFileName(),
			            signUpBean.getEmirateFileContent(), signUpBean.getEmirateFileContentType(), signUpBean.getEmirateFileExt());
			        emirateImageId = httpClientUtil.uploadImage(emirateDocument);
			      }

			      if (signUpBean.getResidenceFileContent() != null && signUpBean.getResidenceFileContent().length > 0) {
			        MultipartFileUtil emirateDocument = new MultipartFileUtil(signUpBean.getResidenceFileName(),
			            signUpBean.getResidenceFileContent(), signUpBean.getResidenceFileContentType(),
			            signUpBean.getResidenceFileExt());
			        residenceImageId = httpClientUtil.uploadImage(emirateDocument);
			      }

			      if (signUpBean.getPassportFileContent() != null && signUpBean.getPassportFileContent().length > 0) {
			        MultipartFileUtil passportDocument = new MultipartFileUtil(signUpBean.getPassportFileName(),
			            signUpBean.getPassportFileContent(), signUpBean.getPassportFileContentType(),
			            signUpBean.getPassportFileExt());
			        passportImageId = httpClientUtil.uploadImage(passportDocument);
			      }
			      signUpBean.setEmirateImageId(emirateImageId == null ? "0" : emirateImageId);
			      signUpBean.setPassportImageId(passportImageId == null ? "0" : passportImageId);
			      signUpBean.setResidenceImageId(residenceImageId == null ? "0" : residenceImageId);
			     
			        int updatedResult = httpClientUtil.updateUser(httpClientUtil.prepareSignUpForm(signUpBean));
			      
			        LOGGER.info("Updated Result >>> " + updatedResult);

			      if (SignUpConstant.ZERO_STRING.equals(wsResponse)) {
			        dedRegSuccess = false;
			      } else {
			        dedRegSuccess = true;
			      }
			    } catch (ParserConfigurationException e) {
			      dedRegSuccess = false;
			      LOGGER.error("ParserConfigurationException while saving the user information in DED", e);
			    } catch (SAXException e) {
			      dedRegSuccess = false;
			      LOGGER.error("SAXException while saving the user information in DED", e);
			    } catch (IOException e) {
			      dedRegSuccess = false;
			      LOGGER.error("IOException while saving the user information in DED", e);
			    } catch (Exception e) {
			      dedRegSuccess = false;
			      LOGGER.error("Exception while saving the user information in DED", e);
			    }
			    return wsResponse;

			  }
		  
		  private DedUserInfoEntity preparedLocalDedData(SignUpBean signUpBean) {
			    DedUserInfoEntity entity = null;
			    entity = profileDao.getLocalDedData(signUpBean.getUserId());
			    if (entity == null) {
			      entity = new DedUserInfoEntity();
			    }
			    entity.setUserId(signUpBean.getUserId());
			    entity.setSubscriberId(signUpBean.getSubscriberId());
			    entity.setFullName(signUpBean.getUserName());
			    entity.setEmail(signUpBean.getEmail());
			    entity.setMobileNumber(signUpBean.getMobileNo());
			    entity.setDateOfBirth(dateToStringYYYYMMDD(signUpBean.getDateOfBirth()));
			    if (StringUtils.isNotEmpty(signUpBean.getEmirateExpireDate())) {
			      entity.setNationalIdExpiryDate(dateToStringYYYYMMDD(signUpBean.getEmirateExpireDate()));
			      entity.setNationalIdFileId(signUpBean.getEmirateImageId());
			      entity.setNationalId(signUpBean.getEmiratesId());
			    }
			    if (StringUtils.isNotEmpty(signUpBean.getPassportExpiryDate())) {
			      entity.setPassportExpiryDate(dateToStringYYYYMMDD(signUpBean.getPassportExpiryDate()));
			      entity.setPassportNumber(signUpBean.getPassportNo());
			      entity.setPassportFileId(signUpBean.getPassportImageId());
			    }
			    if (StringUtils.isNotEmpty(signUpBean.getResidenceExpireDate())) {
			      entity.setResidenceExpiryDate(dateToStringYYYYMMDD(signUpBean.getResidenceExpireDate()));
			      entity.setResidencNumber(signUpBean.getResidenceId());
			      entity.setResidenceFileId(signUpBean.getResidenceImageId());
			    }
			    LOGGER.info("Country ID >>>>>>>>>>>>>> " + signUpBean.getNationality());
			    entity.setNationalityCountryId(signUpBean.getNationality());
			    if (SignUpConstant.ENGLISH.equalsIgnoreCase(signUpBean.getLocale().getLanguage())) {
			      entity.setPrefferedLanguageId(LANG_E);
			    } else {
			      entity.setPrefferedLanguageId(LANG_A);
			    }
			    entity.setNationalityId(signUpBean.getNationality());
			    entity.setApprovedUse("-1");

			    return entity;
			  }

		@Override
		public void updateUserpasswordInDB(String userName, String userPassword) throws Exception {
			String password = EncryptionUtility.encrypt(userPassword);
			profileDao.updateUserpasswordInDB(userName, password);
		}
}
