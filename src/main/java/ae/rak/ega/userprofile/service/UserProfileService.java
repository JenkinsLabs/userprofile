package ae.rak.ega.userprofile.service;

import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import ae.rak.ega.userprofile.command.SignUpBean;
import ae.rak.ega.userprofile.model.GenderEntity;
import ae.rak.ega.userprofile.model.LiferayCountryEntity;
import ae.rak.ega.userprofile.model.MaritalStatusEntity;
import ae.rak.ega.userprofile.model.SignUpAttachmentEntity;
import ae.rak.ega.userprofile.model.SignUpEntity;

public interface UserProfileService {
	public SignUpBean getUserProfile(String userName,PortletRequest request);
	public List<LiferayCountryEntity> getCountries(String locale);
	 public SignUpAttachmentEntity getAttachmentById(int id);
	 public List<GenderEntity> getGenderEntityList();
	 public List<MaritalStatusEntity> getMaritalStatusList();
	 public void updateGeneralUserInfo(SignUpBean signUpBean, String userName);
	 public SignUpBean getUserUpdateRequestInfo(long userId);
	  public boolean isEmailAvailable(String email);
	  public boolean isMobileNUmberAvailable(String mobile);
	  public boolean isUniqueIdAvailable(String uniquid);
	  public String saveUserUpdateRequest(SignUpBean signUpBean, String userName,Locale locale);
	  
	  public void updateUserpasswordInDB(String userName, String userPassword) throws Exception;
}
