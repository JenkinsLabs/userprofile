package ae.rak.ega.userprofile.command;

/**
 * @author Sandeep Kumar Puvvada
 *
 * Class AttachmentBean
 * Version 1.0
 * 
 */

public class AttachmentBean {

  private int attachmentId;

  private String attachmentType;

  private byte[] content;

  private String extension;

  private String fileName;

  public int getAttachmentId() {
    return attachmentId;
  }

  public void setAttachmentId(int attachmentId) {
    this.attachmentId = attachmentId;
  }

  public String getAttachmentType() {
    return attachmentType;
  }

  public void setAttachmentType(String attachmentType) {
    this.attachmentType = attachmentType;
  }

  public byte[] getContent() {
    return content;
  }

  public void setContent(byte[] content) {
    this.content = content;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  @Override
  public String toString() {
    return "AttachmentBean [attachmentId=" + attachmentId + ", attachmentType=" + attachmentType + ", extension="
        + extension + ", fileName=" + fileName + "]";
  }

}
