package ae.rak.ega.userprofile.command;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author Satish
 *
 * Class SignUpBean
 * Version 1.0
 * 
 */
public class SignUpBean {

  private String registrationType;
  private String userName;
  private String password;
  private String confirmPassword;
  private String firstName;
  private String secondName;
  private String familyName;
  private String dateOfBirth;
  private String email;
  private String confirmEmail;
  private String mobileNo;
  private String nationality;

  private String emiratesId;
  private int emirateFileId;
  private String emirateExpireDate;
  private CommonsMultipartFile emirateAttachement;
  private String emirateFileName;
  private String emirateFileExt;
  private byte[] emirateFileContent;
  private String emirateFileContentType;

  private String residenceId;
  private int residenceFileId;
  private String residenceExpireDate;
  private CommonsMultipartFile residenceAttachement;
  private String residenceFileName;
  private String residenceFileExt;
  private byte[] residenceFileContent;
  private String residenceFileContentType;

  private String passportNo;
  private int passportFileId;
  private String passportExpiryDate;
  private CommonsMultipartFile passportAttachement;
  private String passportFileName;
  private String passportFileExt;
  private byte[] passportFileContent;
  private String passportFileContentType;

  private String terms;
  private String captchaText;
  private Locale locale;
  private long userId;

  private String otpRedirectUrl;
  private ActionRequest request;
  private String otpPwd;

  private String radioPassword;
  private String gender;
  private String maritalStatus;
  private String status;
  /**
   * DED extra fields starts
   */
  private String emirateImageId;
  private String passportImageId;
  private String residenceImageId;

  private long egaUserExtId;
  private String extraEmailCheck;
  private String extraUserCheck;
  private String extraEmirateCheck;
  private String extraResidenceCheck;
  private String extraPassportCheck;
  private String extraMobileCheck;
  private String nationalityValue;
  private String genderValue;
  private String maritalStatusValue;

  private int tempEmirateFileId;
  private int tempResidenceFileId;
  private int emirateBackValue;
  private int residenceBackValue;
  private int passportBackValue;
  private Date otpCreatedDate;
  private Date otpModifiedDate;
  private String buttonName;
  private String subscriberId;
  /**
   * DED extra fields ends
   */
  private List<AttachmentBean> attachmentList;

  private String emailOldValue;
  private String mobileOldValue;
  private String nationalityOldValue;
  private String emirateIdOldValue;
  private String idExpiryOldValue;
  private String unifiedOldValue;
  private String unifiedExpiryOldvalue;

  private String environment;
  private String emirateRadioPath;
  private String emirateIdGCC;
  private String extraEmirateCheckGCC;

  public String getRegistrationType() {
    return registrationType;
  }

  public void setRegistrationType(String registrationType) {
    this.registrationType = registrationType;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    if (userName != null) {
      this.userName = userName.trim();
    }
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    if (password != null) {
      this.password = password.trim();
    }
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    if (confirmPassword != null) {
      this.confirmPassword = confirmPassword.trim();
    }
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    if (firstName != null) {
      this.firstName = firstName.trim();
    }
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    if (secondName != null) {
      this.secondName = secondName.trim();
    }
  }

  public String getFamilyName() {
    return familyName;
  }

  public void setFamilyName(String familyName) {
    if (familyName != null) {
      this.familyName = familyName.trim();
    }
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    if (dateOfBirth != null) {
      this.dateOfBirth = dateOfBirth.trim();
    }
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    if (email != null) {
      this.email = email.trim();
    }
  }

  public String getConfirmEmail() {
    return confirmEmail;
  }

  public void setConfirmEmail(String confirmEmail) {
    if (confirmEmail != null) {
      this.confirmEmail = confirmEmail.trim();
    }
  }

  public String getMobileNo() {
    return mobileNo;
  }

  public void setMobileNo(String mobileNo) {
    if (mobileNo != null) {
      this.mobileNo = mobileNo.trim();
    }
  }

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    if (nationality != null) {
      this.nationality = nationality.trim();
    }
  }

  public String getEmiratesId() {
    return emiratesId;
  }

  public void setEmiratesId(String emiratesId) {
    if (emiratesId != null) {
      this.emiratesId = emiratesId.trim();
    } else {
      this.emiratesId = "";
    }
  }

  public String getEmirateExpireDate() {
    return emirateExpireDate;
  }

  public void setEmirateExpireDate(String emirateExpireDate) {
    if (emirateExpireDate != null) {
      this.emirateExpireDate = emirateExpireDate.trim();
    } else {
      this.emirateExpireDate = "";
    }
  }

  public CommonsMultipartFile getEmirateAttachement() {
    return emirateAttachement;
  }

  public void setEmirateAttachement(CommonsMultipartFile emirateAttachement) {
    this.emirateAttachement = emirateAttachement;
    this.emirateFileName = emirateAttachement.getOriginalFilename();
    this.emirateFileContent = emirateAttachement.getBytes();
    this.emirateFileExt = getFileExtension(this.emirateFileName);
    this.emirateFileContentType = emirateAttachement.getContentType();
  }

  public String getEmirateFileName() {
    return emirateFileName;
  }

  public void setEmirateFileName(String emirateFileName) {
    this.emirateFileName = emirateFileName;
  }

  public String getEmirateFileExt() {
    return emirateFileExt;
  }

  public void setEmirateFileExt(String emirateFileExt) {
    this.emirateFileExt = emirateFileExt;
  }

  public byte[] getEmirateFileContent() {
    return emirateFileContent;
  }

  public void setEmirateFileContent(byte[] emirateFileContent) {
    this.emirateFileContent = emirateFileContent;
  }

  public String getEmirateFileContentType() {
    return emirateFileContentType;
  }

  public void setEmirateFileContentType(String emirateFileContentType) {
    this.emirateFileContentType = emirateFileContentType;
  }

  public String getResidenceId() {
    return residenceId;
  }

  public void setResidenceId(String residenceId) {
    if (residenceId != null) {
      this.residenceId = residenceId.trim();
    } else {
      this.residenceId = "";
    }
  }

  public String getResidenceExpireDate() {
    return residenceExpireDate;
  }

  public void setResidenceExpireDate(String residenceExpireDate) {
    if (residenceExpireDate != null) {
      this.residenceExpireDate = residenceExpireDate.trim();
    } else {
      this.residenceExpireDate = "";
    }
  }

  public CommonsMultipartFile getResidenceAttachement() {
    return residenceAttachement;
  }

  public void setResidenceAttachement(CommonsMultipartFile residenceAttachement) {
    this.residenceAttachement = residenceAttachement;
    this.residenceFileName = residenceAttachement.getOriginalFilename();
    this.residenceFileContent = residenceAttachement.getBytes();
    this.residenceFileExt = getFileExtension(this.residenceFileName);
    this.residenceFileContentType = residenceAttachement.getContentType();
  }

  public String getResidenceFileName() {
    return residenceFileName;
  }

  public void setResidenceFileName(String residenceFileName) {
    this.residenceFileName = residenceFileName;
  }

  public String getResidenceFileExt() {
    return residenceFileExt;
  }

  public void setResidenceFileExt(String residenceFileExt) {
    this.residenceFileExt = residenceFileExt;
  }

  public byte[] getResidenceFileContent() {
    return residenceFileContent;
  }

  public void setResidenceFileContent(byte[] residenceFileContent) {
    this.residenceFileContent = residenceFileContent;
  }

  public String getResidenceFileContentType() {
    return residenceFileContentType;
  }

  public void setResidenceFileContentType(String residenceFileContentType) {
    this.residenceFileContentType = residenceFileContentType;
  }

  public String getPassportNo() {
    return passportNo;
  }

  public void setPassportNo(String passportNo) {
    if (passportNo != null) {
      this.passportNo = passportNo.trim();
    } else {
      this.passportNo = "";
    }
  }

  public String getPassportExpiryDate() {
    return passportExpiryDate;
  }

  public void setPassportExpiryDate(String passportExpiryDate) {
    if (passportExpiryDate != null) {
      this.passportExpiryDate = passportExpiryDate;
    } else {
      this.passportExpiryDate = "";
    }
  }

  public CommonsMultipartFile getPassportAttachement() {
    return passportAttachement;
  }

  public void setPassportAttachement(CommonsMultipartFile passportAttachement) {
    this.passportAttachement = passportAttachement;
    this.passportFileName = passportAttachement.getOriginalFilename();
    this.passportFileContent = passportAttachement.getBytes();
    this.passportFileExt = getFileExtension(this.passportFileName);
    this.passportFileContentType = passportAttachement.getContentType();
  }

  public String getPassportFileName() {
    return passportFileName;
  }

  public void setPassportFileName(String passportFileName) {
    this.passportFileName = passportFileName;
  }

  public String getPassportFileExt() {
    return passportFileExt;
  }

  public void setPassportFileExt(String passportFileExt) {
    this.passportFileExt = passportFileExt;
  }

  public byte[] getPassportFileContent() {
    return passportFileContent;
  }

  public void setPassportFileContent(byte[] passportFileContent) {
    this.passportFileContent = passportFileContent;
  }

  public String getPassportFileContentType() {
    return passportFileContentType;
  }

  public void setPassportFileContentType(String passportFileContentType) {
    this.passportFileContentType = passportFileContentType;
  }

  public String getCaptchaText() {
    return captchaText;
  }

  public void setCaptchaText(String captchaText) {
    if (captchaText != null) {
      this.captchaText = captchaText.trim();
    }
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getTerms() {
    return terms;
  }

  public void setTerms(String terms) {
    this.terms = terms;
  }

  public List<AttachmentBean> getAttachmentList() {
    return attachmentList;
  }

  public void setAttachmentList(List<AttachmentBean> attachmentList) {
    this.attachmentList = attachmentList;
  }

  private String getFileExtension(String atachmentFileName) {

    String docExtention = "";
    if (atachmentFileName != null && atachmentFileName.length() > 0) {
      int lastIndexOfDotDoc = atachmentFileName.lastIndexOf('.');
      docExtention = atachmentFileName.substring(lastIndexOfDotDoc, atachmentFileName.length());
    }
    return docExtention;
  }

  public String getEmirateImageId() {
    return emirateImageId;
  }

  public void setEmirateImageId(String emirateImageId) {
    this.emirateImageId = emirateImageId;
  }

  public String getPassportImageId() {
    return passportImageId;
  }

  public void setPassportImageId(String passportImageId) {
    this.passportImageId = passportImageId;
  }

  public String getResidenceImageId() {
    return residenceImageId;
  }

  public void setResidenceImageId(String residenceImageId) {
    this.residenceImageId = residenceImageId;
  }

  public String getOtpRedirectUrl() {
    return otpRedirectUrl;
  }

  public void setOtpRedirectUrl(String otpRedirectUrl) {
    this.otpRedirectUrl = otpRedirectUrl;
  }

  public ActionRequest getRequest() {
    return request;
  }

  public void setRequest(ActionRequest request) {
    this.request = request;
  }

  public String getOtpPwd() {
    return otpPwd;
  }

  public void setOtpPwd(String otpPwd) {
    this.otpPwd = otpPwd;
  }

  public int getEmirateFileId() {
    return emirateFileId;
  }

  public void setEmirateFileId(int emirateFileId) {
    this.emirateFileId = emirateFileId;
  }

  public int getResidenceFileId() {
    return residenceFileId;
  }

  public void setResidenceFileId(int residenceFileId) {
    this.residenceFileId = residenceFileId;
  }

  public int getPassportFileId() {
    return passportFileId;
  }

  public void setPassportFileId(int passportFileId) {
    this.passportFileId = passportFileId;
  }

  public long getEgaUserExtId() {
    return egaUserExtId;
  }

  public void setEgaUserExtId(long egaUserExtId) {
    this.egaUserExtId = egaUserExtId;
  }

  public String getRadioPassword() {
    return radioPassword;
  }

  public void setRadioPassword(String radioPassword) {
    this.radioPassword = radioPassword;
  }

  public String getExtraEmailCheck() {
    return extraEmailCheck;
  }

  public void setExtraEmailCheck(String extraEmailCheck) {
    this.extraEmailCheck = extraEmailCheck;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getMaritalStatus() {
    return maritalStatus;
  }

  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  public String getExtraUserCheck() {
    return extraUserCheck;
  }

  public void setExtraUserCheck(String extraUserCheck) {
    this.extraUserCheck = extraUserCheck;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getNationalityValue() {
    return nationalityValue;
  }

  public void setNationalityValue(String nationalityValue) {
    this.nationalityValue = nationalityValue;
  }

  public String getGenderValue() {
    return genderValue;
  }

  public void setGenderValue(String genderValue) {
    this.genderValue = genderValue;
  }

  public String getMaritalStatusValue() {
    return maritalStatusValue;
  }

  public void setMaritalStatusValue(String maritalStatusValue) {
    this.maritalStatusValue = maritalStatusValue;
  }

  public int getTempEmirateFileId() {
    return tempEmirateFileId;
  }

  public void setTempEmirateFileId(int tempEmirateFileId) {
    this.tempEmirateFileId = tempEmirateFileId;
  }

  public int getTempResidenceFileId() {
    return tempResidenceFileId;
  }

  public void setTempResidenceFileId(int tempResidenceFileId) {
    this.tempResidenceFileId = tempResidenceFileId;
  }

  public int getEmirateBackValue() {
    return emirateBackValue;
  }

  public void setEmirateBackValue(int emirateBackValue) {
    this.emirateBackValue = emirateBackValue;
  }

  public int getResidenceBackValue() {
    return residenceBackValue;
  }

  public void setResidenceBackValue(int residenceBackValue) {
    this.residenceBackValue = residenceBackValue;
  }

  public Date getOtpCreatedDate() {
    return otpCreatedDate;
  }

  public void setOtpCreatedDate(Date otpCreatedDate) {
    this.otpCreatedDate = otpCreatedDate;
  }

  public Date getOtpModifiedDate() {
    return otpModifiedDate;
  }

  public void setOtpModifiedDate(Date otpModifiedDate) {
    this.otpModifiedDate = otpModifiedDate;
  }

  public String getButtonName() {
    return buttonName;
  }

  public void setButtonName(String buttonName) {
    this.buttonName = buttonName;
  }

  public String getSubscriberId() {
    return subscriberId;
  }

  public void setSubscriberId(String subscriberId) {
    this.subscriberId = subscriberId;
  }

  public String getExtraEmirateCheck() {
    return extraEmirateCheck;
  }

  public void setExtraEmirateCheck(String extraEmirateCheck) {
    this.extraEmirateCheck = extraEmirateCheck;
  }

  public String getExtraResidenceCheck() {
    return extraResidenceCheck;
  }

  public void setExtraResidenceCheck(String extraResidenceCheck) {
    this.extraResidenceCheck = extraResidenceCheck;
  }

  public String getExtraMobileCheck() {
    return extraMobileCheck;
  }

  public void setExtraMobileCheck(String extraMobileCheck) {
    this.extraMobileCheck = extraMobileCheck;
  }

  public String getEmailOldValue() {
    return emailOldValue;
  }

  public void setEmailOldValue(String emailOldValue) {
    this.emailOldValue = emailOldValue;
  }

  public String getMobileOldValue() {
    return mobileOldValue;
  }

  public void setMobileOldValue(String mobileOldValue) {
    this.mobileOldValue = mobileOldValue;
  }

  public String getNationalityOldValue() {
    return nationalityOldValue;
  }

  public void setNationalityOldValue(String nationalityOldValue) {
    this.nationalityOldValue = nationalityOldValue;
  }

  public String getEmirateIdOldValue() {
    return emirateIdOldValue;
  }

  public void setEmirateIdOldValue(String emirateIdOldValue) {
    this.emirateIdOldValue = emirateIdOldValue;
  }

  public String getIdExpiryOldValue() {
    return idExpiryOldValue;
  }

  public void setIdExpiryOldValue(String idExpiryOldValue) {
    this.idExpiryOldValue = idExpiryOldValue;
  }

  public String getUnifiedOldValue() {
    return unifiedOldValue;
  }

  public void setUnifiedOldValue(String unifiedOldValue) {
    this.unifiedOldValue = unifiedOldValue;
  }

  public String getUnifiedExpiryOldvalue() {
    return unifiedExpiryOldvalue;
  }

  public void setUnifiedExpiryOldvalue(String unifiedExpiryOldvalue) {
    this.unifiedExpiryOldvalue = unifiedExpiryOldvalue;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getEmirateRadioPath() {
    return emirateRadioPath;
  }

  public void setEmirateRadioPath(String emirateRadioPath) {
    this.emirateRadioPath = emirateRadioPath;
  }

  public int getPassportBackValue() {
    return passportBackValue;
  }

  public void setPassportBackValue(int passportBackValue) {
    this.passportBackValue = passportBackValue;
  }

  public String getExtraPassportCheck() {
    return extraPassportCheck;
  }

  public void setExtraPassportCheck(String extraPassportCheck) {
    this.extraPassportCheck = extraPassportCheck;
  }

  public String getEmirateIdGCC() {
    return emirateIdGCC;
  }

  public void setEmirateIdGCC(String emirateIdGCC) {
    this.emirateIdGCC = emirateIdGCC;
  }

  public String getExtraEmirateCheckGCC() {
    return extraEmirateCheckGCC;
  }

  public void setExtraEmirateCheckGCC(String extraEmirateCheckGCC) {
    this.extraEmirateCheckGCC = extraEmirateCheckGCC;
  }

  @Override
  public String toString() {
    return "SignUpBean [registrationType=" + registrationType + ", userName=" + userName + ", password=" + password
        + ", confirmPassword=" + confirmPassword + ", firstName=" + firstName + ", secondName=" + secondName
        + ", familyName=" + familyName + ", dateOfBirth=" + dateOfBirth + ", email=" + email + ", confirmEmail="
        + confirmEmail + ", mobileNo=" + mobileNo + ", nationality=" + nationality + ", emiratesId=" + emiratesId
        + ", emirateExpireDate=" + emirateExpireDate + ", emirateAttachement=" + emirateAttachement
        + ", emirateFileName=" + emirateFileName + ", emirateFileExt=" + emirateFileExt + ", emirateFileContentType="
        + emirateFileContentType + ", residenceId=" + residenceId + ", residenceExpireDate=" + residenceExpireDate
        + ", residenceAttachement=" + residenceAttachement + ", residenceFileName=" + residenceFileName
        + ", residenceFileExt=" + residenceFileExt + ", residenceFileContentType=" + residenceFileContentType
        + ", passportNo=" + passportNo + ", passportExpiryDate=" + passportExpiryDate + ", passportAttachement="
        + passportAttachement + ", passportFileName=" + passportFileName + ", passportFileExt=" + passportFileExt
        + ", passportFileContentType=" + passportFileContentType + ", terms=" + terms + ", captchaText=" + captchaText
        + ", locale=" + locale + ", userId=" + userId + ", attachmentList=" + attachmentList + "]";
  }

}
