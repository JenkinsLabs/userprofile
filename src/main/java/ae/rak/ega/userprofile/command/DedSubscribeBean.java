package ae.rak.ega.userprofile.command;

import java.util.List;
import java.util.Locale;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author Sandeep Kumar Puvvada
 *
 * Class DedSubscribeBean
 * Version 1.0
 * 
 */
public class DedSubscribeBean {

  private String registrationType;
  private String userName;
  private String password;
  private String confirmPassword;
  private String firstName;
  private String secondName;
  private String familyName;
  private String dateOfBirth;
  private String email;
  private String confirmEmail;
  private String mobileNo;
  private String nationality;

  private String emiratesId;
  private String emirateExpireDate;
  private CommonsMultipartFile emirateAttachement;
  private String emirateFileName;
  private String emirateFileExt;
  private byte[] emirateFileContent;
  private String emirateFileContentType;

  private String residenceId;
  private String residenceExpireDate;
  private CommonsMultipartFile residenceAttachement;
  private String residenceFileName;
  private String residenceFileExt;
  private byte[] residenceFileContent;
  private String residenceFileContentType;

  private String passportNo;
  private String passportExpiryDate;
  private CommonsMultipartFile passportAttachement;
  private String passportFileName;
  private String passportFileExt;
  private byte[] passportFileContent;
  private String passportFileContentType;

  private String terms;
  private String captchaText;
  private Locale locale;
  private String userId;

  /**
   * DED extra fields starts
   */
  private String emirateImageId;
  private String passportImageId;
  private String residenceImageId;
  private String appType;
  private String interfaceLang;
  private String preferredLanguage;
  private String nationalityCountryId;

  private String subscriberId;
  /**
   * DED extra fields ends
   */
  private List<AttachmentBean> attachmentList;

  public String getRegistrationType() {
    return registrationType;
  }

  public void setRegistrationType(String registrationType) {
    this.registrationType = registrationType;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public String getFamilyName() {
    return familyName;
  }

  public void setFamilyName(String familyName) {
    this.familyName = familyName;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getConfirmEmail() {
    return confirmEmail;
  }

  public void setConfirmEmail(String confirmEmail) {
    this.confirmEmail = confirmEmail;
  }

  public String getMobileNo() {
    return mobileNo;
  }

  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getEmiratesId() {
    return emiratesId;
  }

  public void setEmiratesId(String emiratesId) {
    this.emiratesId = emiratesId;
  }

  public String getEmirateExpireDate() {
    return emirateExpireDate;
  }

  public void setEmirateExpireDate(String emirateExpireDate) {
    this.emirateExpireDate = emirateExpireDate;
  }

  public CommonsMultipartFile getEmirateAttachement() {
    return emirateAttachement;
  }

  public void setEmirateAttachement(CommonsMultipartFile emirateAttachement) {
    this.emirateAttachement = emirateAttachement;
    this.emirateFileName = emirateAttachement.getOriginalFilename();
    this.emirateFileContent = emirateAttachement.getBytes();
    this.emirateFileExt = getFileExtension(this.emirateFileName);
    this.emirateFileContentType = emirateAttachement.getContentType();
  }

  public String getEmirateFileName() {
    return emirateFileName;
  }

  public void setEmirateFileName(String emirateFileName) {
    this.emirateFileName = emirateFileName;
  }

  public String getEmirateFileExt() {
    return emirateFileExt;
  }

  public void setEmirateFileExt(String emirateFileExt) {
    this.emirateFileExt = emirateFileExt;
  }

  public byte[] getEmirateFileContent() {
    return emirateFileContent;
  }

  public void setEmirateFileContent(byte[] emirateFileContent) {
    this.emirateFileContent = emirateFileContent;
  }

  public String getEmirateFileContentType() {
    return emirateFileContentType;
  }

  public void setEmirateFileContentType(String emirateFileContentType) {
    this.emirateFileContentType = emirateFileContentType;
  }

  public String getResidenceId() {
    return residenceId;
  }

  public void setResidenceId(String residenceId) {
    this.residenceId = residenceId;
  }

  public String getResidenceExpireDate() {
    return residenceExpireDate;
  }

  public void setResidenceExpireDate(String residenceExpireDate) {
    this.residenceExpireDate = residenceExpireDate;
  }

  public CommonsMultipartFile getResidenceAttachement() {
    return residenceAttachement;
  }

  public void setResidenceAttachement(CommonsMultipartFile residenceAttachement) {
    this.residenceAttachement = residenceAttachement;
    this.residenceFileName = residenceAttachement.getOriginalFilename();
    this.residenceFileContent = residenceAttachement.getBytes();
    this.residenceFileExt = getFileExtension(this.residenceFileName);
    this.residenceFileContentType = residenceAttachement.getContentType();
  }

  public String getResidenceFileName() {
    return residenceFileName;
  }

  public void setResidenceFileName(String residenceFileName) {
    this.residenceFileName = residenceFileName;
  }

  public String getResidenceFileExt() {
    return residenceFileExt;
  }

  public void setResidenceFileExt(String residenceFileExt) {
    this.residenceFileExt = residenceFileExt;
  }

  public byte[] getResidenceFileContent() {
    return residenceFileContent;
  }

  public void setResidenceFileContent(byte[] residenceFileContent) {
    this.residenceFileContent = residenceFileContent;
  }

  public String getResidenceFileContentType() {
    return residenceFileContentType;
  }

  public void setResidenceFileContentType(String residenceFileContentType) {
    this.residenceFileContentType = residenceFileContentType;
  }

  public String getPassportNo() {
    return passportNo;
  }

  public void setPassportNo(String passportNo) {
    this.passportNo = passportNo;
  }

  public String getPassportExpiryDate() {
    return passportExpiryDate;
  }

  public void setPassportExpiryDate(String passportExpiryDate) {
    this.passportExpiryDate = passportExpiryDate;
  }

  public CommonsMultipartFile getPassportAttachement() {
    return passportAttachement;
  }

  public void setPassportAttachement(CommonsMultipartFile passportAttachement) {
    this.passportAttachement = passportAttachement;
    this.passportFileName = passportAttachement.getOriginalFilename();
    this.passportFileContent = passportAttachement.getBytes();
    this.passportFileExt = getFileExtension(this.passportFileName);
    this.passportFileContentType = passportAttachement.getContentType();
  }

  public String getPassportFileName() {
    return passportFileName;
  }

  public void setPassportFileName(String passportFileName) {
    this.passportFileName = passportFileName;
  }

  public String getPassportFileExt() {
    return passportFileExt;
  }

  public void setPassportFileExt(String passportFileExt) {
    this.passportFileExt = passportFileExt;
  }

  public byte[] getPassportFileContent() {
    return passportFileContent;
  }

  public void setPassportFileContent(byte[] passportFileContent) {
    this.passportFileContent = passportFileContent;
  }

  public String getPassportFileContentType() {
    return passportFileContentType;
  }

  public void setPassportFileContentType(String passportFileContentType) {
    this.passportFileContentType = passportFileContentType;
  }

  public String getCaptchaText() {
    return captchaText;
  }

  public void setCaptchaText(String captchaText) {
    this.captchaText = captchaText;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTerms() {
    return terms;
  }

  public void setTerms(String terms) {
    this.terms = terms;
  }

  public List<AttachmentBean> getAttachmentList() {
    return attachmentList;
  }

  public void setAttachmentList(List<AttachmentBean> attachmentList) {
    this.attachmentList = attachmentList;
  }

  private String getFileExtension(String atachmentFileName) {

    String docExtention = "";
    if (atachmentFileName != null && atachmentFileName.length() > 0) {
      int lastIndexOfDotDoc = atachmentFileName.lastIndexOf('.');
      docExtention = atachmentFileName.substring(lastIndexOfDotDoc, atachmentFileName.length());
    }
    return docExtention;
  }

  public String getEmirateImageId() {
    return emirateImageId;
  }

  public void setEmirateImageId(String emirateImageId) {
    this.emirateImageId = emirateImageId;
  }

  public String getPassportImageId() {
    return passportImageId;
  }

  public void setPassportImageId(String passportImageId) {
    this.passportImageId = passportImageId;
  }

  public String getResidenceImageId() {
    return residenceImageId;
  }

  public void setResidenceImageId(String residenceImageId) {
    this.residenceImageId = residenceImageId;
  }

  public String getAppType() {
    return appType;
  }

  public void setAppType(String appType) {
    this.appType = appType;
  }

  public String getInterfaceLang() {
    return interfaceLang;
  }

  public void setInterfaceLang(String interfaceLang) {
    this.interfaceLang = interfaceLang;
  }

  public String getPreferredLanguage() {
    return preferredLanguage;
  }

  public void setPreferredLanguage(String preferredLanguage) {
    this.preferredLanguage = preferredLanguage;
  }

  public String getNationalityCountryId() {
    return nationalityCountryId;
  }

  public void setNationalityCountryId(String nationalityCountryId) {
    this.nationalityCountryId = nationalityCountryId;
  }

  public String getSubscriberId() {
    return subscriberId;
  }

  public void setSubscriberId(String subscriberId) {
    this.subscriberId = subscriberId;
  }

}
