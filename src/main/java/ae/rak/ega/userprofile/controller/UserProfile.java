package ae.rak.ega.userprofile.controller;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.ldap.LdapContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ibm.websphere.security.WSSecurityException;
import com.ibm.websphere.security.auth.callback.WSCallbackHandlerImpl;
import ae.rak.ega.userprofile.command.ChangePasswordCommand;
import ae.rak.ega.userprofile.command.SignUpBean;
import ae.rak.ega.userprofile.conatants.SignUpConstant;
import ae.rak.ega.userprofile.model.GenderEntity;
import ae.rak.ega.userprofile.model.LiferayCountryEntity;
import ae.rak.ega.userprofile.model.MaritalStatusEntity;
import ae.rak.ega.userprofile.model.SignUpAttachmentEntity;
import ae.rak.ega.userprofile.service.UserProfileService;
import ae.rak.ega.userprofile.util.TDSConnection;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

@Controller
@RequestMapping(value = "VIEW")
public class UserProfile {
	private Logger LOGGER=Logger.getLogger(UserProfile.class);
		
	@Autowired
	private UserProfileService userService;
	
	@Autowired
	private TDSConnection tdsConnection;
	
	  @ModelAttribute("signUpBean")
	  public SignUpBean getSignUpBean() {
	    return new SignUpBean();
	  }
	  
	  @ModelAttribute("changePwsdCommand")
	  public ChangePasswordCommand getChangepswdBean() {
	    return new ChangePasswordCommand();
	  }
	
	 @ModelAttribute("countryList")
	  public List<LiferayCountryEntity> getCountryList(PortletRequest request) {
	    String locale = request.getLocale().getLanguage();
	    return userService.getCountries(locale);
	  }

	  @ModelAttribute("maritalStatusList")
	  public List<MaritalStatusEntity> getMaritalStatusList() {
	    return userService.getMaritalStatusList();
	  }

	  @ModelAttribute("genderList")
	  public List<GenderEntity> getGenderList() {
	    return userService.getGenderEntityList();
	  }
	
	@RenderMapping(params = "action=showGeneralInformation")
	public String getComplainet(RenderRequest request,RenderResponse response,ModelMap map	){
		LOGGER.info("Showing "+ request.getRemoteUser() + " profile ");
		SignUpBean signUpBean = userService.getUserProfile(request.getRemoteUser(), request);
		
		SignUpBean userRequestbean = new SignUpBean();
		userRequestbean = userService.getUserUpdateRequestInfo(signUpBean.getUserId());
		   
	    if (StringUtils.isNotEmpty(userRequestbean.getStatus()) && "1".equals(userRequestbean.getStatus())) {
	    	LOGGER.info("Update Request Status " + userRequestbean.getStatus());
	      request.setAttribute("displayMode", "VIEW");
	    }

		
		map.addAttribute("signUpBean", signUpBean);
		return "userProfile";
	}
	
	  @RenderMapping(params = "action=generalSuccess")
	  public String showGeneralSuccessPage() {
	    return "general-information-success";
	  }
	 @RenderMapping
	  public String showeGeneralInformation(RenderRequest request, ModelMap map) {
	    LOGGER.info("---------update General Information method-------------");
	    SignUpBean bean = new SignUpBean();
	    bean = userService.getUserProfile(request.getRemoteUser(), request);
	    LOGGER.info("--------bean----userId-------" + bean.getUserId());
	    LOGGER.info("--------EmirateFileId Value-------" + bean.getEmirateFileId());
	    LOGGER.info("--------ResidenceFileId Value-------" + bean.getResidenceFileId());
	    map.addAttribute(SignUpConstant.SIGNUP_BEAN, bean);
	    
	    SignUpBean userRequestbean = new SignUpBean();
		userRequestbean = userService.getUserUpdateRequestInfo(bean.getUserId());
		
		LOGGER.info(" ResidenceFileId Value" + userRequestbean.getResidenceFileId());
		LOGGER.info(" EmirateFileId Value" + userRequestbean.getEmirateFileId());
		   
	    if (StringUtils.isNotEmpty(userRequestbean.getStatus()) && "1".equals(userRequestbean.getStatus())) {
	    	LOGGER.info("Update Request Status " + userRequestbean.getStatus());
	      request.setAttribute("displayMode", "VIEW");
	    }
	    
	  //  return "general-information";
	    
	    return "UserProfileDetailss";
	  }
	 
	 @RenderMapping(params = "action=ChangePassword")
	  public String changePassword(RenderRequest request) {
		 
		 new ChangePasswordCommand();
		 
		 String reurnpage = "changepassword";
		 
		 if(StringUtils.isNotBlank(request.getParameter("OldPasswordMismatch"))){
			 LOGGER.info("OldPasswordMismatch if !! " );
			 request.setAttribute("oldpasswdwrong", true);
			 reurnpage = "changepassword";
		 }
		 if(StringUtils.isNotBlank(request.getParameter("PasswordException"))){
			 
			if("Password updated successfully".equals(request.getParameter("PasswordException"))){
			 request.setAttribute("passwordupdated", "passwordsuccess");
			 LOGGER.info("Password updated successfully !! " );
			 reurnpage = "passwordsuccess";
			}
			else{
				 request.setAttribute("passwordupdatedfailed", "passwordupdatedfailed");
				 reurnpage = "changepassword";
			}
		 }
		 return reurnpage;
		 
	 }
	 
	 @ActionMapping(params = "action=updatepassword")
	 public void updatePassword(ActionRequest request,ActionResponse response,@ModelAttribute("changePwsdCommand") ChangePasswordCommand changepswd){
		 
		 LOGGER.info("Current Password !! " + changepswd.getCurrentPassword());
		 LOGGER.info("New Password !! " + changepswd.getNewPassword());
		 LOGGER.info("confirm Password !! " + changepswd.getConfirmPassword());
		 String oldPwd = changepswd.getCurrentPassword();
	        String newPassword = changepswd.getNewPassword();
	        String confirmNewPassword = changepswd.getConfirmPassword();
		 
		 String userId = ((java.security.Principal) request.getUserPrincipal()).getName();
	        LoginContext lc = null;

	        Subject subject = null;
	        try {
	            lc = new LoginContext("WSLogin",new WSCallbackHandlerImpl(userId, oldPwd));

	        } catch (LoginException le) {
	        	LOGGER.error("Cannot create LoginContext. " + le.getMessage());

	        } catch (SecurityException se) {

	        	LOGGER.error("Security Exception. " + se.getMessage());
	        }
	        boolean passwordVerification = false;
	        try {
	            lc.login();
	            subject = lc.getSubject();
	            LOGGER.info("loged in" + subject.getPublicCredentials());
	            passwordVerification = true;
	            com.ibm.websphere.security.auth.WSSubject.setRunAsSubject(subject);
	        } catch (LoginException le) {
	            passwordVerification = false;
	            LOGGER.error("Fails to create Subject. " + le.getMessage());

	        } catch (WSSecurityException ex) {
	            //  Logger.getLogger(ChangePassword.class.getName()).log(Level.SEVERE, null, ex);
	            passwordVerification = false;
	            LOGGER.error("WSSecurityException @@ " + ex);
	        }

	        String userName = request.getRemoteUser();
	        LOGGER.info("userName - " + userName);
	        LOGGER.info("passwordVerification" + passwordVerification);
	        if (passwordVerification) {
	        	LOGGER.info("Password Maching ! I am able to change password");
	            try {
	                resetPassword(request, response, newPassword);
	                
	                userService.updateUserpasswordInDB(userName, newPassword);
	                
	                //
	            } catch (Exception ex) {
	               // java.util.logging.Logger.getLogger(UserProfile.class.getName()).log(Level.SEVERE, null, ex);
	            	LOGGER.error("Exception while doing rest password" , ex);
	            }
	        }
	        else{
	        response.setRenderParameter("OldPasswordMismatch", "Old Password is wrong");
	        }
		 
		 response.setRenderParameter("action", "ChangePassword");
	 }
	 
	 @RenderMapping(params = "action=showIdsInformation")
	  public String showIdsInformation(RenderRequest request, ModelMap map) {
	    LOGGER.info("---------ids Information method-------------");
	  //  clearSession(request);
	   
	    SignUpBean bean = new SignUpBean();
	    SignUpBean userRequestbean = new SignUpBean();
	    bean = userService.getUserProfile(request.getRemoteUser(), request);
	    LOGGER.info("--------bean----userId-------" + bean.getUserId());
	    userRequestbean = userService.getUserUpdateRequestInfo(bean.getUserId());
	   
	    if (StringUtils.isNotEmpty(userRequestbean.getStatus()) && "1".equals(userRequestbean.getStatus())) {
	    	LOGGER.info("Update Request Status " + userRequestbean.getStatus());
	      request.setAttribute("displayMode", "VIEW");
	    }

	    map.addAttribute(SignUpConstant.SIGNUP_BEAN, bean);
	    return "ids-information";
	  }
	 
	 @ActionMapping(params = "action=updateGeneralInformation")
	  public void updateGeneralInformation(@ModelAttribute("signUpBean") SignUpBean signUpBean, ActionRequest request,
	      ActionResponse response) {
	    String buttonValue = request.getParameter(SignUpConstant.BUTTON_NAME);
	    LOGGER.info("------buttonValue---------" + buttonValue);
	    String environment = System.getProperty("app.env");
	    if (buttonValue == null) {
	      response.setRenderParameter(SignUpConstant.ACTION, "showMyProfile");
	      return;
	    }
	    LOGGER.info("------signUpBean---in updateGeneralInformation---------" + signUpBean);
	    signUpBean.setLocale(request.getLocale());
	    signUpBean.setRequest(request);
	    signUpBean.setEnvironment(environment);
	    LOGGER.info("---------userId signupbean------------" + signUpBean.getUserId());
	  
	    if (null != request.getRemoteUser()) {
	    	userService.updateGeneralUserInfo(signUpBean, request.getRemoteUser());
	    }

	    response.setRenderParameter(SignUpConstant.ACTION, "generalSuccess");

	  }
	
	 @ResourceMapping(value = "fetchDocId")
	  public void fetchEmirateDoc(ResourceResponse response, @RequestParam int id) {
	    LOGGER.info("----documentID--in fetchEmirateDocId---in ded user profile " + id);
	    OutputStream os = null;
	    try {
	      SignUpAttachmentEntity attachment = userService.getAttachmentById(id);

	      byte[] fileData = attachment.getContent();//bean.getContent()
	      if (fileData != null) {
	        String documentExtension = attachment.getDocExtension();
	        if (SignUpConstant.DOC.equals(documentExtension)) {
	          response.setContentType("application/msword");
	        } else if (SignUpConstant.DOCX.equals(documentExtension)) {
	          response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
	        } else if (SignUpConstant.PDF.equals(documentExtension)) {
	          response.setContentType("application/pdf");
	        } else {
	          response.setContentType("application/octet-stream");
	        }
	        String fullFileName = attachment.getDocName().toString() + documentExtension;
	        LOGGER.info("fullFileName::::" + fullFileName);

	        response.setProperty("Content-Disposition", "attachment; filename=" + fullFileName);
	        os = response.getPortletOutputStream();
	        os.write(fileData);
	        os.flush();
	        os.close();
	      }
	    } catch (Exception e) {
	      LOGGER.error("Exception while downloading the file:", e);

	    }

	  }
	 
	 @ResourceMapping(value = "checkMailIdAvailability")
	  public void checkMailIdAvailability(@ModelAttribute("signUpBean") SignUpBean signUpBean, ResourceRequest request,
	      ResourceResponse response, ModelMap map) {
	    LOGGER.info("---- checkUserNameAvailability ----");
	    boolean isEmailIdAvailable = false;
	    String eMail = request.getParameter("email");
	    LOGGER.info("--- eMail -----:" + eMail);
	  JsonObject jsonObj = new JsonObject();
	 Gson gson = new Gson();
	  //  JsonArray jsonArrayValue = new JsonArray();
	 
	  //  JSONArray jsonArrayId = JSONFactoryUtil.createJSONArray();
	   // JSONArray jsonArrayValue = JSONFactoryUtil.createJSONArray();
	    try {
	      isEmailIdAvailable = userService.isEmailAvailable(eMail);
	      map.addAttribute(SignUpConstant.IS_EMAIL_ID_AVAILABLE, isEmailIdAvailable);
	      request.getPortletSession().setAttribute(SignUpConstant.IS_EMAIL_ID_AVAILABLE, isEmailIdAvailable);
	      response.resetBuffer();
	      response.setContentType(SignUpConstant.APPLICATION_JSON);
	      PrintWriter writer = response.getWriter();

	      jsonObj.addProperty(SignUpConstant.IS_EMAIL_ID_AVAILABLE,isEmailIdAvailable);
	      jsonObj.addProperty("eMailId",eMail);
	   //   jsonArrayValue.put(isEmailIdAvailable);
	     String jsonString = gson.toJson(jsonObj);
	    LOGGER.info("jsonString >>>>>>> " + jsonString);
	      writer.write(jsonString);
	      LOGGER.info("User name verified and details were written to json object");

	    } catch (Exception e) {

	      LOGGER.error("Error in verifying the username in the method of checkUserNameAvailability\t" + e);
	    }
	  }
	 
	 @ResourceMapping(value = "checkMobileNumberAvailability")
	  public void checkMobileNumberAvailability(ResourceRequest request, ResourceResponse response, ModelMap map) {
	    LOGGER.info("-------inside-checkMobileNumberAvailability-------------");
	    boolean isMobileAvailable = false;
	    String mobileNo = request.getParameter("mobileNo");

	    LOGGER.info("--- mobileNumber -----:" + mobileNo);
	    JsonObject jsonObj = new JsonObject();
		 Gson gson = new Gson();
	    try {
	      isMobileAvailable = userService.isMobileNUmberAvailable(mobileNo);
	      LOGGER.info("---- isMobileAvailable ---:" + isMobileAvailable);
	      map.addAttribute(SignUpConstant.IS_MOBILE_NUMBER_AVAILABLE, isMobileAvailable);

	      request.getPortletSession().setAttribute(SignUpConstant.IS_MOBILE_NUMBER_AVAILABLE, isMobileAvailable);

	      response.resetBuffer();
	      response.setContentType(SignUpConstant.APPLICATION_JSON);
	      PrintWriter writer = response.getWriter();
	      jsonObj.addProperty(SignUpConstant.IS_MOBILE_NUMBER_AVAILABLE, isMobileAvailable);
	      jsonObj.addProperty("mobileNo", mobileNo);
	  
	      String jsonString = gson.toJson(jsonObj);
	      
	      LOGGER.info("Mobile json String " +jsonString);
	      writer.write(jsonString);
	      
	    
	      LOGGER.info("Mobile Number verified and details were written to json object");

	    } catch (Exception e) {
	      LOGGER.error("Error in verifying the mobile Number in the method of checkMObileAvailability\t", e);
	    }
	  }
	 
	 @ResourceMapping(value = "checkPassportNoAvailability")
	  public void checkPassportNoAvailability(ResourceRequest request, ResourceResponse response, ModelMap map) {
		 LOGGER.info("checkPassportNoAvailability >>>>>> ");
		 boolean isUniqueIdAvailable = false;
		    String uniquid = request.getParameter("passportNo");

		    LOGGER.info("--- uniquid -----:" + uniquid);
		    JsonObject jsonObj = new JsonObject();
			 Gson gson = new Gson();

		    try {
		      isUniqueIdAvailable = userService.isUniqueIdAvailable(uniquid);
		      LOGGER.info("-------isPassportNoAvailable--------" + isUniqueIdAvailable);
		      map.addAttribute(SignUpConstant.IS_PASSPORT_NO_AVAILABLE, isUniqueIdAvailable);

		      request.getPortletSession().setAttribute(SignUpConstant.IS_PASSPORT_NO_AVAILABLE, isUniqueIdAvailable);

		      response.resetBuffer();
		      response.setContentType(SignUpConstant.APPLICATION_JSON);
		      PrintWriter writer = response.getWriter();

		      jsonObj.addProperty(SignUpConstant.IS_PASSPORT_NO_AVAILABLE,isUniqueIdAvailable);
		      jsonObj.addProperty("passportNo",uniquid);
		      
		     String jsonString = gson.toJson(jsonObj);
		     
		      writer.write(jsonString);
		      LOGGER.info("passport No verified and details were written to json object");

		    } catch (Exception e) {
		      LOGGER.error("Error in verifying the passport No in the method of checkUserNameAvailability\t", e);
		    }
	 }
	 
	 @ResourceMapping(value = "checkResidenceIDAvailability")
	  public void checkResidenceIDAvailability(ResourceRequest request, ResourceResponse response, ModelMap map) {

	    boolean isresidenceIdAvailable = false;
	    String residenceId = request.getParameter("residenceId");
	    String parsedresidenceId = "";
	    for(int i = 0; i < residenceId.length();i++ )
	    {
	    	char a = residenceId.charAt(i);
	    	if(Character.isDigit(a))
	    	{
	    		parsedresidenceId = parsedresidenceId+  a;
	    	}
	    }

	    LOGGER.info("--- Residence Id -----:" + residenceId);
	    JsonObject jsonObj = new JsonObject();
		 Gson gson = new Gson();

	    try {
	      isresidenceIdAvailable = userService.isUniqueIdAvailable(parsedresidenceId);
	      LOGGER.info("-------isResidenceIdAvailable--------" + isresidenceIdAvailable);
	      map.addAttribute(SignUpConstant.IS_RESIDENCE_ID_AVAILABLE, isresidenceIdAvailable);

	      request.getPortletSession().setAttribute(SignUpConstant.IS_RESIDENCE_ID_AVAILABLE, isresidenceIdAvailable);

	      response.resetBuffer();
	      response.setContentType(SignUpConstant.APPLICATION_JSON);
	      PrintWriter writer = response.getWriter();
	      
	      jsonObj.addProperty(SignUpConstant.IS_RESIDENCE_ID_AVAILABLE,isresidenceIdAvailable);
	      jsonObj.addProperty("residenceId",residenceId);
	      
	      String jsonString = gson.toJson(jsonObj);
		     
	      writer.write(jsonString);
	      LOGGER.info("Residence Id verified and details were written to json object");

	    } catch (Exception e) {
	      LOGGER.error("Error in verifying the Emirate Id in the method of checkUserNameAvailability\t", e);
	    }
	  }
	 
	 @ResourceMapping(value = "checkEmirateIDAvailability")
	  public void checkEmirateIDAvailability(ResourceRequest request, ResourceResponse response, ModelMap map) {

	    boolean isEmirateIdAvailable = false;
	    String emirateId = request.getParameter("emirateId");
	    String parsedEmirateId = "";
	    for(int i = 0; i < emirateId.length();i++ )
	    {
	    	char a = emirateId.charAt(i);
	    	if(Character.isDigit(a))
	    	{
	    		parsedEmirateId = parsedEmirateId+  a;
	    	}
	    }

	    LOGGER.info("--- emirateId -----:" + emirateId);
	    JsonObject jsonObj = new JsonObject();
		 Gson gson = new Gson();

	    try {
	      isEmirateIdAvailable = userService.isUniqueIdAvailable(parsedEmirateId);
	      LOGGER.info("-------isEmirateIdAvailable--------" + isEmirateIdAvailable);
	      map.addAttribute(SignUpConstant.IS_EMIRATE_ID_AVAILABLE, isEmirateIdAvailable);

	      request.getPortletSession().setAttribute(SignUpConstant.IS_EMIRATE_ID_AVAILABLE, isEmirateIdAvailable);

	      response.resetBuffer();
	      response.setContentType(SignUpConstant.APPLICATION_JSON);
	      PrintWriter writer = response.getWriter();
	      
	      jsonObj.addProperty(SignUpConstant.IS_EMIRATE_ID_AVAILABLE,isEmirateIdAvailable);
	      jsonObj.addProperty("emirateId",emirateId);
	      
	      String jsonString = gson.toJson(jsonObj);
		     
	      writer.write(jsonString);
	      LOGGER.info("Emirate Id verified and details were written to json object");

	    } catch (Exception e) {
	      LOGGER.error("Error in verifying the Emirate Id in the method of checkUserNameAvailability\t", e);
	    }
	  }
	 
	 @ActionMapping(params = "action=saveUserUpdateRequest")
	  public void saveUserUpdateRequest(@ModelAttribute("signUpBean") SignUpBean signUpBean, ActionRequest request,
	      ActionResponse response, ModelMap map) {
	    String buttonValue = request.getParameter(SignUpConstant.BUTTON_NAME);
	    LOGGER.info("------buttonValue---------" + buttonValue);
	   /* if (buttonValue == null) {
	      response.setRenderParameter(SignUpConstant.ACTION, "showMyProfile");
	      return;
	    }*/
	   
	    LOGGER.info("------signUpBean---in saveUserUpdateRequest---------" + signUpBean);
	    LOGGER.info("---------userId signupbean------------" + signUpBean.getUserId());
	    signUpBean.setLocale(request.getLocale());
	  
	    userService.saveUserUpdateRequest(signUpBean, request.getRemoteUser(), request.getLocale());
	    map.addAttribute("updateProfile", true);
	    response.setRenderParameter(SignUpConstant.ACTION, SignUpConstant.SUCCESS);

	  }
	 
	 @RenderMapping(params = "action=success")
	  public String success() {
	    return "sign_up_completed";

	  }
	 
 
	 public void resetPassword(ActionRequest request, ActionResponse response, String password) throws Exception {
	       
	        String userId = ((java.security.Principal) request.getUserPrincipal()).getName(); //"test1";
	        BasicAttributes entry;
	        BasicAttribute oc1;
	        try {
	            String entryDN = "uid=" + userId + ",cn=ega,c=ae";

	            BasicAttribute userPassword = new BasicAttribute("userPassword", password);

	            entry = new BasicAttributes();
	            oc1 = new BasicAttribute("objectClass");
	            oc1.add("top");
	            oc1.add("person");

	            oc1.add("inetOrgPerson");

	            entry.put(userPassword);

	            LOGGER.info("entryDN >> " + entryDN);
	            String environment = System.getProperty("app.env");
	            LdapContext _ldapconnection = tdsConnection.getConnection(environment);
	            _ldapconnection.modifyAttributes(entryDN, DirContext.REPLACE_ATTRIBUTE, entry);
	            response.setRenderParameter("PasswordException", "Password updated successfully");
	            LOGGER.info("user password updated sucessfully");

	        } catch (Exception e) {
	            response.setRenderParameter("PasswordException", "Password updation failed");
	            LOGGER.error("An Exception Occured in updating User   " , e);
	            
	        }
	    }
}
