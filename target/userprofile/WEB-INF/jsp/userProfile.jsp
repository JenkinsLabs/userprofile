<!DOCTYPE HTML><%@page language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>   
	<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%> 
<h3>User Profile</h3>

<div class="form_content">
				<%@ include file="/WEB-INF/jsp/left_info_bar.jsp" %>
				
					<fieldset class="idInfoFieldset">
					<legend>
						<spring:message code="user.registration" />
					</legend>
					
					<div class="form_details_left myprofile">
					
						<ul>
						   <li><label><spring:message code="user.name" /></label> <span id="formText">${signUpBean.userName}</span></li>
						
							 <li><label><spring:message code="first.name" /></label> <span id="formText">${signUpBean.firstName}</span></li>
							 
							  <li><label><spring:message code="family.name" /></label> <span id="formText">${signUpBean.familyName}</span></li>
						  
							<li><label><spring:message code="email" /></label> <span id="formText">${signUpBean.email}</span></li>
							<li><label><spring:message code="marital.status" /></label>
							<c:choose>							
							<c:when test="${signUpBean.maritalStatus==-1}">							
							</c:when>
							<c:otherwise>							
							<span id="formText">${signUpBean.maritalStatusValue}</span>							
							</c:otherwise>
							
							</c:choose> 
							</li>
					
						</ul>
					</div>
					<div class="form_details_right myprofile">
						<ul>
						<li><label><spring:message code="nationality" /></label>						
						<c:choose>							
							<c:when test="${signUpBean.nationality==-1}">							
							</c:when>
							<c:otherwise>							
							<span id="formText">${signUpBean.nationalityValue}</span>							
							</c:otherwise>							
							</c:choose>
						
						</li>
						<li><label><spring:message code="second.name" /></label> <span id="formText">${signUpBean.secondName}</span></li>
						   
						  
						
							
							
											
							<li><label><spring:message code="mobile" /></label><span id="formText">${signUpBean.mobileNo}</span></li>
						
										
							<li><label><spring:message code="dob" /></label><span id="formText">${signUpBean.dateOfBirth}</span></li>
							
							<c:choose>
                          <c:when test="${signUpBean.gender==1}">
                          <li><label><spring:message code="gender" /></label><span id="formText"><spring:message code="label.male"/></span></li>
                           </c:when>
                           <c:otherwise>
                           <li><label><spring:message code="gender" /></label><span id="formText"><spring:message code="label.female"/></span></li>                          
                           </c:otherwise>
                           </c:choose>							
						</ul>

					</div>
		</fieldset>

					<fieldset class="idInfoFieldset2">
						<legend>
							<spring:message code="id.information" />
						</legend>				
<c:if test="${not empty signUpBean.emirateRadioPath}">			
<div id="idRadioButton" class="form_details_left radioButtonsMyProfile">
<ul>
<li>
<label><spring:message code="provide.id" /></label>
<span>
<input type="radio" id="radioId" name="radioId" value="0" disabled="disabled" checked="checked"/>

<label><spring:message code="label.national.id" /></label>
<input type="radio" id="radioId" name="radioId" value="1" disabled="disabled"/> 
<label><spring:message code="label.unified.passport.id" /></label>
</span>
</li>
</ul>
</div>
</c:if>	
						<c:if test="${not empty signUpBean.emirateFileName}">
						<div id="citizenId">
							<table class="tabDeg myprofile">

								<tr>
									<th><spring:message code="emirates.id" /></th>
									<th><spring:message code="expire.date" /></th>
									<th><spring:message code="copy.attachement" /></th>

								</tr>
								<tr>
								<c:choose>
								<c:when test="${(signUpBean.nationality=='187') || (signUpBean.nationality=='119') || (signUpBean.nationality=='36')||(signUpBean.nationality=='165')||(signUpBean.nationality=='176')}">
									<td valign="top"><span id="tableText">${signUpBean.emirateIdGCC}</span>
									</td>
									</c:when>
								<c:otherwise>
									<td valign="top"><span id="tableText">${signUpBean.emiratesId}</span>
									</td>

								</c:otherwise>								
								</c:choose>
								
									
									<td valign="top"><span id="tableText">${signUpBean.emirateExpireDate}</span>
									</td>
									<td valign="top"><span class="downloadLink"> 
									
									<a id="link_color"
													href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.emirateFileId}"/></portlet:resourceURL>'>
													<spring:message code="emirate.id"></spring:message> </a>
									</span></td>
								</tr>
							</table>
						</div>
						</c:if>
						
							<div id="expatId">
							<c:if test="${not empty signUpBean.residenceFileName}">
								<table class="tabDeg myprofile">

									<tr>

										<th><spring:message code="residence.id" /></th>
										<th><spring:message code="expat.expire.date" /></th>
										<th><spring:message code="residence.copy.attachement" /></th>

									</tr>

									<tr>

										<td valign="top"><span id="tableText">${signUpBean.residenceId}</span>

										</td>
										<td valign="top"><span id="tableText">${signUpBean.residenceExpireDate}</span>
										<td valign="top"><span class="downloadLink"> 	<a id="link_color"
													href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.residenceFileId}"/></portlet:resourceURL>'>
													<spring:message code="residence.id"></spring:message></a>
										</span> 
										</td>
									</tr>


								</table>
								</c:if>
								
								<c:if test="${not empty signUpBean.passportFileName}">
								<table class="tabDeg myprofile">


									<tr>

										<th><spring:message code="passport.no" /></th>
										<th><spring:message code="passport.expire.date" /></th>
										<th><spring:message code="passport.copy.attachement" /></th>

									</tr>

									<tr>

										<td valign="top"><span id="tableText">${signUpBean.passportNo}</span>

										</td>
										<td valign="top"><span id="tableText">${signUpBean.passportExpiryDate}</span>
										<td valign="top"><span class="downloadLink"> 	<a id="link_color"
													href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.passportFileId}"/></portlet:resourceURL>'>
													 <spring:message code="passport.id"></spring:message></a>
										</span> 
										</td>
									</tr>


								</table>
								</c:if>
							</div>
						
					</fieldset>