<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ page import="java.util.Properties"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page import="java.util.Locale"%>
<%@ page import="java.util.ResourceBundle"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
	rel="stylesheet">
	<script src="<%=request.getContextPath() %>/js/id_information_validation.js"></script>
	<script src="<%=request.getContextPath() %>/js/general_information_validation.js"></script>

<portlet:actionURL var="updateGeneralInformation">
	<portlet:param name="action" value="updateGeneralInformation"></portlet:param>
</portlet:actionURL>

<portlet:renderURL var="resetSignUpPage">
<portlet:param name="action" value="resetSignUpPage"></portlet:param>
</portlet:renderURL>
<portlet:actionURL var="saveUserUpdateRequest">
	<portlet:param name="action" value="saveUserUpdateRequest"></portlet:param>
</portlet:actionURL>

<%
	Locale locale = request.getLocale();
	String alignment = "left";
	String lang = "en";
	if (locale.getLanguage().equalsIgnoreCase("ar")) {
		alignment = "right";
		lang = "ar";
	}

%>
<% if(lang.equals("ar")){%>
<script>



 $( function() { 
 $( "#dateOfBirth" ).datepicker({dateFormat: "dd/mm/yy"});
 $( "#emirateidexpiry" ).datepicker({dateFormat: "dd/mm/yy"});
 $( "#residenceExpireDate" ).datepicker({dateFormat: "dd/mm/yy"});
        $("#dateOfBirth").datepicker($.datepicker.regional["ar"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
        } ); 
         
           $( function() { 
           $("#emirateidexpiry").datepicker($.datepicker.regional["ar"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
        } ); 
        
        $( function() { 
           $("#residenceExpireDate").datepicker($.datepicker.regional["ar"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
        } ); 
  
  </script>
  <%}else{ %>
  <script>
   $( function() { 
 	$( "#dateOfBirth" ).datepicker({dateFormat: "dd/mm/yy"});
 $( "#emirateidexpiry" ).datepicker({dateFormat: "dd/mm/yy"});
 $( "#residenceExpireDate" ).datepicker({dateFormat: "dd/mm/yy"});
        $("#dateOfBirth").datepicker($.datepicker.regional["en"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
         } );
         
          $( function() { 
 
        $("#emirateidexpiry").datepicker($.datepicker.regional["en"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
         });
          $( function() { 
 
        $("#residenceExpireDate").datepicker($.datepicker.regional["en"]).datepicker("option", {           
          changeMonth: true,
          changeYear: true
        });
         });
         </script>
 <%}%>


<script type="text/javascript">
  var errorMessage = {
			firstName : '<spring:message code="Empty.FamilyName"/>' ,
			firstNameFormat : '<spring:message code="error.firstNameFormat"/>' ,
			familyName : '<spring:message code="Empty.FamilyName"/>',
			dateOfBirth : '<spring:message code="Empty.DateOfBirth"/>' ,
			secondName: '<spring:message code="Empty.SecondName"/>' ,
			maritalStatus:'<spring:message code="error.marital.status"/>',
			gender:'<spring:message code="error.gender"/>',
			regType : '<spring:message code="error.select.registration"/>' ,
			nationality : '<spring:message code="error.select.nationality"/>' ,
			email : '<spring:message code="error.email"/>' ,
			emailFormat : '<spring:message code="error.emailFormat"/>' ,
			confirmEmail : '<spring:message code="error.confirmEmail"/>' ,
			mobile : '<spring:message code="error.mobileNo"/>' ,
			idExpireDate : '<spring:message code="error.idExpireDate"/>' , 
			idCopyAttachement : '<spring:message code="error.idCopyAttachement"/>' ,
			emiratesID : '<spring:message code="error.emiratesID"/>' ,
			passportNo : '<spring:message code="error.passportNo"/>' ,
			passportExpiredate : '<spring:message code="error.passportExpiredate"/>' ,
			passportCopyAttachement : '<spring:message code="error.passportCopyAttachement"/>' ,
			residenceId : '<spring:message code="error.residenceId"/>' ,
			invalidResidenceId: '<spring:message code="error.invalid.residenceId"/>' ,
			residenceExpiredate : '<spring:message code="error.residenceExpiredate"/>' , 
			residenceCopyAttachement : '<spring:message code="error.residenceCopyAttachement"/>' ,
			emirateExpiryDate: '<spring:message code="error.id.expiry.date"/>' ,
			residenceExpireDate: '<spring:message code="error.residence.id.expiry.date"/>' ,
			passportExpiryDate: '<spring:message code="error.passport.expiry.date"/>' ,
			passportAttachment:'<spring:message code="error.passportAttachment"/>' ,
			fileLength:'<spring:message code="error.invalid.file.length"/>',
			attachmentType:'<spring:message code="error.attachment.type"/>',
			invalidMobile:'<spring:message code="invalid.mobile"/>',
			invalidPassport:'<spring:message code="invalid.passport"/>',
			emirateId:'<spring:message code="error.emirateId"/>',
			invalidEmirateId:'<spring:message code="error.invalid.emirateId"/>',
			terms:'<spring:message code="Empty.Terms"/>',
			invalidConfirmEmail: '<spring:message code="invalid.error.confirmEmail"/>' ,
			invalidConfirmPassword:'<spring:message code="invalid.error.confirmPassword"/>' ,
			emptyPassword:'<spring:message code="empty.error.confirmPassword"/>' ,
			veryWeak:'<spring:message code="error.very.weak"/>' ,
			emailExist:'<spring:message code="email.exist"/>',
			passwordInclude:'<spring:message code="password.include"/>',
			characters:'<spring:message code="password.characters"/>',
			oneCapitalLetter:'<spring:message code="password.one.capital.letter"/>',
			oneNumber:'<spring:message code="password.one.number"/>',
			noSpaces:'<spring:message code="password.no.spaces"/>',
			emailIdExist:'<spring:message code="emirate.id.exist"/>',
			residenceIdExist:'<spring:message code="residence.id.exist"/>',
			mobileNumberExist:'<spring:message code="mobile.exist"/>',
			
		
	};
	</script>
	
	<script type="text/javascript">
	 $( document ).ready(function() {  
	<c:if test="${not empty displayMode}">
		$("#firstName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#secondName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#familyName").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#dateOfBirth").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#genderId").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#maritalStatusId").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#email").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#confirmEmail").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#mobileNo").css('cursor','not-allowed').prop('disabled', 'disabled');
		$("#nationality").css('cursor','not-allowed').prop('disabled', 'disabled');
		$('#citizenExpatFieldsetId').find('input,radiobutton').prop('disabled', 'disabled');
		$('#formsubmitid').prop('disabled', 'disabled');
	</c:if>
});

function checkEmirateIDAvailability(id){
	console.log("---checkEmirateIDAvailability---"+id);
	var emirateId=document.getElementById(id).value;
			
	
	if(id=='emiratesId'){
    //alert("-emiratesId condition--");
	var extraEmirateCheck=document.getElementById('extraEmirateCheckId').value;
	}else{
	//alert("---else condition---");
	var extraEmirateCheck=document.getElementById('extraEmirateCheckGCCId').value;
	}
	if(emirateId!=extraEmirateCheck){
	  $("#emiratesId").parent().find('.errormsg,.succesmsg').remove(); 
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkEmirateIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	emirateId: emirateId
        },
        success: function( data ) {
        	 console.log('id ------'+id);
        	if(data.isEmirateIdAvailable){
        		console.log('Emirate Id exists');
        		if($('#'+id).parent().find('.errormsg').length == 0){
        			$('#'+id).parent().append("<p class='failureImg	 emirateIdFailure'></p>");
        		$('#'+id).parent().append("<p class='errormsg'>"+data.emirateId+" <spring:message code='emirate.id.exist'/></p>");
        		}
        		isEmirateValid = false;
        	}else{
        		console.log('Emirate Id doesnot exists');
        		if($('#'+id).parent().find('.errormsg').length == 0)
        		 $('#'+id).parent().append("<p class='succesmsg'>"+data.emirateId+" <spring:message code='emirate.id.doesnot.exist'/></p>"); 
        		/* $('#'+id).parent().append("<p class='succesmsg emirateIdSuccess'></p>"); */
        		isEmirateValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
        	console.log("some error in-----checkEmirateIDAvailability-------"+textStatus);
        }
      });
	}else{
		isEmirateValid = true;
	}
}



function checkResidenceIDAvailability(){
	console.log("---checkResidenceIDAvailability---");
	var residenceId=document.getElementById('residenceId').value;
	var extraResidenceCheck=document.getElementById('extraResidenceCheckId').value;
	console.log('residenceId--'+residenceId);
	  $("#residenceId").parent().find('.errormsg,.succesmsg').remove(); 
	if(residenceId!=extraResidenceCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkResidenceIDAvailability"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	residenceId: residenceId
        },
        success: function( data ) {
        	 console.log('id ------'+data.isresidenceIdAvailable);
        	if(data.isresidenceIdAvailable){
        		console.log('Residence Id exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0){
        			/* $("#residenceId").parent().append("<p class='ur_failureImg emirateIdFailure'></p>"); */
        		$("#residenceId").parent().append("<p class='errormsg'>"+data.residenceId+" <spring:message code='residence.id.exist'/></p>");
        		}
        		isResidenceValid = false;
        	}else{
        		console.log('Residence Id doesnot exists');
        		if($("#residenceId").parent().find('.errormsg').length == 0)
        		 $("#residenceId").parent().append("<p class='succesmsg'>"+data.residenceId+" <spring:message code='residence.id.doesnot.exist'/></p>"); 
        		/* $("#residenceId").parent().append("<p class='succesmsg'></p>"); */
        		isResidenceValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		isResidenceValid = true;
	} 
}
 

function checkEmailAvailability(){
	var email=document.getElementById('email').value;
	var extraEmailCheck=document.getElementById('extraEmailCheckId').value;	
   console.log('email'+email);
   console.log('extraEmailCheck'+extraEmailCheck);  
    $("#email").parent().find('.errormsg,.succesmsg').remove(); 
	if(email!=extraEmailCheck){
	//$( "#userName" ).parent().find('.ur_errormsg,.ur_succesmsg').remove();
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMailIdAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	email: email
        },
        success: function( data ) {        	
	        //	alert(data.isEmailIdAvailable);
	        	//if(data.value[0]){
	        	if(data.isEmailIdAvailable){
	        		if($("#email").parent().find('.errormsg').length == 0){
	        			/* $("#email").parent().append("<p class='errormsg'></p>"); */
	        		$("#email").parent().append("<p class='errormsg'>"+data.eMailId+" <spring:message code='email.exist'/></p>");
	        		}
	        		isEmailValid = false;
	        		return false;
	        	}else{
	        		if($("#email").parent().find('.errormsg').length == 0)
	        		 $("#email").parent().append("<p class='succesmsg'>"+data.eMailId+" <spring:message code='email.doesnot.exist'/></p>"); 
	        		 isEmailValid = true;       		
	        		
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}
}



function checkMobileNumberAvailability(){
	var mobileNo=document.getElementById('mobileNo').value;
	var extraMobileCheck=document.getElementById('extraMobileCheckId').value;
	var index1 = extraMobileCheck.indexOf('5');
    var phoneSubString = extraMobileCheck.substring(index1);	
	var format1="0971"+phoneSubString;
	var format2="00971"+phoneSubString;
	var format3="971"+phoneSubString;
	var format4="0"+phoneSubString;
   console.log('mobileNo'+mobileNo);
   console.log('extraMobileCheck'+extraMobileCheck);
   $("#mobileNo").parent().find('.errormsg,.succesmsg').remove();
	/* if(mobileNo!=extraMobileCheck){ */
		if ( [ format1, format2, format3,format4 ].indexOf( mobileNo ) == -1 ) { 
	   //alert("------not matching--------------");
		console.log("-------inside mobile number ajax calling-------------");
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkMobileNumberAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	mobileNo: mobileNo
        },
        success: function( data ) {        	
	        	console.log("----------data value-----"+data.isMobileAvailable);
	        	if(data.isMobileAvailable){
	        		console.log("--------mobileNo--exists-------");
	        		if($("#mobileNo").parent().find('errormsg').length == 0){
	        		/* 	$("#mobileNo").parent().append("<p class='errormsg'></p>"); */
	        		$("#mobileNo").parent().append("<p class='errormsg'>"+data.mobileNo+" <spring:message code='mobile.exist'/></p>");
	        		}
	        		isMobileValid = false;
	        	}else{
	        		console.log("--------mobileNo-not-exists-------");
	        		if($("#mobileNo").parent().find('.errormsg').length == 0)
	        		 $("#mobileNo").parent().append("<p class='succesmsg'>"+data.mobileNo+" <spring:message code='mobile.doesnot.exist'/></p>"); 
	        		/* $("#mobileNo").parent().append("<p class='succesmsg'></p>"); */
	        		isMobileValid = true;
	        	}
        	
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	}else{
		isMobileValid = true;
	}
}

function checkPassportNoAvailability(){
	console.log("---checkPassportNoAvailability---");
	var passportNo=document.getElementById('passportNo').value;
	var extraPassportCheck=document.getElementById('extraPassportNoCheckId').value;
	console.log('passportNo--'+passportNo);
	 $("#passportNo").parent().find('.errormsg,.succesmsg').remove();
	if(passportNo!=extraPassportCheck){
    $.ajax({
    	type: "POST",
        url: '<portlet:resourceURL id="checkPassportNoAvailability" escapeXml="false"></portlet:resourceURL>',
        dataType: "json",
        crossDomain: true,
        async:false,
        data: {
        	passportNo: passportNo
        },
        success: function( data ) {
        	 console.log('id ------'+data.isPassportNoAvailable);
        	if(data.isPassportNoAvailable){
        		console.log('Passport Id exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0){
        		/* 	$("#passportNo").parent().append("<p class='errormsg'></p>"); */
        		$("#passportNo").parent().append("<p class='errormsg'>"+data.passportNo+" <spring:message code='passport.no.exist'/></p>");
        		}
        		isPassportValid = false;
        	}else{
        		console.log('passportNo doesnot exists');
        		if($("#passportNo").parent().find('.errormsg').length == 0)
        		$("#passportNo").parent().append("<p class='succesmsg'>"+data.passportNo+" <spring:message code='passport.id.doesnot.exist'/></p>");
        	
        		isPassportValid = true;
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("some error"+errorThrown);
        }
      });
	 }else{
		 isPassportValid = true;
	} 
}

	function showSelectType(value){
        	 console.log("--------value-----------"+value.value);
      	   if(value.value == "217"){
 				$("#citizenId").css("display", "block");
		$("#expatId").css("display", "none");
		$("#citizenExpatFieldsetId").css("display", "block");
	            
 			  } 
      	   else if(value.value == -1){
				$("#citizenId").css("display", "none");
			$("#expatId").css("display", "none");
			$("#citizenExpatFieldsetId").css("display", "none");		
			} else{
	       		$("#citizenId").css("display", "block");
		$("#expatId").css("display", "block");
		$("#citizenExpatFieldsetId").css("display", "block");
 			}
	}
	
	
function load() {
				var nationalityType = document.getElementById("nationality").value;
				
				 if(nationalityType == "217"){
	   				$("#citizenId").css("display", "block");
		$("#expatId").css("display", "none");
		$("#citizenExpatFieldsetId").css("display", "block");             
	   	       	
	   			}else if(nationalityType == -1){
	   				$("#citizenId").css("display", "none");
			$("#expatId").css("display", "none");
			$("#citizenExpatFieldsetId").css("display", "none");
				}else{
					$("#citizenId").css("display", "block");
		$("#expatId").css("display", "block");
		$("#citizenExpatFieldsetId").css("display", "block");
	   			}
				
			}
</script>
	<c:if test="${not empty displayMode}">
	<p class="updateRequest"><spring:message code="update.request.progress"></spring:message></p>
</c:if>
<body onload=load()>
	<div class="form-wrapper directioncls">
		<form:form id="userSignUpForm" name="userSignUp" commandName="signUpBean" method="post"
			action="${saveUserUpdateRequest}" enctype="multipart/form-data" autocomplete="off">
			<form:hidden name="buttonName" path="buttonName" id="buttonNameId" />
			<form:hidden path="otpRedirectUrl" value="${redirectUrl}" />
			<form:hidden path="emirateFileId" value="${signUpBean.emirateFileId}"/>
			<form:hidden path="residenceFileId" value="${signUpBean.residenceFileId}"/>
			<form:hidden path="passportFileId" value="${signUpBean.passportFileId}"/>
			<form:hidden path="egaUserExtId" value="${signUpBean.egaUserExtId}"/>
			<form:hidden id="nationalityValue" path="nationalityValue"/>
			<form:hidden id="genderValue" path="genderValue"/>
			<form:hidden id="maritalStatusValue" path="maritalStatusValue"/>			
			<form:hidden  id="userNameEdit" path="userName" value="${signUpBean.userName}"/> 
			<form:hidden  id="userID" path="userId" value="${signUpBean.userId}"/> 						
			<form:hidden id="extraEmailCheckId" path="extraEmailCheck" value="${signUpBean.email}"/>
			<form:hidden id="extraEmirateCheckId" path="extraEmirateCheck" value="${signUpBean.emiratesId}"/>
			<form:hidden id="extraEmirateCheckGCCId" path="extraEmirateCheckGCC" value="${signUpBean.emirateIdGCC}"/>
			<form:hidden id="extraResidenceCheckId" path="extraResidenceCheck" value="${signUpBean.residenceId}"/>
			<form:hidden id="extraMobileCheckId" path="extraMobileCheck" value="${signUpBean.mobileNo}"/>
			<form:hidden id="extraPassportNoCheckId" path="extraPassportCheck" value="${signUpBean.passportNo}"/>
		
			<div class="container"> 
<div class="col-md-12">
<div class="anonysums">
		<h5><spring:message code="user.registration" /></h5>
		
			 <div class="row">
			 
			 	<div class="col-md-6 col-lg-4">
     				 <div class="input-groupdiv"> <span class="info_box"></span>
        <label><spring:message code="user.name" /><span>*</span></label>
       <input type="text" maxlength="30" id="userNameEditId"  class="form-control" value="${signUpBean.userName}" disabled="disabled" style="cursor: not-allowed;"/>
			      </div>
			    </div>
			    
			    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="email.tooltip" /></span>
        <label><spring:message code="email" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
       
						<form:input	type="text" maxlength="45" name="email" path="email" id="email" class="form-control" tabindex="5"/>
					<form:errors path="email" class="errormsg" />
      </div>
    </div>
			  <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="confirm.email.tooltip" /></span>
        <label><spring:message code="confirm.email" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
        
						<form:input	type="text" maxlength="45" name="confirmEmail" path="confirmEmail" id="confirmEmail" class="form-control" tabindex="6" oncopy="return false" onpaste="return false"/>
					<form:errors path="confirmEmail" class="errormsg" />
        
      </div>
    </div>
    
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="firstName.tooltip" /></span>
        <label><spring:message code="first.name" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
       <form:input type="text" maxlength="35" name="firstName" path="firstName"
										id="firstName" class="form-control" tabindex="7"/>
						
					<form:errors path="firstName" class="errormsg" />
        
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="secondName.tooltip" /></span>
        <label><spring:message code="second.name" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
        <form:input type="text" maxlength="35" name="secondName" path="secondName" id="secondName" class="form-control" tabindex="8" />
					<form:errors path="secondName" class="errormsg" />
        
      </div>
    </div>
     <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="familyName.tooltip" /></span>
        <label><spring:message code="family.name" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
       
						<form:input type="text" maxlength="35" name="familyName" path="familyName"
										id="familyName" class="form-control" tabindex="9"/>
					<form:errors path="familyName" class="errormsg" />
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> <span class="info_box"> <spring:message code="mobile.tooltip" /></span>
        <label><spring:message code="mobile" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
      
			<form:input type="text" maxlength="14" name="mobileNo" path="mobileNo"
										id="mobileNo" class="form-control" tabindex="10"/>			
					<form:errors path="mobileNo" class="errormsg" />
        
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv">
        <label><spring:message code="dob" /><span>*</span> </label>
        
        <form:input type="text" name="dateOfBirth" path="dateOfBirth"
										id="dateOfBirth" class="form-control" tabindex="11"/>
					<form:errors path="dateOfBirth" class="errormsg" />
        
      </div>
    </div>
     <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv">
        <label><spring:message code="gender" /> <span> *</span> </label>
       <form:select id="genderId" path="gender"
										class="form-control"  tabindex="12">
										<form:option value="-1">
							<spring:message code="select" />
						</form:option>
						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${genderList}" itemValue="genderId" itemLabel="genderEn" />
						<%
							} else {
						%>
						<form:options items="${genderList}" itemValue="genderId" itemLabel="genderAr" />
						<%
							}
						%>
					</form:select>
					<form:errors path="gender" class="errormsg" />
        
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv"> 
        <label><spring:message code="marital.status" /> <span> *</span> </label>
        <form:select id="maritalStatusId" path="maritalStatus"
										class="form-control"  tabindex="11">

						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${maritalStatusList}" itemValue="maritalStatusId" itemLabel="maritalStatusEn" />
						<%
							} else {
						%>
						<form:options items="${maritalStatusList}" itemValue="maritalStatusId" itemLabel="maritalStatusAr" />
						<%
							}
						%>

					</form:select>
					<form:errors path="maritalStatus" class="errormsg" />
        
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="input-groupdiv">
        <label><spring:message code="nationality" /> <span>*</span> </label>
<form:select id="nationality" path="nationality"
										class="form-control" tabindex="10" onchange="showSelectType(this)">

						<form:option value="-1">
							<fmt:message key="select" />
						</form:option>
						<%
							if (request.getLocale().getLanguage()
											.equalsIgnoreCase("en")) {
						%>
						<form:options items="${countryList}" itemValue="id"
											itemLabel="descriptionEnglish" />
						<%
							} else {
						%>
						<form:options items="${countryList}" itemValue="id"
											itemLabel="descriptionArabic" />
						<%
							}
						%>
					</form:select>
					<form:errors path="nationality" class="errormsg" />
         
      </div>
    </div>
    
			 </div>
		</div> <!-- anonysums -->
		
		<div id="citizenExpatFieldsetId" style="display: none">
			<div class="anonysums">
			<h5><spring:message code="id.information" /></h5>
			
			<div id="citizenId" style="display: block;">
				<div class="tabldiv">
<table class="Informationtable">
    <thead>
		<th><spring:message code="emirates.id" /></th>
		<th><spring:message code="expire.date" /></th>
		<th><spring:message code="copy.attachement" /></th>
    </thead>
    <tbody>
    <td>
		<div class="input-groupdiv"> 
		<span class="info_box"> <spring:message code="emirate.id.tooltip" /></span>
		<label><spring:message code="emirates.id" /> <span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
		
								<form:input type="text" name="emiratesId" path="emiratesId"
								id="emiratesId" class="form-control" tabindex="15" maxlength="20" />
		<form:errors path="emiratesId" class="errormsg" />
		</div>
	</td>
	<td>
		<div class="input-groupdiv"> 
		
		<label><spring:message code="expire.date" /> <span>*</span></label>
						<form:input type="text" name="emirateExpireDate"
								path="emirateExpireDate" id="emirateidexpiry"
								class="form-control" tabindex="16" />
							<form:errors path="emirateExpireDate" class="errormsg" />
		
		</div>
	</td>
    <td>
		<a href="#">
			<div class="input-groupdiv"> 
			<span class="info_box"> <spring:message code="attachment.tooltip" /></span>
			<label><spring:message code="copy.attachement" /> <span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
			<form:input type="file" maxlength="35" name="emirateAttachement"
								path="emirateAttachement" id="emirateAttachement"
								class="form-control" tabindex="17" />
							<form:errors path="emirateAttachement" class="errormsg" />
							<c:if test="${userDetails.emirateFileId!=0 }">
							<a id="link_color" href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.emirateFileId}"/></portlet:resourceURL>'>
													<spring:message code="emirate.id"></spring:message></a>
													</c:if>
			
			</div>
		</a>
	</td>
</tbody>
</table>
	</div>	
			</div>
			
			
			<div id="expatId" style="display: block;">
<div class="tabldiv">
<table class="Informationtable">
    <thead>
		<th><spring:message code="residence.id" /></th>
		<th><spring:message code="expat.expire.date" /></th>
		<th><spring:message code="residence.copy.attachement" /></th>
    </thead>
    <tbody>
    <td>
		<div class="input-groupdiv"> 
		<span class="info_box"> <spring:message code="residence.id.tooltip" /></span>
		<label><spring:message code="residence.id" /> <span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
		<form:input type="text" maxlength="15" name="residenceId"
								path="residenceId" id="residenceId" class="form-control"
								tabindex="18" />
							<form:errors path="residenceId" class="errormsg" />
		
		</div>
	</td>
	<td>
		<div class="input-groupdiv"> 
		
		<label><spring:message code="expat.expire.date" /> <span>*</span></label>
		<form:input type="text" name="residenceExpireDate"
								path="residenceExpireDate" id="residenceExpireDate"
								class="form-control" tabindex="19" />
							<form:errors path="residenceExpireDate" class="errormsg" />
		
		</div>
	</td>  
    <td>
		<a href="#">
			<div class="input-groupdiv"> 
			<span class="info_box"> <spring:message code="attachment.tooltip" /></span>
			<label><spring:message code="residence.copy.attachement" /><span>*</span> <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i></label>
			<form:input type="file" maxlength="35"
								name="residenceAttachement" path="residenceAttachement"
								id="residenceAttachement" class="form-control" tabindex="20" />
							<form:errors path="residenceAttachement" class="errormsg" />
							<c:if test="${userDetails.residenceFileId!=0 }">
							<label><a id="link_color" href='<portlet:resourceURL id="fetchDocId"><portlet:param name="id"  value="${signUpBean.residenceFileId}"/></portlet:resourceURL>'>
													<spring:message code="residence.id"></spring:message></a></label>
													</c:if>
			
			</div>
		</a>
	</td>
</tbody>
</table>
</div>
</div>
		
			</div>
		</div>
		
		</div>
		</div>	
			
			<p class="text-center">  
<input type="submit" value="<spring:message code="button.submit"/>" class="btn_submit" id="formsubmitid">
<input value="<spring:message code="button.cancel"/>" class="btn_cancel" id="resetForm" type="button">
 
  </p>
  <br>
  <br>
  <br>
  <br>		
					
	</form:form>
	</div>
	</body>