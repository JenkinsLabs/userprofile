$( document ).ready(function() {  
	
	
	$('#generalLink').css('color','#B5251C').css('font-weight', 'bold');
	
	var valid = true;
	 $("#firstName").on( "blur change", function() {
		
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
     
         if ($("#firstName").val() == "") {
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.firstName+"</p>");
               }
               valid=valid&false;
         }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
        	
             if($(this).parent().find('errormsg').length == 0){
                 $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
             }
             valid = valid && false;              
         }else{
        	
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
	 
	 $("#secondName").on( "blur change", function() {
		
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
         if($("#secondName").val()==""){
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.secondName+"</p>");
               }
               valid=valid&false;
         }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
        	
             if($(this).parent().find('errormsg').length == 0){
                 $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
             }
             valid = valid && false;              
         }else{
        	
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
	 
	 $("#familyName").on( "blur change", function() {
			
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
         if($("#familyName").val()==""){
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.familyName+"</p>");
               }
               valid=valid&false;
         }else if(!/^[a-zA-Z\u0600-\u06FF .]+$/.test(this.value)){
        	
             if($(this).parent().find('errormsg').length == 0){
                 $(this).parent().append("<p class='errormsg'>"+errorMessage.firstNameFormat+"</p>");                  
             }
             valid = valid && false;              
         }else{
        	
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
	 
	 $("#dateOfBirth").on( "blur change", function() {
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
         if($("#dateOfBirth").val()==""){
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.dateOfBirth+"</p>");
               }
               valid=valid&false;
         }else{
        	
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
     
     $("#maritalStatusId").on( "blur change", function() {
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
         if($("#maritalstatus").val()==-1){
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.maritalStatus+"</p>");
               }
               valid=valid&false;
         }else{
        	
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
     $("#genderId").on( "blur change", function() {
         this.value = this.value.trim();
         $(this).parent().find('.errormsg').remove();
         if($("#genderId").val()==-1){
        	
             if($(this).parent().find('errormsg').length == 0){
                   $(this).parent().append("<p class='errormsg'>"+errorMessage.gender+"</p>");
               }
               valid=valid&false;
         }else{
        
           $(this).parent().find('errormsg').remove();
           valid = valid && true;
         }
       });
     
     /*$('form').submit(function(event) {
   $('input').blur();
   $('select').blur();
  
    if(!valid){ 
    	
     event.preventDefault();
    }
    else{
    	
	var element = document.getElementById("buttonNameId");
	element.value = "submit";
    }
  });*/
     
});